#include "qi_param.h"
#include "qi_assert.h"

static mqi_mat_t 		C, M, P, transco;
static mqi_mat_t		p_trans;
static mqi_vect_t		rat_point;
static mqi_vect_t		l;
static mqi_curve_param_t	cp, cp2;
static mqi_coef_t		det, max;
static mqi_hpoly_t		respoly;

static sshort conic_data[3][3] = {
	
	{ 1, 0, 0 },
	{ 0, 1, 0 },
	{ 0, 0,-1 }

};

/*static sshort conic_through_orig_data[3][3] = {

	{ 1, 0, 0 },
	{ 0, 0,-1 },
	{ 0,-1, 0 }

};*/

static sshort ratpoint_data[3] 			= { 1, 0, 1 };
/*static sshort ratpoint_through_orig_data[3] 	= { 0, 0, 1 };*/

void free_memory (void) {

	mqi_mat_clear (C);
	mqi_mat_clear (M);
	mqi_mat_clear (P);
	mqi_mat_clear (p_trans);
	mqi_vect_clear (rat_point);
	mqi_vect_clear (l);
	mqi_hpoly_clear(respoly);
	mqi_curve_param_clear (cp);
	mqi_curve_param_clear (cp2);
	mqi_coef_clear (det);
	mqi_coef_clear (max);
	mqi_log_printf ("\n");
}

int main (void) {

	unsigned short k;


	atexit (free_memory);

	mqi_mat_init_sshort (C, 3, 3);
	mqi_mat_set_all_sshort (C, &conic_data[0][0]);

	/** Random noise matrix */
	mqi_mat_init_sshort (P, 3, 3);
	mqi_coef_init_sshort(det);
	mqi_coef_init_sshort(max);
	mqi_coef_set_sshort(max,100);
	
	
	/** Parametrize through a ratpoint not going through the origin */
	mqi_vect_init_sshort (rat_point, 3);
	mqi_vect_set_all_sshort (rat_point, &ratpoint_data[0], 3);

	/** Apply the random transformation */
	mqi_mat_random_transformation (M, C, P, max, 0);

	/** Apply the transformation to the original rational point, using P */
	mqi_mat_init_dynamic (transco, P->n_rows, P->n_cols, mqi_mat_typeof(P));
	mqi_mat_transco	     (transco, P);
	mqi_mat_mul (rat_point, transco, rat_point);	

	/** Do the parametrization : is it "C" or "M" ?? */
	qi_param_conic_through_ratpoint (M, rat_point, p_trans, cp, l);

	mqi_log_printf ("Parametrizing conic:\n");
	mqi_mat_print (M);
	mqi_log_printf ("\nThrough rational point: "); mqi_vect_print(rat_point); mqi_log_printf ("\n");

	mqi_log_printf ("Gives:\n");
	mqi_curve_param_print (cp);

	mqi_log_printf ("\nWith transformation matrix:\n");
	mqi_mat_print (p_trans);

	mqi_log_printf ("\nVector:\n");
	mqi_vect_print (l);
	mqi_log_printf ("\n");

	mqi_mat_det (det, p_trans);

	/** Check that the transformation matrix has a non nul determinant */
	qi_assert ( mqi_coef_sign(det) != 0 );

	/** Check that the curve param is not nul */
	for ( k = 0; k < cp->size; k++ ) { 
		qi_assert ( ! mqi_hpoly_is_zero(cp->equations[k]) );
	}	

	/** Check that (Pcp)'C(Pcp) = 0 */
	mqi_curve_param_init_cpy (cp2, cp);
	
	mqi_curve_param_mul_mat 	(cp2, P, cp);
	mqi_curve_param_mul_mat 	(cp, M, cp2);

	mqi_log_printf ("\ncp: "); mqi_curve_param_print(cp); mqi_log_printf ("\n");
	mqi_log_printf ("cp2:"); mqi_curve_param_print(cp2);mqi_log_printf ("\n");

	mqi_curve_param_scalar_product 	(respoly, cp2, cp);

	qi_assert ( mqi_hpoly_is_zero (respoly) );

	return (EXIT_SUCCESS);
	
}

