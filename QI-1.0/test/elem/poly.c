#include "qi_elem.h"
#include "qi_assert.h"

static schar	poly_coefs[6] = {
	-6, -8, -9, 10, -5, 0
};

static mqi_hpoly_t poly;
static unsigned char result = 2;

void free_memory (void) {
	
	mqi_hpoly_clear (poly);
	mqi_log_printf ("\n");
	
}

int main (void) {

	unsigned char	count;
	
	atexit (free_memory);
	
	mqi_hpoly_init_schar (poly, 10);
	mqi_hpoly_set_all_schar (poly, &poly_coefs[0], 6);

	poly->degree = 10;

	count =	qi_poly_descartes (poly);

	mqi_log_printf ("Descartes ( "); mqi_hpoly_print(poly); mqi_log_printf (" ) = %u\n", count);	
	qi_assert ( count == result );
	
	return (EXIT_SUCCESS);
	
}

