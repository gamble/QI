#include "qi_elem.h"
#include "qi_assert.h"

static mqi_vect_t	pt1, pt2;
static mqi_coef_t	a, b, d, max;
static mqi_vect_t	k1, k2, k1p, k2p, acc;
static mqi_mat_t	quad;

static sshort pt1_data[4] = { -6, 9, 12, 117 };
static sshort pt2_data[4] = { 588, -882, -1176, -11466 }; /* Equivalent */
static sshort pt3_data[4] = { 36, -54, -72, 707 };	  /* Not equivalent (707 instead of 702) */
static sshort vect_data[10] =  { 5, -1982, 77, 12, 44, 90, 23, 45, 12, -66 };
static sshort vect_data2[10] = { 5, -182, 76, 12, 44, 90, 26, 45, 12, -66 };

void free_memory (void) {

	mqi_vect_clear (pt1);
	mqi_vect_clear (pt2);
	mqi_coef_clear (a);
	mqi_coef_clear (b);
	mqi_coef_clear (d);
	mqi_coef_clear (max);
	mqi_vect_clear (k1);
	mqi_vect_clear (k2);
	mqi_vect_clear (k1p);
	mqi_vect_clear (k2p);
	mqi_vect_clear (acc);
	mqi_mat_clear  (quad);
	mqi_log_printf ("\n");

}

int main (void) {

	atexit (free_memory);

	mqi_vect_init_sshort (pt1, 4);
	mqi_vect_init_sshort (pt2, 4);

	mqi_vect_init_sshort (k1, 4);
	mqi_vect_init_sshort (k2, 4);
	mqi_vect_init_sshort (k1p, 4);
	mqi_vect_init_sshort (k2p, 4);
	mqi_vect_init_sshort (acc, 4);

	mqi_coef_init_sshort(a);
	mqi_coef_init_sshort(b);
	mqi_coef_init_sshort(d);
	mqi_coef_init_sshort(max);
	
	mqi_vect_set_all_sshort (pt1, pt1_data, 4);
	mqi_vect_set_all_sshort (pt2, pt2_data, 4);

	qi_assert ( qi_point_equals (pt1, pt2) );

	mqi_vect_set_all_sshort (pt2, pt3_data, 4);

	qi_assert ( ! qi_point_equals (pt1, pt2) );	

	mqi_coef_set_schar (max, 100);
	
	mqi_coef_randomize (a, max);
	mqi_coef_randomize (b, max);
	mqi_coef_randomize (d, max);
	mqi_mat_randomize  (k1, max);
	mqi_mat_randomize  (k2, max);

	mqi_vect_mul_coef (acc, k1, a);
	mqi_vect_mul_coef (k1p, k2, b);
	mqi_vect_mul_coef (k1p, k1p, d);
	mqi_vect_sub	  (k1p, acc, k1p);

	mqi_vect_mul_coef (acc, k2, a);
	mqi_vect_mul_coef (k2p, k1, b);
	mqi_vect_sub	  (k2p, acc, k2p);


	qi_assert ( qi_are_equal (k1, k2, k1p, k2p, d) );

	mqi_vect_clear (pt1);
	mqi_vect_init_sshort (pt1, 10);

	mqi_vect_set_all_sshort (pt1, vect_data, 10);
	
	qi_vect_to_matrix (quad, pt1);

	mqi_log_printf ("Vector: "); mqi_vect_print(pt1); mqi_log_printf (" to matrix:\n");
	mqi_mat_print (quad);

	mqi_vect_clear (pt1);

	qi_mat_to_vect (pt1, quad);

	mqi_log_printf ("Matrix: "); mqi_mat_print(quad); mqi_log_printf ("\nto vector: "); mqi_vect_print(pt1); mqi_log_printf ("\n");
	
	mqi_vect_clear (pt1);
	mqi_mat_clear  (quad);
	mqi_vect_init_sshort (pt1, 10);

	mqi_vect_set_all_sshort (pt1, vect_data2, 10);
	
	qi_vect_to_matrix (quad, pt1);

	mqi_log_printf ("Vector: "); mqi_vect_print(pt1); mqi_log_printf (" to matrix:\n");
	mqi_mat_print (quad);

	mqi_vect_clear (pt1);

	qi_mat_to_vect (pt1, quad);

	mqi_log_printf ("Matrix: "); mqi_mat_print(quad); mqi_log_printf ("\nto vector: "); mqi_vect_print(pt1); mqi_log_printf ("\n");
	
	return (EXIT_SUCCESS);
	
}

