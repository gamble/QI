#include "qi_elem.h"
#include "qi_assert.h"

#define N_DRAWS		10000
#define MAX_VAL		10000

static mqi_coef_t	random_coef, average, max;
static mqi_coef_t	a, b, c;

void free_memory (void) {
	
	mqi_coef_clear (a);
	mqi_coef_clear (b);
	mqi_coef_clear (c);
	mqi_coef_clear (random_coef);
	mqi_coef_clear (average);
	mqi_coef_clear (max);
	mqi_log_printf ("\n");
	
}

int main (void) {
	
	unsigned short k;
	int sign;
	
	atexit (free_memory);
	
	mqi_coef_init_sshort(a);
	mqi_coef_init_sshort(b);
	mqi_coef_init_sshort(c);
	mqi_coef_init_sshort(random_coef);
	mqi_coef_init_sshort(average);
	mqi_coef_init_sshort(max);
	
	mqi_coef_set_schar (average , 0);
	mqi_coef_set_sshort(max, MAX_VAL);
	
	for ( k = 0; k < N_DRAWS; k++ ) { 
		
		mqi_coef_randomize (a, max);
		/*mqi_log_printf ("\nDraw number %u: ", k); mqi_coef_print(a);*/
		mqi_coef_add	  (average, average, a);
		
	}
	
	mqi_coef_set_sshort (max, (sshort)N_DRAWS);

	mqi_coef_divexact   (average, average, max);

	mqi_log_printf ("\nAverage (must be close to 0) : ");
	mqi_coef_print (average);
	mqi_log_printf ("\n");

	/** I suppose that the probability to have an average outside of [-10,+10] is near zero */
	qi_assert ( mqi_coef_cmp_schar (average, -10) > 0 && mqi_coef_cmp_schar (average, 10) < 0 );

	mqi_coef_set_schar (a, -3);
	mqi_coef_set_schar (b, -7);
	mqi_coef_set_sshort(c, 64);
	
	sign = qi_coef_sign (a, b, c);
	
	qi_assert ( sign < 0 );

	mqi_coef_set_schar (a, 60);

	sign = qi_coef_sign (a, b, c);

	qi_assert ( sign > 0 );

	mqi_coef_set_schar (a, 32);

	sign = qi_coef_sign (a, b, c);

	qi_assert ( sign < 0 );

	mqi_coef_set_schar (a, 0);
	sign = qi_coef_sign (a, b, c);
	
	qi_assert ( sign < 0 );

	mqi_coef_set_schar (a, 70);
	mqi_coef_set_schar (b, 0);
	sign = qi_coef_sign (a, b, c);

	qi_assert ( sign > 0 );
	
	return (EXIT_SUCCESS);
	
}

