#include "qi_elem.h"
#include "qi_assert.h"

#define N_DRAWS	10000

static sshort  mat_data[4][4] = {

        { 6, -12, 36, -7   },
        { -12, 36, -72, 49 },
        { 9, 48, 144, 21   },
        { 6, -12, 36, -7   }

};

static sshort  mat_data2[4][4] = {
        
        { 1, 2, 0, 0            },
        { 70, 80, -66, 12       },
        { 18, 24, 36, 0         },
        { 1, 1, 0, 0            }
};

static sshort  mat_data3[4][4] = {
        
        { 99, 100, 0, -78       },
        { 50, 67, 89, -221      }, 
        { 72, -769, 98          },
        { 0, 0, 0, -111         }
};

static sshort symmat_data[3][3] = {

        {0,  2,  -1  },
        { 2,  4,   1 },
        { -1, 1,   0 }

};

static sshort symmat_data2[3][3] = {

        { 98, 12, 0 },
        { 12, -5, -72},
        { 0, -72, 47}

};

static sshort gauss_data[4][4] = {

	{0, -3, 5, 4 },
	{-3, 3, 7, 8 },
	{5, 7, 0, 2 },
	{4, 8, 2, 9 }

	/*{ 5, 0, -2, 0 },
	{ 0, 2, -5, 3 },
	{ -2, -5, 1, 0 },
	{ 0, 3, 0, 1 }*/

	
};

static signed char inert30_data[2] = { 3, 0 };
static signed char inert03_data[2] = { 0, 3 };
static signed char inert22_data[2] = { 2, 2 };
static signed char inert31_data[2] = { 3, 1 };
static signed char inert13_data[2] = { 1, 3 };

static signed char point1_data[4]  = { 2, -7, 12, -49 };
static signed char point2_data[4]  = { -77, 5, 90, 13 };

static mqi_mat_t	mat, reduced, q1, q2, res, sum, can, tm, concat;
static mqi_vect_t	inertia, inert30,inert03,inert22,inert31,inert13;
static mqi_vect_t	point1, point2;
static mqi_coef_t	D, det, N;
static unsigned short	rank, rank1, rank2, ncols, ncols2;
static unsigned char	inertia_nrc;

void free_memory (void) {

	mqi_mat_clear 	(mat);
	mqi_mat_clear	(reduced);
	mqi_mat_clear	(q1);
	mqi_mat_clear	(q2);
	mqi_mat_clear	(res);
	mqi_mat_clear	(sum);
	mqi_vect_clear	(inertia);
	mqi_vect_clear	(inert30);
	mqi_vect_clear	(inert03);
	mqi_vect_clear	(inert22);
	mqi_vect_clear	(inert31);
	mqi_vect_clear	(inert13);
	mqi_vect_clear  (point1);
	mqi_vect_clear  (point2);
	mqi_coef_clear	(D);
	mqi_coef_clear  (det);
	mqi_coef_clear	(N);
	
	mqi_log_printf ("\n");
	
}

int main (void) {

	unsigned short k, l;
	
	atexit (free_memory);
	
	mqi_mat_init_mpz_t	(mat, 4, 4);
	mqi_mat_init_cpy	(reduced, mat);
	mqi_mat_init_slong	(sum, 4, 4);
	mqi_mat_init_mpz_t	(tm, 4, 4);
	mqi_mat_init_mpz_t	(can, 4, 4);

	mqi_vect_init_schar	(inertia, 2);
	
	mqi_coef_init_slong	(N);
	mqi_coef_set_slong	(N, N_DRAWS);
	
#define initvect(v)					\
	mqi_vect_init_schar	(v, 2);			\
	mqi_vect_set_all_schar	(v, v##_data, 2);
	
	initvect(inert30)
	initvect(inert03)
	initvect(inert22)
	initvect(inert31)
	initvect(inert13)

	mqi_vect_init_schar (point1, 4);
	mqi_vect_init_schar (point2, 4);
	mqi_vect_set_all_schar (point1, point1_data, 4);
	mqi_vect_set_all_schar (point2, point2_data, 4);
	
	/** 1 */
	mqi_mat_set_all_sshort	(mat, &mat_data[0][0]);
	qi_mat_inertia (inertia, mat);
	mqi_log_printf ("Matrix: \n");
	mqi_mat_print  (mat);
	mqi_log_printf ("\nInertia: "); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert30) == 1 );
	
	qi_mat_signed_inertia (inertia, mat);
	mqi_log_printf ("Signed inertia: "); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert03) == 1 );
	
	rank = mqi_mat_rank (reduced, mat);
	qi_mat_inertia_known_rank (inertia, mat, rank);
	
	mqi_log_printf ("Inertia knowing the matrix rank (%u): ", rank); mqi_vect_print(inertia); mqi_log_printf ("\n");
	
	qi_assert ( mqi_vect_equals (inertia, inert30) == 1 );
	
	/** 2 */
	mqi_mat_set_all_sshort	(mat, &mat_data2[0][0]);
	qi_mat_inertia (inertia, mat);
	mqi_log_printf ("\nMatrix: \n");
	mqi_mat_print  (mat);
	mqi_log_printf ("\nInertia: "); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert22) == 1 );
	
	qi_mat_signed_inertia (inertia, mat);
	mqi_log_printf ("Signed inertia: "); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert22) == 1 );
	
	rank = mqi_mat_rank (reduced, mat);
	qi_mat_inertia_known_rank (inertia, mat, rank);
	
	mqi_log_printf ("Inertia knowing the matrix rank (%u): ", rank); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert22) == 1 );
	
	/** 3 */	
	mqi_mat_set_all_sshort	(mat, &mat_data3[0][0]);
	qi_mat_inertia (inertia, mat);
	mqi_log_printf ("\nMatrix: \n");
	mqi_mat_print  (mat);
	mqi_log_printf ("\nInertia: "); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert31) == 1 );
	
	qi_mat_signed_inertia (inertia, mat);
	mqi_log_printf ("Signed inertia: "); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert13) == 1 );
	
	rank = mqi_mat_rank (reduced, mat);
	qi_mat_inertia_known_rank (inertia, mat, rank);
	
	mqi_log_printf ("Inertia knowing the matrix rank (%u): ", rank); mqi_vect_print(inertia); mqi_log_printf ("\n");

	qi_assert ( mqi_vect_equals (inertia, inert31) == 1 );
	
	/** Inertia of non rational conic */
	mqi_mat_init_slong (q1, 3, 3);
	mqi_mat_init_slong (q2, 3, 3);
	mqi_coef_init_schar(D);
	
	mqi_mat_set_all_sshort (q1, &symmat_data[0][0]);
	mqi_mat_set_all_sshort (q2, &symmat_data2[0][0]);
	mqi_coef_set_schar     (D, 17);

	inertia_nrc = qi_mat_inertia_non_rational_conic (q1, q2, D);

	mqi_log_printf ("\nInertia of non rational conic defined by:\n");
	mqi_mat_print (q1);
	mqi_log_printf ("\n\n");
	mqi_mat_print (q2);
	mqi_log_printf ("\nAnd Delta = "); mqi_coef_print(D); mqi_log_printf ("\n");
	
	mqi_log_printf ("Equals: ");
	
	if ( inertia_nrc ) { mqi_log_printf ("[2,1]\n"); }
	else		   { mqi_log_printf ("[3,0]\n"); }
	
	qi_assert ( inertia_nrc == 1 );
	
	mqi_mat_set_all_sshort(mat, &mat_data[0][0]);
	qi_mat_singular (res, mat);
	mqi_log_printf ("Singular locus of:\n");
	mqi_mat_print (mat);
	mqi_log_printf ("\n\nGives:\n");
	mqi_mat_print (res);

	mqi_coef_init_slong (det);
	
	mqi_log_printf ("\nSending point: "); mqi_vect_print(point1); mqi_log_printf (" to infinity:\n");
	mqi_mat_clear (mat);

	/** Send to infinity */
	qi_mat_send_to_infinity (mat, point1);

	mqi_mat_print (mat);
	mqi_log_printf ("\n");

	/** Check: resulting matrix must have a non-nul determinant */
	mqi_mat_det (det, mat);
	
	qi_assert  ( mqi_coef_sign(det) != 0 );
	
	mqi_log_printf ("\nSending points: "); mqi_vect_print(point1); mqi_log_printf (" and "); mqi_vect_print(point2); mqi_log_printf(" to infinity:\n");
	mqi_mat_clear (mat);

	/** Send to zw */
	qi_mat_send_to_zw (mat, point1, point2);

	mqi_mat_print (mat);
	mqi_log_printf ("\n");

	/** Same check */
	mqi_mat_det (det, mat);

	qi_assert ( mqi_coef_sign(det) != 0 );


	/** Random initialization */
	mqi_coef_clear(D);
	
	mqi_coef_init_slong(D);
	mqi_coef_set_schar(D, 100);
	
	mqi_mat_clear(mat);
	mqi_mat_init_slong(mat, 4, 4);
	
	mqi_log_printf ("Random matrix draw (sum of %u draws):\n", N_DRAWS);

/*	for ( k = 0; k < N_DRAWS; k++ ) { */
			
		mqi_mat_randomize(mat, D);
		mqi_mat_add	(sum, sum, mat);
		
	/*}*/
	
	
	for ( k = 0; k < sum->n_rows; k++ ) {
		for ( l = 0; l < sum->n_cols; l++ ) {
			mqi_coef_divexact (MAT(sum,k,l), MAT(sum,k,l), N);
		}
	}
	
	mqi_mat_print (sum);
	mqi_log_printf ("\nCoefficients must be close to zero.\n");

#ifdef __USE_GMP
	/** Gauss elimination */
	mqi_mat_clear (mat);
	mqi_mat_init_mpz_t(mat, 4, 4);
	
	mqi_mat_set_all_sshort (mat, &gauss_data[0][0]);

	mqi_log_printf ("Gauss elimination of:\n");
	mqi_mat_print (mat);
	mqi_log_printf ("\nGives:\n");

	qi_mat_gauss (can, mat, tm); 

	mqi_mat_print (can);

	mqi_log_printf ("\nTransformation matrix:\n");
	
	mqi_mat_print (tm);

#else
	mqi_log_printf ("Gauss elimination test needs GMP support, as the coefficients may grow larger than a classical long integer.\n");
#endif

#ifdef __USE_GMP
	
	/** Linear intersection test */
	mqi_mat_clear (q1);
	mqi_mat_clear (q2);
	mqi_mat_clear (res);
		
	/** Draws a number of columns between 1 and 4 included */
	ncols = (unsigned short)(1.0f + 3.0f * rand() / RAND_MAX);
	mqi_mat_init_mpz_t (q1, 4, ncols);
	ncols2 = (unsigned short)(1.0f + 3.0f * rand() / RAND_MAX);
	mqi_mat_init_mpz_t (q2, 4, ncols2);

	mqi_coef_clear (D);
	mqi_coef_init_mpz_t (D);
	mqi_coef_set_schar (D, 100);

	mqi_mat_randomize (q1, D);
	mqi_mat_randomize (q2, D);
/*	qi_mat_linear_intersect (reduced, q1, q2);*/
	/*mqi_log_printf ("reduced->n_cols = %d\n", reduced->n_cols);*/
	mqi_log_printf ("Linear intersection between:\n");
	mqi_mat_print (q1);
	mqi_log_printf ("\nAnd\n");
	mqi_mat_print (q2);
	mqi_log_printf ("\nGives:\n");
/*	mqi_mat_print (reduced);*/


	return (EXIT_SUCCESS);	
	qi_assert ( reduced->n_cols == MAX(0, ncols + ncols2 - 4) );
	if ( reduced->n_cols > 0 ) {

		/** Calculate the rank of q1 and q2 */
		mqi_mat_clear (reduced);
		mqi_mat_init_mpz_t (reduced, q1->n_rows, q1->n_cols);
		rank1 = mqi_mat_rank (reduced, q1);

		mqi_mat_clear (reduced);
		mqi_mat_init_mpz_t (reduced, q2->n_rows, q2->n_cols);
		rank2 = mqi_mat_rank (reduced, q2);

		/** Appends a column of reduced to q1 and q2 */		
		mqi_mat_init_cpy (concat, q1);
		mqi_mat_clear	 (q1);
		mqi_mat_init_mpz_t (q1, concat->n_rows, concat->n_cols+1);
		mqi_mat_cpy	 (q1, concat);

		mqi_mat_clear (concat);

		mqi_mat_init_cpy (concat, q2);
		mqi_mat_clear	 (q2);
		mqi_mat_init_mpz_t (q2, concat->n_rows, concat->n_cols+1);
		mqi_mat_cpy	 (q2, concat);
	
		ncols = (unsigned short)(1.0f + 3.0f * rand() / RAND_MAX);
		mqi_mat_cpy_col (q1, reduced, ncols, q1->n_cols - 1);
		ncols = (unsigned short)(1.0f + 3.0f * rand() / RAND_MAX);
		mqi_mat_cpy_col (q2, reduced, ncols, q2->n_cols - 1);

		mqi_mat_clear (reduced);
		mqi_mat_init_mpz_t (reduced, q1->n_rows, q1->n_cols);

		qi_assert ( mqi_mat_rank (reduced, q1) == rank1 );

		mqi_mat_clear (reduced);
		mqi_mat_init_mpz_t (reduced, q2->n_rows, q2->n_cols);

		qi_assert ( mqi_mat_rank (reduced, q2) == rank2 );

	}

#else
	mqi_log_printf ("Linear intersection test needs GMP support, as the coefficients may grow larger than a classical long integer.\n");
#endif

	return (EXIT_SUCCESS);

}

