#ifndef _qi_assert_h_
#define _qi_assert_h_

#include <stdlib.h>	/** For "exit" */

/** A replacement of the standard "assert" function,
 *  which calls "exit" instead of "abort", and allowing
 *  previously registered function with atexit() to
 *  be executed properly. */

/*#ifndef NDEBUG*/

/** Assertions should never be desactivated in libqi, they're
 *  used to check test validities. */
#define qi_assert(test) \
	if( !(test) ) 	 \
		{ printf( "### Assertion failed in file %s, line %d : " #test "\n", __FILE__, __LINE__ ); exit (EXIT_FAILURE); }
/*#else
#define qi_assert(test) do{}while(0)
#endif*/

#endif

