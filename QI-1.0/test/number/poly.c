#include "qi_assert.h"
#include "qi_number.h"

static schar		coefs[5]	= { 7, 21, -35, 42, 70 };
static schar	        scoefs[5] 	= { 1, 3,  -5,  6,  10 };

static schar		hcoefs1[3][3] 	= {

	{ 7, 0, -49  },
	{ 14, -21, 0 },
	{ 0, 0, 70   }
	
};

static schar		hcoefs2[3][3] 	= {

	{ -35, 0, 0 },
	{ 0, 0, 0   },
	{ 28, -42, -63 }

};

static mqi_hpoly_t	hpoly1[3];
static mqi_hpoly_t	hpoly2[3];

static mqi_hpoly_t	poly, poly2, spoly, spoly2;
static mqi_hhpoly_t	hhpoly1, hhpoly2, hhpoly;
static mqi_coef_t	coef;

void free_memory (void) {

	unsigned short k;
	
	mqi_coef_clear  (coef);
	mqi_hpoly_clear (poly);
	mqi_hpoly_clear (poly2);
	mqi_hpoly_clear (spoly);

	mqi_hhpoly_clear(hhpoly1);
	mqi_hhpoly_clear(hhpoly2);
	mqi_hhpoly_clear(hhpoly);

	for ( k = 0; k < 3; k++) {
		mqi_hpoly_clear (hpoly1[k]);
		mqi_hpoly_clear (hpoly2[k]);
	}

	mqi_log_printf ("\n");

}

int main (void) {

	unsigned short k;

	atexit (free_memory);
	
	/** ***** */
	/** hpoly */
	/** ***** */
	mqi_coef_init_schar  (coef);
	mqi_hpoly_init_schar (poly, 10);
	mqi_hpoly_init_schar (poly2, 10);
	mqi_hpoly_init_schar (spoly, 10);
	mqi_hpoly_init_schar (spoly2, 10);
	
	poly->degree  = 6;
	spoly->degree = 6;

	mqi_hpoly_set_all_schar (poly, coefs, 5);
	mqi_hpoly_set_all_schar (spoly, scoefs, 5);
	
	mqi_log_printf ("Optimizing: "); mqi_hpoly_print (poly);
	qi_hpoly_optimize (poly);
	mqi_log_printf ("\nGives: ");    mqi_hpoly_print (poly);
	
	qi_assert ( mqi_hpoly_equals (poly, spoly) == 1 );

	mqi_hpoly_zero (poly);
	mqi_hpoly_zero (spoly);
	mqi_hpoly_set_all_schar (poly, coefs, 3);
	mqi_hpoly_set_all_schar (poly2, &coefs[3], 2);
	mqi_hpoly_set_all_schar (spoly, scoefs, 3);
	mqi_hpoly_set_all_schar (spoly2, &scoefs[3], 2);

	poly->degree   = 4;
	poly2->degree  = 3;
	spoly->degree  = 4;
	spoly2->degree = 3;

	qi_hpoly_pseudo_discriminant (coef, spoly);

	mqi_log_printf ("\nPseudo discriminant of "); mqi_hpoly_print(spoly); mqi_log_printf (" = "); mqi_coef_print(coef);
	mqi_log_printf ("\n");
	
	mqi_log_printf ("Optimizing: ("); mqi_hpoly_print(poly); mqi_log_printf (", "); mqi_hpoly_print (poly2); mqi_log_printf (") = \n");

	qi_hpoly_optimize_gcd (poly, poly2);
	mqi_log_printf ("("); mqi_hpoly_print(poly); mqi_log_printf (", "); mqi_hpoly_print (poly2); mqi_log_printf (")\n");

	qi_assert ( mqi_hpoly_equals (poly, spoly)   == 1 );
	qi_assert ( mqi_hpoly_equals (poly2, spoly2) == 1 );

	
	/** ****** */
	/** hhpoly */
	/** ****** */
	for ( k = 0; k < 3; k++ ) {
		mqi_hpoly_init_schar (hpoly1[k], 10);
		mqi_hpoly_init_schar (hpoly2[k], 10);
		mqi_hpoly_set_all_schar (hpoly1[k], hcoefs1[k], 3);
		mqi_hpoly_set_all_schar (hpoly2[k], hcoefs2[k], 3);
		hpoly1[k]->degree = 3;
		hpoly2[k]->degree = 3;
	}

	mqi_hhpoly_init_schar (hhpoly1, 10);
	mqi_hhpoly_init_schar (hhpoly2, 10);
	
	hhpoly1->degree = 3;
	hhpoly2->degree = 3;
	
	mqi_hhpoly_set_all (hhpoly1, hpoly1, 3);
	mqi_hhpoly_set_all (hhpoly2, hpoly2, 3);

	mqi_hhpoly_init_cpy (hhpoly, hhpoly1);
	
	mqi_log_printf ("Primitive part of: "); mqi_hhpoly_print (hhpoly1); mqi_log_printf (" : \n");
	mqi_hhpoly_content (coef, hhpoly1);
	mqi_log_printf ("Content: "); mqi_coef_print(coef); mqi_log_printf ("\n");
	
	qi_hhpoly_pp (hhpoly, hhpoly1, coef);
	
	mqi_hhpoly_print (hhpoly);
	mqi_log_printf ("\n");

	mqi_log_printf ("Optimizing: "); mqi_hhpoly_print (hhpoly1); mqi_log_printf ("\n");
	mqi_log_printf ("And:	     "); mqi_hhpoly_print (hhpoly2); mqi_log_printf ("\n");
	mqi_log_printf ("With the gcd of their coefficients, gives:\n");
	
	qi_hhpoly_optimize_gcd (hhpoly1, hhpoly2);
	
	mqi_hhpoly_print (hhpoly1);
	mqi_log_printf ("\n");
	mqi_hhpoly_print (hhpoly2);	
	
	return (EXIT_SUCCESS);
	
}

