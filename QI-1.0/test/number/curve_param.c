#include "qi_number.h"
#include "qi_assert.h"

static mqi_curve_param_t cp, cp2, cp3, cp4, cptmp, cptmp2, cptmp3, cptmp4;
static mqi_hpoly_t   p[4];
static mqi_hpoly_t   delta1, delta2;
static mqi_coef_t    coef;

static sshort coefs[4][3] = { 
	{ -8, 24, 16    }, 	
	{ 8, 40, 32     },
	{ -80, 88, -48  },
	{ 40, -56, -16  }
};

static sshort coefs2[4][3] = {
	{ -4, 8, 8 	},
	{ 16, -20, -24 	},
	{ 44, - 40, 44 	},
	{ 12, 32, 36 	}
};

static sshort delta_coefs1[5] = { 1, -3, 5, -7, 4   };
static sshort delta_coefs2[5] = { 5, -6, 9, 11, -19 };

static unsigned short k;

void free_memory (void) {

	mqi_poly_clear (p[0]);
	mqi_poly_clear (p[1]);
	mqi_poly_clear (p[2]);
	mqi_poly_clear (p[3]);
	
	mqi_curve_param_clear (cp);
	mqi_curve_param_clear (cp2);
	mqi_curve_param_clear (cp3);
	mqi_curve_param_clear (cp4);
	mqi_curve_param_clear (cptmp);
	mqi_curve_param_clear (cptmp2);
	mqi_curve_param_clear (cptmp3);
	mqi_curve_param_clear (cptmp4);
	
	mqi_poly_clear (delta1);
	mqi_poly_clear (delta2);
	
	mqi_coef_clear (coef);
	
	mqi_log_printf ("\n");

}

int main (void) {

	atexit (free_memory);

	mqi_hpoly_init_sshort (p[0], 5);
	mqi_hpoly_init_sshort (p[1], 5);
	mqi_hpoly_init_sshort (p[2], 5);
	mqi_hpoly_init_sshort (p[3], 5);

	for ( k = 0; k < 4; k++ ) { 
		mqi_poly_set_all_sshort (p[k], coefs[k], 3);
		p[k]->degree = 2;
	}

	mqi_coef_init_sshort (coef);
	
	mqi_curve_param_init (cp, 4);
	mqi_curve_param_set_all (cp, p);
	mqi_curve_param_init_cpy (cp3, cp);
	mqi_curve_param_init_cpy (cp4, cp);
	
	mqi_log_printf ("Optimizing: ");	
	mqi_curve_param_print (cp);
	
	qi_curve_param_optimize (cp);

	mqi_log_printf ("\nGives:");
	mqi_curve_param_print (cp);
	
	mqi_coef_set_sshort (coef, 8);
	mqi_curve_param_divexact_coef (cp4, cp3, coef);

	qi_assert ( mqi_curve_param_equals (cp, cp4) );
	
	for ( k = 0; k < 4; k++ ) { 
		mqi_poly_set_all_sshort (p[k], coefs2[k], 3);
	}

	mqi_curve_param_init (cp2, 4);
	mqi_curve_param_set_all (cp2, p);
	mqi_curve_param_cpy (cp4, cp2);

	mqi_curve_param_cpy (cp, cp3);
	
	mqi_coef_set_sshort (coef, 4);
	
	mqi_curve_param_divexact_coef (cp3, cp,  coef);
	mqi_curve_param_divexact_coef (cp4, cp2, coef);
	
	mqi_log_printf ("\nOptimizing:\n");
	mqi_curve_param_print (cp);
	mqi_log_printf ("\nAnd\n");
	mqi_curve_param_print (cp2);
	mqi_log_printf ("\nGives:\n");

	qi_curve_param_optimize_gcd (cp, cp2);

	mqi_curve_param_print (cp);
	mqi_log_printf ("\n");
	mqi_curve_param_print (cp2);

	qi_assert ( mqi_curve_param_equals (cp, cp3) == 1 );
	qi_assert ( mqi_curve_param_equals (cp2, cp4) == 1 );

	mqi_coef_set_sshort  (coef, 3);

	mqi_curve_param_mul_coef (cp, cp, coef);
	mqi_curve_param_cpy (cp2, cp);
	mqi_curve_param_cpy (cp3, cp);
	mqi_curve_param_cpy (cp4, cp);
	
	mqi_coef_set_sshort (coef, 2);
	mqi_curve_param_mul_coef (cp2, cp2, coef);
	mqi_coef_set_sshort (coef, 3);
	mqi_curve_param_mul_coef (cp3, cp3, coef);
	mqi_coef_set_sshort (coef, 4);
	mqi_curve_param_mul_coef (cp4, cp4, coef);

	mqi_log_printf ("\nConic optimization:\n");
	mqi_curve_param_print (cp); mqi_log_printf ("\n");
	mqi_curve_param_print (cp2); mqi_log_printf ("\n");
	mqi_curve_param_print (cp3); mqi_log_printf ("\n");
	mqi_curve_param_print (cp4); mqi_log_printf ("\n");

	mqi_log_printf ("Gives:\n");

	qi_curve_param_optimize_conic (cp, cp2, cp3, cp4);

	mqi_curve_param_print (cp); mqi_log_printf ("\n");
	mqi_curve_param_print (cp2); mqi_log_printf ("\n");
	mqi_curve_param_print (cp3); mqi_log_printf ("\n");
	mqi_curve_param_print (cp4); mqi_log_printf ("\n");

	mqi_hpoly_init_sshort (delta1, 10);
	mqi_hpoly_init_sshort (delta2, 10);

	mqi_hpoly_set_all_sshort (delta1, delta_coefs1, 5);
	mqi_hpoly_set_all_sshort (delta2, delta_coefs2, 5);

	delta1->degree = 4;
	delta2->degree = 4;

	mqi_coef_set_sshort (coef, 36);

	mqi_hpoly_mul_coef (delta1, delta1, coef);
	mqi_hpoly_mul_coef (delta2, delta2, coef);
	mqi_curve_param_mul_coef (cp, cp, coef);
	mqi_curve_param_mul_coef (cp2, cp2, coef);
	mqi_curve_param_mul_coef (cp3, cp3, coef);
	mqi_curve_param_mul_coef (cp4, cp4, coef);
	
	mqi_log_printf ("\nSmooth quartic optimization:\n");
	mqi_curve_param_print (cp); mqi_log_printf ("\n");
	mqi_curve_param_print (cp2); mqi_log_printf ("\n");
	mqi_curve_param_print (cp3); mqi_log_printf ("\n");
	mqi_curve_param_print (cp4); mqi_log_printf ("\n");
	mqi_log_printf ("Delta1: ");
	mqi_hpoly_print (delta1);
	mqi_log_printf ("\nDelta2: ");
	mqi_hpoly_print (delta2);
		
	mqi_log_printf ("\nGives:\n");

	mqi_curve_param_init_cpy (cptmp, cp);
	mqi_curve_param_init_cpy (cptmp2, cp2);
	mqi_curve_param_init_cpy (cptmp3, cp3);
	mqi_curve_param_init_cpy (cptmp4, cp4);
	
	qi_curve_param_optimize_smooth_quartic (cp, cp2, cp3, cp4, delta1, delta2);

	mqi_curve_param_print (cp); mqi_log_printf ("\n");
	mqi_curve_param_print (cp2); mqi_log_printf ("\n");
	mqi_curve_param_print (cp3); mqi_log_printf ("\n");
	mqi_curve_param_print (cp4); mqi_log_printf ("\n");
	mqi_log_printf ("Delta1: ");
	mqi_hpoly_print (delta1);
	mqi_log_printf ("\nDelta2: ");
	mqi_hpoly_print (delta2);

	mqi_curve_param_mul_coef (cp, cp, coef);
	qi_assert ( mqi_curve_param_equals (cp, cptmp) == 1 );
	
	mqi_curve_param_mul_coef (cp2, cp2, coef);
	qi_assert ( mqi_curve_param_equals (cp2, cptmp2) == 1 );

	mqi_coef_set_sshort (coef, 6);
	mqi_curve_param_mul_coef (cp3, cp3, coef);
	qi_assert ( mqi_curve_param_equals (cp3, cptmp3) == 1);

	mqi_curve_param_mul_coef (cp4, cp4, coef);
	qi_assert ( mqi_curve_param_equals (cp4, cptmp4) == 1);
	
	return (EXIT_SUCCESS);
	
}

