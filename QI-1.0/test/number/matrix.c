#include "qi_number.h"
#include "qi_assert.h"

/** A simplistic arithmetic test:
 *
 *  We calculate:
 *
 *  P = -M
 *
 *  Then:
 *  P+M
 *  P-M
 *  P*M
 *  P.*M
 *  (M)t
 *  lambda*M+mu*P
 *
 */
static mqi_mat_t 	A, B, C, D, _A, _B, _C, _D, tmp, tmp2;
static mqi_vect_t	v, _v;

static sshort v_data[2] 	= { -13, 27 };
static sshort v_optdata[2]	= { -78, 81 };

static sshort A_data[3][3] = {

	{ 5,  216,  156 },
	{ 25, 144,  39  },
	{ -45,-36,  -78 }
	
};

static sshort B_data[3][3] = {

	{ -25, 12,  13  },
	{ 75,  -24, 169 },
	{ 225, 144, -26 }
	
};

static sshort C_data[3][4] = {

	{ 6,	-12,	36 , -7},
	{ -12,	36,	-72, 49},
	{ 9,	48,	144, 21},
	
};

static sshort D_data[3][4] = {

	{ 9, 	-24,	108, 56},
	{ 21,	60,	216, 70},
	{ 30,	72,	324, 77}
};

static sshort C_opt3_data[3][4] = {

	{ 2, -1, 6,   1   },
	{ -4, 3, -12, -7 },
	{ 3,  4, 24,  -3  }

};

static sshort C_opt4_data[3][4] = {

	{ 2, -1, 6,   -1 },
	{ -4, 3, -12,  7 },
	{ 3,  4, 24,   3 }

};

static sshort D_opt4_data[3][4] = {

	{ 3,  -2, 18, 8  },
	{ 7,  5,  36, 10 },
	{ 10, 6,  54, 11 }

};


static sshort A_opt_data[3][3] = {

	{ 1, -6,    4 },
	{ 5, -4,    1 },
	{ -9, 1, -2 }

};

static sshort A_opt2_data[3][3] = {

	{ 1,  18, 12 },
	{ 5,  12, 3  },
	{ -9, -3, -6 }

};

static sshort B_opt2_data[3][3] = {

	{ -5,  1,  1 },
	{ 15, -2, 13 },
	{ 45, 12, -2 }
		
};

void free_memory (void) {

	mqi_mat_clear (A);
	mqi_mat_clear (B);
	mqi_mat_clear (C);
	mqi_mat_clear (D);
	mqi_mat_clear (_A);
	mqi_mat_clear (_B);
	mqi_mat_clear (_C);
	mqi_mat_clear (_D);
	mqi_mat_clear (tmp);
	mqi_mat_clear (tmp2);
	mqi_vect_clear(v);
	mqi_vect_clear(_v);
	mqi_log_printf ("\n");

}

int main (void) {

	atexit (free_memory);
	
	
	mqi_mat_init_sshort (A,  3, 3);
	mqi_mat_init_sshort (B,  3, 3);
	mqi_mat_init_sshort (C,  3, 4);
	mqi_mat_init_sshort (D,  3, 4);
	mqi_vect_init_sshort(v,  2);
	
	mqi_mat_set_all_sshort (A, &A_data[0][0]);
	mqi_mat_set_all_sshort (B, &B_data[0][0]);
	mqi_mat_set_all_sshort (C, &C_data[0][0]);
	mqi_mat_set_all_sshort (D, &D_data[0][0]);
	
	mqi_mat_init_cpy (_A, A);
	mqi_mat_init_cpy (_B, B);
	mqi_mat_init_cpy (_C, C);
	mqi_mat_init_cpy (_D, D);
	
	mqi_mat_init_cpy (tmp, A);
	mqi_mat_init_cpy (tmp2, B);
	
	mqi_log_printf ("Optimizing:\n");
	mqi_mat_print  (A);
	mqi_log_printf ("\nGives:\n");

	mqi_mat_set_all_sshort (tmp, &A_opt_data[0][0]);	

	qi_mat_optimize (A);

	mqi_mat_print  (A);
	mqi_log_printf ("\n");

	qi_assert ( mqi_mat_equals (A, tmp) == 1 );

	mqi_mat_cpy (A, _A);

	mqi_mat_set_all_sshort (tmp, &A_opt2_data[0][0]);
	mqi_mat_set_all_sshort (tmp2, & B_opt2_data[0][0]);

	mqi_log_printf ("\nOptimizing 2:\n");
	mqi_mat_print (A);
	mqi_log_printf ("\nAnd\n");
	mqi_mat_print (B);
	mqi_log_printf ("\nGives:\n");
	
	qi_mat_optimize_2 (A, B);
	
	mqi_mat_print (A);
	mqi_log_printf ("\nAnd\n");
	mqi_mat_print (B);
	mqi_log_printf ("\n");
	
	qi_assert ( mqi_mat_equals (A, tmp) == 1  );
	qi_assert ( mqi_mat_equals (B, tmp2) == 1 );
	
	mqi_vect_set_all_sshort (v, &v_data[0], 2);
	mqi_vect_init_cpy (_v, v);
	
	mqi_mat_clear (tmp);
	mqi_mat_init_sshort (tmp, 3, 4);
	mqi_mat_set_all_sshort (tmp, &C_opt3_data[0][0]);
	
	mqi_log_printf ("\nOptimizing 3:\n");
	mqi_mat_print (C);
	mqi_log_printf ("\nWith line vector: ");
	mqi_vect_print(v);
	mqi_log_printf ("\nGives\n");

	qi_mat_optimize_3_update (C, v);

	mqi_mat_print(C);
	mqi_log_printf ("\nLine vector: ");
	mqi_vect_print(v);

	mqi_log_printf ("\n");
	
	mqi_vect_set_all_sshort (_v, &v_optdata[0], 2);
	
	qi_assert ( mqi_mat_equals (C, tmp) == 1 );
	qi_assert ( mqi_vect_equals (v, _v) == 1 );


	
	mqi_mat_cpy (A, _A);
	mqi_mat_cpy (B, _B);
	mqi_mat_cpy (C, _C);


	mqi_log_printf ("\nPartial load balancing of:\n");
	mqi_mat_print (C);
	mqi_log_printf ("\nGives:\n");

	qi_mat_load_balancing (C, 0);
	
	mqi_mat_print (C);
	mqi_log_printf ("\n");
	
	mqi_mat_cpy (C, _C);

	mqi_log_printf ("\nFull load balancing of:\n");
	mqi_mat_print (C);
	mqi_log_printf ("\nGives:\n");

	qi_mat_load_balancing (C, 1);
	
	mqi_mat_print (C);
	mqi_log_printf ("\n");

	mqi_mat_cpy (C, _C);
	
	mqi_log_printf ("\nOptimize 4 of:\n");
	mqi_mat_print (C);
	mqi_log_printf ("\nAnd\n");
	mqi_mat_print (D);
	mqi_log_printf ("\nGives:\n");
	
	qi_mat_optimize_4 (C, D);

	mqi_mat_print (C);
	mqi_log_printf ("\nAnd\n");
	mqi_mat_print (D);
	
	mqi_mat_clear(tmp2);
	mqi_mat_init_sshort (tmp2, 3, 4);

	mqi_mat_set_all_sshort (tmp, &C_opt4_data[0][0]);
	mqi_mat_set_all_sshort (tmp2, &D_opt4_data[0][0]);
	
	qi_assert ( mqi_mat_equals (C, tmp)  == 1 );
	qi_assert ( mqi_mat_equals (D, tmp2) == 1 );	
	
	return (EXIT_SUCCESS);

}

