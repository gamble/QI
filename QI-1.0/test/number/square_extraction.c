#include "qi_assert.h"
#include "qi_number.h"

static mqi_coef_t number;
static mqi_coef_t square;
static mqi_coef_t cofactor;

void free_memory (void) {

	mqi_coef_clear (number);
	mqi_coef_clear (square);
	mqi_coef_clear (cofactor);
	mqi_log_printf ("\n");

}

int main (void) {

	atexit (free_memory);

	mqi_coef_init_sshort (number);
	mqi_coef_init_sshort (square);
	mqi_coef_init_sshort (cofactor);

	mqi_coef_set_sshort  (number, 448);
	qi_coef_extract_square_factors (square, cofactor, number);
	
	mqi_coef_print (number);
	mqi_log_printf (" = ");
	mqi_coef_print (square);
	mqi_log_printf ("^2 * ");
	mqi_coef_print (cofactor);
	mqi_log_printf ("\n");
	
	qi_assert ( mqi_coef_cmp_sshort (square, 8) == 0 );
	qi_assert ( mqi_coef_cmp_sshort (cofactor, 7) == 0 );
	
	mqi_coef_set_sshort  (number, 1813);
	qi_coef_extract_square_factors (square, cofactor, number);

	mqi_coef_print (number);
	mqi_log_printf (" = ");
	mqi_coef_print (square);
	mqi_log_printf ("^2 * ");
	mqi_coef_print (cofactor);
	mqi_log_printf ("\n");
	
	qi_assert ( mqi_coef_cmp_sshort (square, 7) == 0 );
	qi_assert ( mqi_coef_cmp_sshort (cofactor, 37) == 0 );
	
	mqi_coef_set_sshort  (number, 1573);
	qi_coef_extract_square_factors (square, cofactor, number);

	mqi_coef_print (number);
	mqi_log_printf (" = ");
	mqi_coef_print (square);
	mqi_log_printf ("^2 * ");
	mqi_coef_print (cofactor);
	mqi_log_printf ("\n");
	
	qi_assert ( mqi_coef_cmp_sshort (square, 11) == 0 );
	qi_assert ( mqi_coef_cmp_sshort (cofactor, 13) == 0 );
	
	mqi_coef_set_sshort  (number, 4096);
	qi_coef_extract_square_factors (square, cofactor, number);

	mqi_coef_print (number);
	mqi_log_printf (" = ");
	mqi_coef_print (square);
	mqi_log_printf ("^2 * ");
	mqi_coef_print (cofactor);
	mqi_log_printf ("\n");
	
	qi_assert ( mqi_coef_cmp_sshort (square, 64) == 0 );
	qi_assert ( mqi_coef_cmp_sshort (cofactor, 1) == 0 );
	
	mqi_coef_set_sshort  (number, 23552);
	qi_coef_extract_square_factors (square, cofactor, number);

	mqi_coef_print (number);
	mqi_log_printf (" = ");
	mqi_coef_print (square);
	mqi_log_printf ("^2 * ");
	mqi_coef_print (cofactor);

	qi_assert ( mqi_coef_cmp_sshort (square, 32) == 0 );
	qi_assert ( mqi_coef_cmp_sshort (cofactor, 23) == 0 );
	
	return (EXIT_SUCCESS);
	
}

