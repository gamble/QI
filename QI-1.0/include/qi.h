#ifndef _qi_h_
#define _qi_h_

/** @file	qi.h
 *  @author	Julien CLEMENT, Sylvain PETITJEAN, Sylvain LAZARD
 *  @brief	Quadric Intersection's main intersection procedure.
 */

#include <mqi/mqi_mat.h>
#include <mqi/mqi_log.h>
#include "qi_number.h"
#include "qi_param.h"
#include "qi_inter.h"
#include "qi_bench.h"
#include "qi_settings.h"
#include "qi_backends.h"
/*#include "qi_parser.h"*/

/** Calculate the exact intersection of two quadrics with integer coefficients.
 *  It includes both the parametrization of the intersection curve and its type.
 *  All those information are grouped in a data structure with type <i>qi_inter_t</i>,
 *  which is initialized automatically (this is what "__INIT" means).
 *
 *  @note	"__IN" and "__INIT" are for documentation purpose only.
 *  		"__IN" is used to designate the inputs of the function.
 *
 *  @param	q1		Matricial form of the first quadric
 *  @param	q2		Matricial form of the second quadric
 *  @param	optimize	0	:	Do not try to optimize the output
 *  						parametrizations (faster)
 *  				1	:	Try to optimize the output
 *  						parametrizations (slower)
 *
 *  @see	qi_inter.h
 *  @see	qi_inter_t.h
 *  @see	qi_inter_t.c
 *
 */
void qi_intersect	(qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
                         int optimize);

#endif

