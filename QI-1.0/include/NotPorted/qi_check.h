#ifndef _qi_check_h_
#define _qi_check_h_

/** ******************************************************** */
/** Used to verify that the intersection results are correct */
/** ******************************************************** */

/** libmqi */
#include "mqi_mat.h"

/** QI */
#include "qi_param.h"

/** Checks if the computed param is ok by replugging in initial quadrics */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  surface_param <bigint> &p, ostream &s);

/** Checks if the computed param is ok by replugging in initial quadrics */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  curve_param <bigint> &p, ostream &s);

/** Checks if p1+sqrt(D)*p2 is ok */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  curve_param <bigint> &p1,  curve_param <bigint> &p2, 
		  bigint &D, ostream &s);

/** Checks if p1+sqrt(D1)*p2+sqrt(D2)*p3 is ok */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  curve_param <bigint> &p1,  curve_param <bigint> &p2, 
		  curve_param <bigint> &p3, 
		  bigint &D1,  bigint &D2, ostream &s);

/** Checks if param c1+sqrt(a)*c2+sqrt(b)*c3+sqrt(a*b)*c4 is ok */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  curve_param <bigint> &c1,  curve_param <bigint> &c2, 
		  curve_param <bigint> &c3,  curve_param <bigint> &c4,
 		  bigint &a,  bigint &b, ostream &s);

/** Checks if param c1+sqrt(a)*c2+(c3+sqrt(a)*c4)*sqrt(b+c*sqrt(a)) is ok */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  curve_param <bigint> &c1,  curve_param <bigint> &c2, 
		  curve_param <bigint> &c3,  curve_param <bigint> &c4,
		  bigint &a,  bigint &b,  bigint &c, ostream &s);

/** Checks if the computed param is ok by replugging in initial quadrics
    Second case, when instead we have a surface param and a polynomial */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  surface_param <bigint> &p, 
		  hom_polynomial <bigint>& pol, ostream &s);

/** Checks if the parameterization s1 + sqrt(det_q[1]) s2 of the quadric 22 q lies on q */
void check_param( bigint_matrix &q,  surface_param <bigint> &s1, 
		  surface_param <bigint> &s2,  bigint &delta, ostream &s);

/** Checks if the computed quartic param c is ok by replugging in initial quadrics
 * c = c1 + sqrt(d). c2 + eps. sqrt(Delta). (c3 + sqrt(d). c4)
 * Delta = Delta1 + sqrt(d). Delta2
 * Note if check_param is ok for c then it is also ok after replacing 
 * sqrt(d) by -sqrt(d) and sqrt(Delta) by -sqrt(Delta) */
void check_param( bigint_matrix &q1,  bigint_matrix &q2, 
		  curve_param <bigint> &c1,  curve_param <bigint> &c2, 
		  curve_param <bigint> &c3,  curve_param <bigint> &c4, 
		  bigint &delta,  hom_polynomial <bigint> &Delta1, 
		  hom_polynomial <bigint> &Delta2, ostream &s); 


#endif

