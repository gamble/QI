#ifndef _qi_inter_h_
#define _qi_inter_h_

/** @file	qi_inter.h
 *  @author	Julien CLEMENT, Sylvain PETITJEAN, Sylvain LAZARD
 *  @brief	Quadric intersection's subroutines triggered
 *  		depending on the number and multiplicity of the
 *  		roots of the determinential equation.
 *  @note	"__INIT" means that the corresponding object is initialized
 *  		inside the function. Only for documentation purpose.
 *  @note	"__IN" is also for documentation purpose.
 *  		It facilitates the idenfication of the inputs when it is not
 *  		straightforward.
 *  @note	If some arguments where specified to be "__IN" or "__INIT",
 *  		then the other arguments with no specifier are necesarily
 *  		<b>output</b> arguments.
 */

#include <mqi/mqi_log.h>
#include <qi_inter_t.h>
#include "qi_param.h"
#include "qi_uspensky.h"
#include "qi_utils.h"

/** @name Intersection when the determinential equation vanishes
 */
/*@{*/

void qi_inter_vanish_det (qi_inter_t __INIT rop, mqi_mat_t q1, mqi_mat_t q2,
			  mqi_hpoly_t det_p);
/*@}*/

/** @name Local routines used when the determinential equation vanishes
 */
/*@{*/

/** Conic and double line */
void __qi_inter_vanish_conic_double_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					 mqi_mat_t sing1,  mqi_mat_t sing2);

/** Line and triple line */
void __qi_inter_vanish_triple_and_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2, 
				       mqi_mat_t q,
				       mqi_mat_t q_other,  mqi_mat_t q_sing,  mqi_mat_t proj_mat,
				       mqi_vect_t sing_p0);

/** Quadruple line */
void __qi_inter_vanish_quadruple_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2, 
                                      mqi_mat_t q,
				      mqi_mat_t q_other,  mqi_mat_t q_sing,  mqi_mat_t proj_mat,
                                      mqi_vect_t sing_p0);

/** Two double lines */
void __qi_inter_vanish_two_double_lines (qi_inter_t __INIT rop, mqi_mat_t q1,  mqi_mat_t q2, 
                                        mqi_mat_t q,
					mqi_mat_t q_other,  mqi_mat_t q_sing,  mqi_mat_t proj_mat,
                                        mqi_vect_t sing_p0);

/** Two concurrent lines and one double line */
void __qi_inter_vanish_two_conc_lines (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2, 
                                      mqi_mat_t q,
				      mqi_mat_t q_other,  mqi_mat_t q_sing,  mqi_mat_t proj_mat,
                                      mqi_vect_t sing_p0,
				      mqi_coef_t in_q);

/** Four concurrent lines (over the complexes) */
void __qi_inter_vanish_four_concurrent_lines (qi_inter_t __INIT rop,  mqi_hpoly_t det_p3,  mqi_mat_t q1,  mqi_mat_t q2,
					     mqi_mat_t q1_r,  mqi_mat_t q2_r,
					     mqi_vect_t sing_p0,  mqi_mat_t proj_mat);


/** Four concurrent lines when we have found one (or two) rational pair(s) of planes */
void __qi_inter_vanish_four_concurrent_lines_ratplane (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
	   					      mqi_mat_t q1_r,  mqi_mat_t q2_r,
						      mqi_vect_t sing_p0,  mqi_mat_t proj_mat);

void __qi_inter_vanish_all_positive_divisors (mqi_vect_t __INIT div, mqi_prime_factorization_t f, unsigned long int maxfactor);
void __qi_inter_vanish_all_negative_divisors (mqi_vect_t __INIT div, mqi_prime_factorization_t f, unsigned long int maxfactor);
void __qi_inter_vanish_all_divisors	     (mqi_vect_t __INIT div, mqi_prime_factorization_t f, unsigned long int maxfactor);


/*@}*/

/** @name Intersection with no multiple roots
 */
/*@{*/
void qi_inter_no_mult (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
		       mqi_hpoly_t det_p,  mqi_hpoly_t det_p_orig);
/*@}*/

/** @name Intersection with one multiple root
 */
/*@{*/
void qi_inter_one_mult (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
			mqi_hpoly_t det_p,  mqi_hpoly_t det_p_orig,  mqi_hpoly_t gcd_p);
/*@}*/

/** @name Local routines used when there is one multiple root
 */
/*@{*/

/** Two lines crossing on a conic */
void __qi_inter_one_mult_conic_2lines_crossing (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					        mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing,
					        int type_flag);

/** Two skew lines and double line */
void __qi_inter_one_mult_two_skew_lines_double_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
						     mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing,
						     int type_flag);

/** Two double lines */
void __qi_inter_one_mult_two_double_lines (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					   mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing,
					   int s_e);

/** Cuspidal quartic */
void __qi_inter_one_mult_cuspidal_quartic (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					   mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing);

/** Two tangent conics */
void __qi_inter_one_mult_two_tangent_conics (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					     mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing,
                                             int img_plane);

/** Nodal quartic */
void __qi_inter_one_mult_nodal_quartic (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
				        mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing,
                                        mqi_coef_t delta_e,  int s_e,  int in_q);

/** Double conic */
void __qi_inter_one_mult_double_conic (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
				       mqi_mat_t q,  mqi_mat_t q_sing,  mqi_hpoly_t det_e);

/** Cubic and tangent line */
void __qi_inter_one_mult_cubic_tangent_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					     mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing);

/** Pair of non-rational planes */
/**  m1*[u v s] +/- sqrt(D)*m2*[u] (D != 0, 1)
  *  q_22_pos is a matrix of inertia 22 */
void __qi_inter_one_mult_secant_conics_non_rational_plane (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,  mqi_hpoly_t det_p,
							   mqi_mat_t q_22_pos,  mqi_coef_t D,
							   mqi_mat_t m1,  mqi_mat_t m2,
							   int rtype);

/** Two secant conics.
  * (when the pair of planes is real
  * and the conics cut in real space (s_e == 1) )
  * det_p = det_e * (a*x-b*y)^2, and delta_e is the discrimant of det_e.
  * q_other is a matrix that does not correspond to the double root of the pencil
  * (namely q1 or q2) q_other2 is matrix of inertia 22 (one exists) */
void __qi_inter_one_mult_secant_conics (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,  mqi_hpoly_t det_p,
				        mqi_mat_t q,  mqi_mat_t q_other,  mqi_mat_t q_sing,  mqi_coef_t delta_e,
				        int in_q);

/** Conic lying in a non-rational plane */
/**  The non-rational plane of the pencil are parameterized by one of the two planes
  *  m1*[u v s] +/- sqrt(D)*m2*[u] (D != 0)
  *  D is here already optimized under -o
  *  q is a matrix that does not correspond to the double root of the pencil (namely q1 or q2) */
void __qi_inter_one_mult_one_conic_non_rational_plane (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,  mqi_hpoly_t det_p,
						       mqi_mat_t q,  mqi_coef_t D,  mqi_mat_t m1,  mqi_mat_t m2_tmp);

/** Two secant conics */
/** When the pair of planes is real and the conics do not cut in real space (s_e == -1) */
/** det_p = det_e * (a*x-b*y)^2, and delta_e is the discrimant of det_e. */
/** q_other is a matrix that does not correspond to the double root of the pencil */
/** (namely q1 or q2) */
/** q_other2 is matrix of inertia 22 if one exists in the pencil (that is when delta_e>0) */
void __qi_inter_one_mult_secant_conics_no_sec (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					       mqi_hpoly_t det_p,  mqi_mat_t q,
					       mqi_mat_t q_other,  mqi_mat_t q_other2,  mqi_mat_t q_sing,
					       mqi_coef_t delta_e);

/*@}*/

/** @name Intersection with two multiple roots
 */
/*@{*/

void qi_inter_two_mult (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					    mqi_hpoly_t det_p,  mqi_hpoly_t det_p_orig,  mqi_hpoly_t gcd_p);
/*@}*/


/** @name Local routines used when there are two multiple roots
 */
/*@{*/

/** Conic and two lines not crossing */
void __qi_inter_two_mult_conic_2lines_not_crossing (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
						    mqi_mat_t qa,  mqi_mat_t qb,
						    int img_cone,  int s_e);

/** Cubic and secant line */
void __qi_inter_two_mult_cubic_and_secant_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					        mqi_mat_t qa,  mqi_mat_t qb);

/** Four skew lines */
void __qi_inter_two_mult_four_skew_lines_rational (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
						   mqi_mat_t qa,  mqi_mat_t qb);

/** Four skew lines when the real part is 2 points */
void __qi_inter_two_mult_four_skew_lines_2points_rational (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
							   mqi_mat_t qa_cp,  mqi_mat_t qb_cp,
							   int in_qa);

/** Four skew lines when the real part is four lines or two points, and the roots are non-rational */
void __qi_inter_two_mult_four_skew_lines_non_rational (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
						       mqi_mat_t sing,  mqi_coef_t d,
						       int s_e);

/*@}*/


/** @name Intersection with a (2,2) quadric
  *
  * Special procedures for the "generic" case algorithm, going through the param of
  * a quadric of intertia (2,2).
  *
  */
/*@{*/

/** Input : initial quadrics q1, q2
*          determinental equation of the pencil : det_p							<br>
*          a quadric 22 : q										<br>
*          case_flag  = 0 if the intersection is a smooth quartic, 2 real affinely finite		<br>
*                     = 1           "                              1 real affinely finite		<br>
*                     = 2           "                              2 real affinely infinite		<br>
*                     = 3 if intersection is a cubic and a line secant					<br>
*                     = 4           "                           non-secant				<br>
*                     = 5 if intersection is two real skew lines					<br>
*  Output : Compute the Parameterization s1 + sqrt(xi) s2 of a quadric 22 in the pencil			<br>
*            and the Associated biquadratic equation poly1 + sqrt(xi) poly2 = 0				<br>
*     Call smooth_quartic(...) or cubic_line(...) for computing the intersection curve			<br>
*/
void qi_inter_with_22 (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
		                               mqi_hpoly_t det_p,  mqi_mat_t q_cp,
					       int case_flag);

/*@}*/

/** Local routines for the intersection with a (2,2) quadric
 */
/*@{*/

/** Returns the max coefficient of the discriminant of the polynomial "p+sqrt(d)*q", after simplification */
void __qi_inter_with_22_h_of_discr4 (mqi_coef_t rop,  mqi_hpoly_t p,  mqi_hpoly_t q,  mqi_coef_t d);

/** No precision specified */
int  __qi_inter_with_22_disc_co ( mqi_hhpoly_t c1,  mqi_hhpoly_t c2,  mqi_coef_t xi,  unsigned short i);

/** Smooth quartic.
Input : initial quadrics q1, q2 (only for checking)
         Parameterization s1 + sqrt(xi) s2 of a quadric 22 in the pencil
         Associated biquadratic equation poly1 + sqrt(xi) poly2 = 0
           (if xi = 1 then s2 and poly2 are 0)
Output : compute of the parameterization of the intersection curve
     c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
 where Delta = Delta1 + sqrt(xi). Delta2
 (if xi = 1 then c2, c4, and Delta2 are 0) */
void __qi_inter_with_22_smooth_quartic (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
				        mqi_surface_param_t s1,  mqi_surface_param_t s2,
				        mqi_hhpoly_t poly1,  mqi_hhpoly_t poly2,
				        mqi_coef_t xi,  int case_flag);

/** Two real skew lines */
/* Input : initial quadrics q1, q2 (only for checking)
         Parameterization s1_cp + sqrt(xi) s2_cp of a quadric 22 in the pencil
         Associated biquadratic equation poly1 + sqrt(xi) poly2 = 0
          (if xi = 1 then s2_cp and poly2 are 0)
   Output : print of the parameterization of the intersection curves 
 (one curve_param for each of the two lines)
 c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
 where the coeff of the ci are linear and Delta = (Delta1 + sqrt(xi). Delta2) in R
 (if xi = 1 then c2, c4, and Delta2 are 0) */
void __qi_inter_with_22_two_real_skew_lines (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,
					     mqi_surface_param_t s1_cp,  mqi_surface_param_t s2_cp,
					     mqi_hhpoly_t poly1,  mqi_hhpoly_t poly2,  mqi_coef_t xi);

/** Cubic and line */
/* Compute the parameterization of a cubic and a line
 Input : initial quadrics q1, q2 (only for checking)
         Parameterization par of a quadric 22 in the pencil
         Associated biquadratic equation poly = 0
 Output : print of the parameterization of the intersection curves 
 (one curve_param for the cubic and one for the line) */
void __qi_inter_with_22_cubic_line (qi_inter_t __INIT rop,  mqi_mat_t q1,  mqi_mat_t q2,  mqi_surface_param_t par_cp,
				    mqi_hhpoly_t poly_cp,
				    int case_flag);


/*@}*/


/** @name Find a (2,2) quadric of the pencil and its parametrization
  */
/*@{*/

/** Input : initial quadrics q1, q2
 * determinental equation of the pencil : det_p
 * a quadric 22  q
 * Output : a quadric 22  quadric_sol and a point on it point_sol
 * the determinant a^2.b of the quadric 22 in the form [a,b] */
void qi_inter_find_quadric_22_and_point (mqi_mat_t quadric_sol, mqi_vect_t point_sol, mqi_coef_t det_R, mqi_vect_t det_sol,
				    	 mqi_mat_t __IN q1,  mqi_mat_t __IN q2,
				    	 mqi_hpoly_t __IN det_p,
				    	 mqi_mat_t __INq_cp);

/** 
 * Input:  a matrix q of inertia 2,2 
 * a point  on it and the determinant of q in the form [a,b] such that det(q)=a^2. b
 * Output : Parameterization of the quadric :   s1 + sqrt(det_q[1]) s2
 * If det_q[1]=1 then  s2=[0,0,0,0] */
void qi_inter_find_parametrization_quadric_22 (mqi_surface_param_t s1, mqi_surface_param_t s2,
					       mqi_mat_t __IN q_cp,
					       mqi_vect_t __IN point_on_q,
					       mqi_vect_t __IN det_q_cp,
					       mqi_coef_t __IN det_R);

/*@}*/

#endif

