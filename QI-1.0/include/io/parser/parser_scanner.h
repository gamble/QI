#ifndef _parser_scanner_h_
#define _parser_scanner_h_

/** ******* */
/** Scanner */
/** ******* */

/** Index of the current character in the input stream */
extern unsigned short  _current_symbol_index;

/** Update the index to the next/previous character of the stream */
#define CURRENT_SYMBOL_INDEX   _current_symbol_index
#define NEXT_SYMBOL_INDEX       do { CURRENT_SYMBOL_INDEX++; token_indice++; }while(0)
#define PREV_SYMBOL_INDEX       --CURRENT_SYMBOL_INDEX

/** Read the content of the stream at a given position. 
 *
 *      If the requested symbol is out of the bounds of the
 *
 *           input stream, return ``end of string'' ('\0', for an overflow) 
 *
 *                or the first character (for an underflow) */

#define scanner_get_current_symbol() \
  ((_current_symbol_index < input_length) ? (input_str[CURRENT_SYMBOL_INDEX]) : '\0')

#define scanner_get_next_symbol() \
  ((_current_symbol_index < input_length) ? (input_str[NEXT_SYMBOL_INDEX]) : '\0')

#define scanner_get_prev_symbol() \
  ((_current_symbol_index > 0) ? (input_str[PREV_SYMBOL_INDEX]) : input_str[0])

#endif

