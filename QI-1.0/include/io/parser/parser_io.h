#ifndef _parser_io_h_
#define _parser_io_h_

extern char * input_str;
extern char * error_str;
extern size_t input_length;

#define parser_set_input_stream(input_stream) \
                do { \
                     input_str = input_stream; \
                     input_length = strlen(input_stream); \
                }while(0)

#define parser_set_error_stream(error_stream) error_str = error_stream;

/** Passes the parser a filename instead of a char pointer.
 *  Uses open and mmap
 */
void parser_set_input_filename (const char *filename);

/** Same for the error */
void parser_set_error_filename (const char *filename);

#endif

