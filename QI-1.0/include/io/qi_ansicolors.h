#ifndef _qi_ansicolors_h_
#define _qi_ansicolors_h_

#include "config.h"

/** Pretty printing constants */
#ifdef COLOR_SHELL
#define FG_BOLD    "\033[1m"
#define FG_MAGENTA "\033[35;40m"
#define FG_NORMAL  "\033[0m"
#define FG_RED     "\033[31;40m"
#define FG_YELLOW  "\033[33;40m"
#else
#define FG_BOLD    ""
#define FG_MAGENTA ""
#define FG_NORMAL  ""
#define FG_RED     ""
#define FG_YELLOW  ""
#endif

#endif
