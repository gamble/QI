#ifndef _qi_backend_t_h_
#define _qi_backend_t_h_

#include <mqi/mqi_log.h>


/* [HELP] Characters having a special binding in some backends,
 * like the "union" symbol.
 * If you plan to add more symbols to this enumeration,
 * you simply have to adjust the indices and *always*
 * let "SYM_INFINITE" beeing the *last*  element.
 */
enum _qi_backend_sym_
 {
	SYM_REAL	= 0,
	SYM_COMPLEX	= 1,
	SYM_UNION	= 2,
	SYM_INFINITE	= 3
 };

typedef enum _qi_backend_sym_	qi_backend_sym;


/** List of the functions redefined in each backend.
 *  You can provide new elements in it but don't forget
 *  to alter the static initialization tables for each
 *  backend in the qi_backend.c implementation file.
 */
struct _qi_backend_t_
 {
	/* Headers and footers of the document. */
	void (*begin_document)(void);
	void (*end_document)(void);

	/* Change the text style */
	void (*style_default)(void);
	void (*style_math)(void);
	void (*style_info)(void);
	void (*exponent)(const char * const text);
	/* Handling of special symbols like union, intersection,
	 * infinity, etc. */
	void (*symbol)(qi_backend_sym symbol);

	/* Document layout */
	void (*newline)(void);
	void (*section)(const char * const text);
	void (*subsection)(const char * const text);
 };

typedef struct	_qi_backend_t_			qi_backend_t;

/* The static definition of a backend can be far easier
 * if you use exactly the same function name as the
 * function pointer in the data structure.
 * The only restriction is that you must declare
 * those functions as *static* to avoid collisions
 * between different backends.
 */
#define DEF_BACKEND(name)		\
	qi_backend_t backend_##name = 	\
	{				\
		begin_document,		\
		end_document,		\
		\
		style_default,		\
		style_math,		\
		style_info,		\
		exponent,		\
		symbol,			\
		\
		newline,		\
		section,		\
		subsection		\
		\
	}

#endif

