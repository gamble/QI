#ifndef _qi_param_h_
#define _qi_param_h_

/** @file	qi_param.h
 *  @author	Julien CLEMENT, Sylvain PETITJEAN, Sylvain LAZARD
 *  @brief	Parametrization of various geometrical entities,
 *  		such as lines, cones, conics, surfaces, and more.
 *  @note	"__INIT" means that the corresponding object is initialized
 *  		inside the function. Only for documentation purpose.
 *  @note	"__IN" is also for documentation purpose.
 *  		It facilitates the idenfication of the inputs when it is not
 *  		straightforward.
 *  @note	If some arguments where specified to be "__IN" or "__INIT",
 *  		then the other arguments with no specifier are necesarily
 *  		<b>output</b> arguments.
 */



#include "qi_elem.h"
#include "qi_settings.h"
/*#include "qi_legendre.h"*/

#include <mqi/mqi_surface_param.h>


/** @name Local routines
 */
/*@{*/

/** Parameterization by discriminant D = e^2-a*f > 0
  * The param is (p_trans + sqrt(D)*p_trans2) * [u^2 v^2 uv]
  * par = [u^2 v^2 uv] */
void _qi_param_conic_1                   (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_vect_t __IN nd, mqi_mat_t p_trans,
					  mqi_mat_t p_trans2, mqi_curve_param_t par);

/** Parameterization by -f*p > 0
  * Equation is (a*f-e^2)*(f*z+e*x+d*y)^2 + (x*(a*f-e^2)+y*(b*f-d*e))^2 + f*p*y^2 = 0
  * The param is (p_trans + sqrt(D)*p_trans2) * [u^2 v^2 uv]
  * par = [u^2 v^2 uv] */
void _qi_param_conic_2                   (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_coef_t __IN de,
					  mqi_coef_t __IN p3, mqi_vect_t __IN nd, mqi_vect_t __IN np,
					  mqi_mat_t p_trans, mqi_mat_t p_trans2, mqi_curve_param_t par);

/** Parameterization of the conic C1+sqrt(delta)*C2 with  discriminant D = (eg)
  * e^2-a*f > 0 
  * The param is :
  * (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 +
                sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv] 
  * par = [u^2 v^2 uv]
  * D = D[0] + sqrt(delta)*D[1] */
void _qi_param_conic_nr1                  (mqi_mat_t __IN C1, mqi_mat_t __IN C2, mqi_coef_t __IN delta,
					   mqi_vect_t D, mqi_vect_t __IN nd, mqi_mat_t p_trans0,
					   mqi_mat_t p_trans1, mqi_mat_t p_trans2,
					   mqi_mat_t p_trans3, mqi_curve_param_t par);

/** Parameterization by -f*p > 0
  * Equation is (a*f-e^2)*(f*z+e*x+d*y)^2+(x*(a*f-e^2)+y*(b*f-d*e))^2+f*p*y^2 = 0
  * The param is 
  * (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 +
                  sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv] 
  * par = [u^2 v^2 uv]
  * D = D[0] + sqrt(delta)*D[1] */
void _qi_param_conic_nr2                  (mqi_mat_t __IN C1, mqi_mat_t __IN C2,
					   mqi_coef_t __IN delta, mqi_vect_t D,
                                           mqi_vect_t __IN de, mqi_vect_t __IN p3, mqi_vect_t __IN nd,
					   mqi_vect_t __IN np, mqi_mat_t p_trans0, mqi_mat_t p_trans1,
					   mqi_mat_t p_trans2, mqi_mat_t p_trans3,
                                           mqi_curve_param_t par); 

/** Computes the parameterization of a non-singular conic when there is no rational
    point or we were unable to find a simple one. */
void _qi_param_conic_no_ratpoint         (mqi_mat_t __IN q, mqi_coef_t __INIT D,
					  mqi_mat_t p_trans, mqi_mat_t p_trans2, mqi_curve_param_t par,
                                          mqi_coef_t __IN dxyv, mqi_coef_t __IN dxzv, mqi_coef_t __IN dyzv,
					  mqi_coef_t __IN p1v, mqi_coef_t __IN p2v, mqi_coef_t __IN p3v);

/** Parametrization of conics without optimization. (cf old version: opt_level = 0).
 */
void _qi_param_conic_no_opt              (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_mat_t p_trans, mqi_mat_t p_trans2,
					  mqi_curve_param_t par);


/** Parameterizes a 2 x 2 matrix when the determinant is 0. */
void _qi_param_2x2sing                   (mqi_mat_t __IN q, mqi_mat_t m1);

/*@}*/



/** @name Surfaces
 */
/*@{*/

/** Outputs the parameterization of the quadric of inertia (2,2) of implicit
    equation "xy - zw = 0"  :  s = [u.t, v.s, u.s, v.t]  */
void qi_param_surface_22                (mqi_surface_param_t s);

/*@}*/


/** @name Conics
 */
/*@{*/

/** Computes the parameterization of a non-singular conic through a rational point
  * Conic should be in the plane (x,y,z) */
void qi_param_conic_through_ratpoint    (mqi_mat_t __IN q, mqi_vect_t __IN rat_point,
					 mqi_mat_t p_trans, mqi_curve_param_t p,
                                         mqi_vect_t l);
/** Computes the parameterization of a non-singular conic going through the point [0,0,1] */
void qi_param_conic_through_origin      (mqi_mat_t __IN q, mqi_mat_t p_trans, mqi_curve_param_t p, mqi_vect_t l);

/** Parametrizes conics when no rational point is known a priori,
  * but there may be some.
  * The param is (p_trans + sqrt(D)*p_trans2) * [u^2 v^2 uv], par = [u^2 v^2 uv] */
void qi_param_conic                     (mqi_mat_t __IN q, mqi_coef_t __INIT D,
					 mqi_mat_t p_trans, mqi_mat_t p_trans2, mqi_curve_param_t par);

/** Parametrizes a conic whose corresponding 3x3 matrix is 
  * C1 +sqrt(delta).C2 when no rational point is known
  * The param is
           (p_trans0 + sqrt(delta)*p_trans1 + sqrt(D)*p_trans2 
                      + sqrt(delta)*sqrt(D)*p_trans3) * [u^2 v^2 uv]
  * par = [u^2 v^2 uv]
  * D = D[0] + sqrt(delta)*D[1] */
void qi_param_conic_nr                  (mqi_mat_t __IN C1, mqi_mat_t __IN C2,
					 mqi_coef_t __IN delta, mqi_vect_t D, 
					 mqi_mat_t p_trans0, mqi_mat_t p_trans1,
					 mqi_mat_t p_trans2, mqi_mat_t p_trans3,
					 mqi_curve_param_t par);

/*@}*/


/** @name Cones
 */
/*@{*/

/** Computes the parameterization of a cone through a rational point
  *  Output is p_trans*[u*s^2 u*t^2 u*s*t v]
  *  s*l[0]+t*l[1] gives the line (sing,rat_point) on the cone */
void qi_param_cone_through_ratpoint     (mqi_mat_t __IN q, mqi_vect_t __IN sing, mqi_vect_t __IN rat_point,
					 mqi_mat_t __INIT p_trans, mqi_surface_param_t __INIT p, mqi_vect_t __IN l);

/** Computes the parameterization of a cone
  * Output is p_trans*[u*s^2 u*t^2 u*s*t v] + sqrt(D)*p_trans2*[u*s^2 u*t^2 u*s*t v] */
void qi_param_cone                      (mqi_mat_t __IN q, mqi_vect_t __IN sing, mqi_coef_t __INIT D,
					 mqi_mat_t __INIT p_trans, mqi_mat_t __INIT p_trans2,
                                         mqi_surface_param_t __INIT p);

/*@}*/


/** @name Lines
 */
/*@{*/

/** Computes the parameterization of a pair of lines in the plane
  * Output is m1*[u v] +/- sqrt(D)*m2*[u] if D is non zero
  * Output is m1*[u v] +/- m2*[u] otherwise */
void qi_param_line_pair                 (mqi_mat_t __IN q, mqi_vect_t __IN sing,
					 mqi_coef_t __INIT D, mqi_mat_t m1, mqi_mat_t m2);

/** Computes the parameterization of a double line
  * Output is m1*[u u] */
void qi_param_line_double               (mqi_mat_t __IN q, mqi_mat_t __IN sing, mqi_mat_t m1);

/** Attempts to reparameterize a rational line by taking as "endpoints" (ie. the points
  * (u,v) = (1,0) and (0,1)) points on the planes x = 0, y = 0, z = 0, w = 0. 
  * In other words, one of the coordinates is 0 when u=0 and another is zero when
  * v=0. This assumes that the current endpoints of line_par have already been
  * optimized. Otherwise p0 and end1 (say) might represent the same point but with
  * different scales.
  * Returns the value of the new parameters corresponding to the first endpoint of
  * the old parameterization */
void qi_param_line_improved1            (mqi_vect_t __INIT rop, mqi_curve_param_t line_par);

/** Attempts to reparameterize a line c = c1+sqrt(xi). c2 such that 
  * one of the coordinates is 0 when u=0 and another is zero when v=0
  * Returns the parameters corresponding to the initial endpoints */
void qi_param_line_improved2            (mqi_vect_t __INIT rop, mqi_curve_param_t c1, mqi_curve_param_t c2,
					 mqi_coef_t __IN xi);

/** Attempts to reparameterize a line c = c1+sqrt(xi). c2 + sqrt(D). c3 + sqrt(D.xi). c4
  * such that one of the coordinates is 0 when u=0 and another is zero when v=0
  * (D = D1 + sqrt(xi). D2) */
void qi_param_line_improved3            (mqi_curve_param_t c1, mqi_curve_param_t c2,
					 mqi_curve_param_t c3, mqi_curve_param_t c4,
                                         mqi_coef_t __IN xi, mqi_coef_t __IN d1, mqi_coef_t __IN d2);

/*@}*/


/** @name Planes
 */

/*@{*/

/** Computes the parameterization of a double plane
  * Output is m1*[s*u t*u v] */
void qi_param_plane_double              (mqi_mat_t __IN q, mqi_mat_t __IN sing, mqi_mat_t m1);

/** Computes the parameterization of a pair of planes
  * Output is m1*[s*u t*u v] +/- sqrt(D)*m2*[s*u] if D is non zero
  * Output is m1*[s*u t*u v] +/- m2*[s*u] otherwise */
void qi_param_plane_pair                (mqi_mat_t __IN q, mqi_mat_t __IN sing, mqi_coef_t __INIT D,
					 mqi_mat_t m1, mqi_mat_t m2);

/*@}*/


/** @name Parametrization 2x2
 */
/*@{*/

/** Computes the parameterization of 2 x 2 matrix, used for lines and planes
  * Output is m1*[u] +/- sqrt(D)*m2*[u] if D is not a square
  * Output is m1*[u] +/- m2*[u] if D is a square
  * When b^2-ac is a square, D is set to 0 */
void qi_param_2x2                       (mqi_mat_t __IN q, mqi_coef_t __INIT D, mqi_mat_t m1, mqi_mat_t m2);

/*@}*/



/** @name Misc
 */
/*@{*/

/** Plugs a surface_param in the equation of a quadric */
void qi_param_plug_sp_in_quadric        (mqi_hhpoly_t __INIT rop, mqi_surface_param_t __IN s1, mqi_mat_t __IN q,
					 mqi_surface_param_t __IN s2);

/** Plugs a curve_param in the equation of a quadric */
void qi_param_plug_cp_in_quadric        (mqi_hpoly_t __INIT rop, mqi_curve_param_t __IN c1,
	       			 	 mqi_mat_t __IN q, mqi_curve_param_t __IN c2);

/*@}*/

#endif

