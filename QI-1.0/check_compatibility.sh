#!/bin/bash

echo -n "Checking compatibility with libmqi ..."

# GMP support is needed
mpz=$(mqi-config --cflags | grep "__USE_GMP")
if [ -z "$mpz" ] ; then	echo "libmqi has been compiled *without* GMP support, so I cannot procede."; exit 1; fi

# MPFR support is needed
mpfr=$(mqi-config --cflags | grep "__USE_MPFR")
if [ -z "$mpfr" ] ; then echo "libmqi has been compiled *without* MPFR support, so I cannot procede."; exit 1; fi

echo "OK"

#if [ ! -z $MPFR_PATH ]
#then
#	mpfr=$(nm $libmqi | grep "mpfr" | tail -1)
#	if [ -z "$mpfr" ]; then echo "libmqi.a has been compiled *without* MPFR support, so I cannot procede."; exit 1; fi
#else
#	mpfr=$(nm $libmqi | grep "mpfr" | tail -1)
#	if [ ! -z "$mpfr" ]; then echo "libmqi.a has been compiled *with* MPFR support, so I cannot procede."; exit 1; fi
#fi
#
#if [ ! -z $MPFI_PATH ]
#then
#	mpfi=$(nm $libmqi | grep "mpfi" | tail -1)
#	if [ -z "$mpfi" ]; then echo "libmqi.a has been compiled *without* MPFI support, so I cannot procede."; exit 1; fi
#else
#	mpfi=$(nm $libmqi | grep "mpfi" | tail -1)
#	if [ ! -z "$mpfi" ]; then echo "libmqi.a has been compiled *with* MPFI support, so I cannot procede."; exit 1; fi
#fi

