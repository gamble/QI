#include <qi/qi.h>

using namespace LiDIA;
using namespace QI;

/** A minimalistic program showing how a client program can use the
    qi library to calculate the intersection of 2 quadrics. */
int main (int argc, char **argv) {

  /** Expected intersection: */
  /* (85) (c) conic and double line :: (r) conic and db line ** degree 1 var **  */
  string q1 = string ( "x*z+x+y^2" );
  string q2 = string ( "x*z+y^2" );
  bigint_matrix m1;
  bigint_matrix m2;
  quad_inter<bigint> interquad;
  QIParser parser;
  QIOutputter outputter;
  QIWriter *writer;
  
  writer = new QIConsoleWriter ();
  /** *********************************** */
  /** You can try the HTML output as well */
  /** writer = new QIHTMLWriter ();       */
  /** *********************************** */
  
  
  parser.setQuadricDesc (q1);
  parser.parse ();
  m1 = parser.getMatricialDesc();
  
  parser.setQuadricDesc (q2);
  parser.parse ();
  m2 = parser.getMatricialDesc();
  
  /** No optimization */
  interquad = intersection(m1, m2, 0, cout);
  
  /** Deprecated way to display results */
/*  cout << "Calculated intersection: ";// << interquad;
  print_all_cc(interquad, m1, m2, cout);*/
  
  /** Transform kernel results into strings */
  outputter.output (interquad, m1, m2);
  
  /** HTML rendering */
  writer->setOutputInformation(outputter.getOutput());
  writer->write (); /** Produces the result to /dev/stdout by default */
  
  exit (0);
  
}
