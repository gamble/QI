#include "qi_elem.h"

/** Solves the equation q v = l dir in integers. q is the matrix of a cone. */
void qi_solve_proj		(mqi_vect_t __INIT v, mqi_mat_t __IN q, mqi_vect_t __IN dir, mqi_vect_t __IN sing) {

	mqi_mat_t	S;
	mqi_mat_t	St;
	mqi_mat_t	q3tmp, q3, qtmp, qtmp2, qtmp3, qtmp4;
	mqi_vect_t	dir3, dir3tmp;

	qi_mat_send_to_infinity (S, sing);

	mqi_mat_transformation (q3tmp, q, S, MQI_MAT_NOTRANSCO);
	mqi_mat_transpose_init (St, S);
	
	/* Need resize q3(3,3) */
	mqi_mat_resize(q3, q3tmp, 3, 3);

	mqi_mat_mul_init((mqi_mat_ptr)dir3tmp, St, (mqi_mat_ptr)dir);

	/* Need resize dir3(3) */
	mqi_vect_resize(dir3, dir3tmp, 3);
	qi_vect_optimize (dir3);

	mqi_mat_transco 	(qtmp, q3);
	mqi_mat_transpose_init  (qtmp2, qtmp); /* We want the transco matrix not the transposed */
	
	mqi_mat_mul_init (qtmp3, qtmp2, (mqi_mat_ptr)dir3);

	mqi_vect_resize((mqi_vect_ptr)qtmp4, (mqi_vect_ptr)qtmp3, 4);

	mqi_mat_mul_init ((mqi_mat_ptr)v, S, qtmp4);
	
	qi_vect_optimize (v);

	mqi_mat_list_clear (S, St, q3, q3tmp, dir3, qtmp, qtmp2, qtmp3, qtmp4, NULL);

}

