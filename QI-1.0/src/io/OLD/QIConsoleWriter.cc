#include "QIConsoleWriter.h"

#define SECTION_LINE    "=============================================="
#define SUBSECTION_LINE "----------------------------------------------------"


/** Example of section:

    ==============================================
                    INPUT QUADRICS               
    ==============================================

*/
string QIConsoleWriter::formatSection      (string sectionName){
  
  string output;
  short n_spaces = (string(SECTION_LINE).length() - sectionName.length()) / 2;

  output += "\n";
  output += SECTION_LINE;
  output += "\n";

  while (n_spaces -- > 0) output += " ";

  output += sectionName;
  output += "\n";

  output += SECTION_LINE;
  output += "\n";

  return output;
  
}

/** Example of subsection:
	----------------------------------------------------
                     Smooth quartic, branch 1
  	----------------------------------------------------
*/
string QIConsoleWriter::formatSubsection   (string subsectionName) {

  string output;
  short n_spaces = (string(SUBSECTION_LINE).length() - subsectionName.length()) / 2;

  output += "\n";
  output += SUBSECTION_LINE;
  output += "\n";

  while (n_spaces -- > 0) output += " ";

  output += subsectionName;
  output += "\n";

  output += SUBSECTION_LINE;
  output += "\n";

  return output;


}
