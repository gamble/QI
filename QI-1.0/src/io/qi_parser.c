#include "qi_parser.h"

/** Public parsing function.
 *  Input:  a character string containing the quadric description
 *  Output: an mqi_vect_t with mpz_t coefficients
 *
 *  The quadric description can be provided in two different formats:
 *
 *  1) Affine/Homogeneous format, like: "x^2+x*w-w^2"
 *  2) Vectorial format, like:          "[1 0 0 1 0 0 0 0 0 -1]"
 *
 */
void qi_parse (mqi_vect_ptr rop, const char *quadric_desc) {

	char * trimmed;
	
	/** We just need to discriminate each representation and
	    delegate the work to the appropriate parser. */
		
	/** First, remove blanks */
	trimmed = trim (quadric_desc, strlen(quadric_desc));

	/** Then, check whether we have an open brace or not */
	if ( trimmed[0] == '[' )
		_qi_parse_vector (rop, trimmed);
	else
		_qi_parse_equation (rop, trimmed);

}

