#include "qi_parser.h"

void _qi_parse_vector   (mqi_vect_ptr rop, const char *vectorial_desc) {

	char *token;
	unsigned short k;
	char *modifiable;

	mqi_vect_init_mpz_t (rop, 10);

	/** Need to have a writable copy of "vectorial_desc" */
	modifiable = strdup (vectorial_desc);

        token = strtok(modifiable, "[,]");
	k = 0;


        while ( token ) {
		mqi_coef_set_str (VECT(rop,k), token);
                token = strtok (NULL, "[,]");
		++k;
        }

	return;

}

