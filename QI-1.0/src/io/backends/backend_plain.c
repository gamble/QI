#include "qi_backend_t.h"

#ifdef LIBQI_COLOR_SHELL
#define FG_BOLD    "\033[1m"
#define FG_NORMAL  "\033[0m"
#define FG_FACE1   "\033[35;1m" // Currently set to magenta + bold
#define FG_FACE2   "\033[31;1m" // Currently set to red + bold
#define FG_FACE3   "\033[34;1m" // Currently set to blue + bold
#else
#define FG_BOLD    ""
#define FG_NORMAL  ""
#define FG_FACE1   ""
#define FG_FACE2   ""
#define FG_FACE3   ""
#endif

const char * const SECTION_LINE =
	"======================================================================";

const char * const SUBSECTION_LINE =
	"----------------------------------------------------------------------";


/* Special symbols bindings */
static const char * const backend_symbols[] =
 {
	"|R",			/* Example: "&real" in html, "\mathbb{R}" in latex */
	"|C",			/* Example: "\mathbb{C}" in latex */
	"U",			/* Example: "&cup" in html */
	"inf",			/* Example: "&infin" in html, "\infty" in latex */
 };

static void begin_document (void)
{
	/* Nothing to do */
}

static void end_document (void)
{
	/* Set the style back to normal */
	mqi_log_printf ("%s", FG_NORMAL);
}

static void style_default (void)
{
	mqi_log_printf ("%s", FG_NORMAL);
}

static void style_math (void)
{
	mqi_log_printf ("%s", FG_BOLD);
}

static void style_info (void)
{
	mqi_log_printf ("%s", FG_FACE3);
}

static void exponent (const char * const text)
{
	mqi_log_printf ("^%s", text);
}

static void symbol (qi_backend_sym sym)
{
	if ( sym > SYM_INFINITE )
	{
		fprintf (stderr,
		"%s error: invalid special symbol code (%d)\n",
		__func__, sym);
	}
	mqi_log_printf ("%s", backend_symbols[sym]);
}

static void newline (void)
{
	mqi_log_printf ("\n");
}

static void section_common (const char * const text,
			    const char * const section_string)
{
	size_t n_spaces;

	/* Center the text */
	n_spaces = 0.5 * ((double)strlen(section_string) - (double)strlen(text));

	mqi_log_printf (
	"\n"
	"%s"
	"\n", section_string);

	/* center */
	while (n_spaces-- > 0) mqi_log_printf (" ");
	
	mqi_log_printf (
	"%s\n"
	"%s"
	"\n\n", text, section_string);
}

static void section (const char * const text)
{
	section_common(text, SECTION_LINE);	
}
static void subsection (const char * const text)
{
	section_common(text, SUBSECTION_LINE);
}


/* Static initialization of the <name> backend
 * structure.
 */
DEF_BACKEND(plain);

