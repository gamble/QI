#include "qi_parser.h"

/** Parses the following grammar:
 *
 *  <>		 ::= <sign>? <expression>
 *  <expression> ::= <termlist> <sign> <expression> | <termlist> <eol>
 *  <termlist>	 ::= <term> '*' <termlist> | <term>
 *  <term>	 ::= <coef> '*' <monomial> | <coef> | <monomial>
 *  <monomial>	 ::= <var> '^' <exponent> | <var>
 *  <var>	 ::= 'x'|'y'|'z'|'w'|'X'|'Y'|'Z'|'W'
 *  <sign>	 ::= '+'|'-'
 *  <coef>	 ::= [0-9]+
 *  <exponent>	 ::= <coef>
 *  <eol>	 ::= '\0'
 *
 **/

/*char input[512];*/
char error[512];

signed char hashtable[17] = { -1, -1, 9, 8,
			       7, 6, 5, -1,
       		               4, 3, 2, -1,
			       1, -1, -1, -1, 0 };

char ** coefficients;

unsigned char powers[4] = { 0, 0, 0, 0};/** Powers of X,Y,Z,W 	  	*/
unsigned char checksum  = 0;	  	/** Sum of powers of X,Y,Z,W 	*/
signed char   sign 	= 0;	  	/** Current monomial sign	*/
char	      *coef     = (char *)0;	/** Current monomial coef	*/
unsigned char current_var = 0; 		/** Current variable {0,1,2,3}  */

void init_coefficients (void) {

	unsigned short k;

	coefficients = (char **)calloc( 10, sizeof(char *) );

	for ( k = 0; k < 10; k++ )
	{
		coefficients[k] = (char *)calloc( COEF_MAX_SIZE, sizeof(char) );
		coefficients[k][0] = '0';
		coefficients[k][1] = '\0';
	}
}

void init_monomial (void) {
	
	reset_powers;
	
	if ( coef )
	{
		free ( coef );
		coef = (char *)0;
	}
	
	coef = (char *)calloc( COEF_MAX_SIZE, sizeof(char) );
	coef[0] = '1';
	coef[1] = '\0';
	sign 	= 1;

}

void init_qi_parser (void)
{

	checksum = 0;
	sign	 = 0;
	coef	 = (char *)0;
	current_var = 0;
	coefficients = (char **)0;
	error[0] = '\0';
}

void exit_func (void) {

	printf ("\n");

}

void parser_parse (void) {

	/** Initializes our custom data */
	init_all;

	/** Optional first minus sign */
	TOKEN_NEW;
	PARSE(sign);
	ON_SUCCESS( TRIGGER(sign); );

	PARSE(expression);

	ON_ERROR (
		fprintf (stderr, "%s\n", error);
		return;
	)

	return;

}

PARSE_DECL(expression) {

	PARSE(termlist);
	ON_ERROR( return; );

	TOKEN_NEW;
	PARSE(sign);
	ON_SUCCESS( 
		TRIGGER(sign);
		PARSE(expression); 
		return;
	)
		
	/** A string finishes with a null character */
	PARSE_TERMINAL('\0');

}

PARSE_DECL(termlist) {

	PARSE(term);
	PARSE_TERMINAL('*');
	ON_SUCCESS ( PARSE(termlist); return; );
	IGNORE_ERROR ( next_coef; return; );

}

PARSE_DECL(term) {

	TOKEN_NEW;
	PARSE(coef);
	ON_SUCCESS (
		TRIGGER(coef);
		PARSE_TERMINAL('*');
		ON_SUCCESS(
			PARSE(monomial);
			return;
		);
		IGNORE_ERROR ( return; );
	);

	PARSE(monomial);	
	
}

PARSE_DECL(monomial) {

	TOKEN_NEW;
	PARSE(var);
	ON_ERROR( return; );
	
	TRIGGER(var);
	
	PARSE_TERMINAL('^');
	ON_SUCCESS(
		TOKEN_NEW;
		PARSE(exponent);
		ON_SUCCESS ( TRIGGER(exponent); );
		return;
	);
	IGNORE_ERROR ( return; );
	
}

PARSE_DECL(exponent) {
	PARSE(coef);
}

PARSE_DECL(var) {

	PARSE_TERMINAL('x');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('y');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('z');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('w');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('X');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('Y');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('Z');
	ON_SUCCESS ( return; );
	PARSE_TERMINAL('W');

}

PARSE_DECL(sign) {

	PARSE_TERMINAL('+');
	ON_SUCCESS( return; );
	PARSE_TERMINAL('-');

}

PARSE_DECL(coef) {

	char digit;
	char optional = 0;

	for ( ; ; ) {	
		for ( digit = '0'; digit <= '9' ; digit ++) {
			PARSE_TERMINAL(digit);
			ON_SUCCESS ( optional = 1; break; );		
		}
		if ( optional == 1 ) {
			IGNORE_ERROR ( return; );
		}
		else	return;
	}
	
		
}

/** ******** */
/** Triggers */
/** ******** */
TRIGGER_DECL(coef) {

	memcpy (coef, &token[0], strlen(token) * sizeof(char));	
	return;

}

TRIGGER_DECL(exponent) {

	short value;

	/*printf ("New exponent parsed: %s\n", token);*/

	/** Here, we raise the current variable */
	value = atoi(token);
	
	while ( --value > 0 ) raise(current_var);

	return;
	
}

TRIGGER_DECL(sign) {

	/*printf ("New sign parsed: %s\n", token); */

	if ( token[0] == '-' ) sign = -1;

	return;
	
}

TRIGGER_DECL(var) {

	/*printf ("New variable parsed %s\n", token);*/

	switch (token[0]) {

		case 'x':
		case 'X': raise_X; break;
		case 'y':
		case 'Y': raise_Y; break;
		case 'z':
		case 'Z': raise_Z; break;
		case 'w':
		case 'W': raise_W; break;
		default:
			  fprintf (stderr, "Valid variables are: x,y,z,w (upper case tolerate)\n");

	}

	return;

}



/** *********** */
/** FOR TESTING */
/** *********** */

/*
void print_result () {


	int k;

	const char monomes[10][4] = { "x^2", "xy", "xz", "xw", "y^2", "yz", "yw", "z^2", "zw", "w^2" };

	printf ("[");
	for ( k = 0; k < 10; k++ ) {
		if ( k ) printf (", ");
		printf ("%s: %s", monomes[k], coefficients[k]);
	}
	printf ("]\n");


}

int main (int argc, char **argv) {

	atexit (exit_func);

	if ( argc < 2 ) {
		printf ("Enter a valid input string (ex: \"2*x^5-4*x^4+x^3-8\")\n");
		return (EXIT_FAILURE);
	}

	errno = ENOMSG;
	parser_set_input_stream (trim(argv[1], strlen(argv[1])));
	parser_set_error_stream (&error[0]);
	parser_parse ();

	ON_ERROR (  return (EXIT_FAILURE);  )
	
	print_result ();

	return (EXIT_SUCCESS);
	
}
*/

