#include "qi_backends.h"

extern qi_backend_t backend_plain;
extern qi_backend_t backend_html;
extern qi_backend_t backend_latex;

qi_backend_t * qi_backends[] =
 {
	&backend_plain,
	&backend_html,
	&backend_latex
 };

