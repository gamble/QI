#include "qi_settings.h"

/** Declare a global structure to avoid passing it in argument.
 *  The kernel refers to this global variable, so don't use
 *  another one or it will be simply ignored.
 *  Changing a setting simply consists in modifying the following
 *  global variable directly, like:
 *
 *  qi_settings.optimize = 1;
 *
 *  Note that the default values may be overwritten
 *  by a client program.
 *  It is the case of the "intersector" program,
 *  provided with QI.
 *
 *  @see	qi_settings.h
 *  */

qi_settings_t qi_settings = {

	/** Kernel settings */

	/* Upper bound for the search of prime factors */
	10000,

	/* Simplify output
	 * (equivalent to "optlevel" in old versions) */
	0,		

	/* Compute cut parameters */
	1,

	/* Projective equations as parameters */
	0,

	/* Omit components which don't belong
	 * to the real affine space R^3 */
	1,

	/* Show the euclidean types of the input
	 * quadrics */
	1,

	/* Output the cut parameters. */
	1,

	/* Verbosity level */
	VERBOSITY_EXHAUSTIVE

};

