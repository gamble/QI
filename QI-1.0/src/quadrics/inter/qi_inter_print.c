#include "qi_inter.h"
#include <mqi/mqi_log.h>
#include "qi_backends.h"
#include "qi_settings.h"


/* Use the appropriate backend to printout the
 * contents of "inter"
 */
void qi_inter_print (qi_inter_t inter)
{
	/* Selected backend */
	qi_backend_t * bk = NULL;

	/* Iterate through each component */
	size_t k;

	/* Check whether there was at least
	 * one component in the real affine space */
	int have_real_component;
	int is_real_component; /* same for the current component */

/* Lighten the syntax */
#define _P	mqi_log_printf
#define _D	bk->style_default();
#define _M	bk->style_math();
#define _I	bk->style_info();
#define _S(x)	bk->symbol(x);
#define _e(x)	bk->exponent(x);
#define _N	bk->newline();
#define section bk->section
#define subsection bk->subsection

	/* Select the appropriate backend */
	bk = qi_backends[qi_settings.backend];

	/* Type of the intersection */
	if ( qi_settings.output_verbosity >= VERBOSITY_EXHAUSTIVE )
	{
		section("Type of the intersection");
	}
	
	/* In the real projective space */
	if ( qi_settings.output_verbosity >= VERBOSITY_LABELS )
	{
		_D _P("Type in the real projective space "); _M _P("P"); _e("3") _P("("); _S(SYM_REAL) _P("): ");
		
		if ( qi_settings.output_verbosity >= VERBOSITY_BRUTE )
		{
			_I _P("%s", inter->real_type); _N
		}
	}

	/* In the complex projective space */
	if ( qi_settings.output_verbosity >= VERBOSITY_LABELS )
	{
		_D _P("Type in the complex projective space "); _M _P("P"); _e("3") _P("("); _S(SYM_COMPLEX) _P("): ");
		
		if ( qi_settings.output_verbosity >= VERBOSITY_BRUTE )
		{
			_I _P("%s", inter->complex_type); _N
		}
	}

	/* Check whether there're real components */
	have_real_component = 0;
	for ( k = 0; k < inter->n_components; k++ )
	{
		have_real_component =
			qi_inter_component_is_in_real_affine_space(inter->components[k]);

		/* A real component has been found */
		if ( have_real_component ) break;
	}

	/* If we didn't find any real component and that we've choosen to
	 * omit imaginary components, then it's finished
	 */
	if ( !have_real_component && qi_settings.output_omit_imaginary_components )
	{
		if ( qi_settings.output_verbosity >= VERBOSITY_LABELS )
		{
			_N
			_M _P("** There's no component belonging to the real affine space. **");
			_N
			_N
		}
		/* Finished */
		return;
	}

	/* Explain the format of the output equations */
	if ( qi_settings.output_verbosity >= VERBOSITY_EXHAUSTIVE )
	{
		section ("Parametrization of the intersection");
		_D _P("Parametrization of each component of the intersection in ");
		_M _S(SYM_REAL) _e("3")
		_D _P(" in homogeneous coordinates ");

		if ( qi_settings.output_projective )
		{
			_M _P("[x(u,v); y(u,v); z(u,v); w(u,v)]");
			_D _P(", where (");
			_M _P("u,v");
			_D _P(") is the parameter in ");
			_M _P("real projective space ( "); _P("P"); _e("1") _P("("); _S(SYM_REAL); _P(")"); _D _P(" )");
			_N
				_D _P("The corresponding affine parametrization is: ");
			_M _P("[x(u,v)/w(u,v); y(u,v)/w(u,v); z(u,v)/w(u,v)]");
			_N
		}
		else
		{
			_M _P("[x(u); y(u); z(u); w(u)]");
			_D _P(", where (");
			_M _P("u");
			_D _P(") is the parameter in ");
			_M _P("the closure of "); _S(SYM_REAL) _P(" ( "); _S(SYM_REAL) _P(" "); _S(SYM_UNION) _P(" "); _S(SYM_INFINITE); _P(" )");
			_N
				_D _P("The corresponding affine parametrization is: ");
			_M _P("[x(u)/w(u); y(u)/w(u); z(u)/w(u)]");
			_N
		}
	}

	/* Print each component of the intersection */
	for ( k = 0; k < inter->n_components; k++ )
	{
		is_real_component =
			qi_inter_component_is_in_real_affine_space(inter->components[k]);

		/* If we omit imaginary components, skip the current one if it's not real */
		if ( qi_settings.output_omit_imaginary_components && !is_real_component )
			continue;
		
		qi_inter_component_print(inter->components[k]);
	}

	_N
	_N

#undef _P
#undef _D
#undef _M
#undef _I
#undef _S
#undef _e
#undef _N
#undef section
#undef subsection
}

