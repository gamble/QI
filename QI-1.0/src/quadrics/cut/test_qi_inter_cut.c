#include "qi_inter_t.h"
#include <limits.h>
#include <mqi/mqi_log.h>
#include <time.h>

int main (void)
{

	mqi_coef_t	u0, u1, u2, u3;
	mqi_coef_t	d0, d1;
	mqi_coef_t	v;
	qi_inter_cut_t	cut;

	srand(time(0));

	mqi_coef_list_init_int (u0, u1, u2, u3, d0, d1, v, NULL);

	mqi_coef_srandomize_int(u0, INT_MAX/2);
	mqi_coef_srandomize_int(u1, INT_MAX/2);
	mqi_coef_srandomize_int(u2, INT_MAX/2);
	mqi_coef_srandomize_int(u3, INT_MAX/2);
	mqi_coef_urandomize_int(d0, INT_MAX/2);
	mqi_coef_urandomize_int(d1, INT_MAX/2);
	mqi_coef_srandomize_int(v,  INT_MAX/2);


	mqi_log_printf ("Coefficients:\n");
	mqi_coef_print (u0);
	mqi_log_printf (", ");
	mqi_coef_print (u1);
	mqi_log_printf (", ");
	mqi_coef_print (u2);
	mqi_log_printf (", ");
	mqi_coef_print (u3);
	mqi_log_printf (", ");
	mqi_coef_print (d0);
	mqi_log_printf (", ");
	mqi_coef_print (d1);
	mqi_log_printf (", ");
	mqi_coef_print (v);
	mqi_log_printf ("\n");

	/* test of [u0, v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_0,
			   u0, v);

	mqi_log_printf ("\nCut parameter with form [u0, v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);
	
	
	/* test of [u0 + u1*sqrt(d0), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_1,
			   u0, u1, d0, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	/* test of [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_2,
			   u0, u1, d0, u2, d1, u3, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	mqi_log_printf ("\n");

	/* SPECIAL CASES WHERE SOME COEFFICIENTS ARE NULL OR ONE ...
	 */

	/* test of [u0, v] */

	mqi_log_printf ("--- u0 is null, u1 is one ---\n");
	mqi_coef_set_zero(u0);
	mqi_coef_set_int (u1, 1);

	qi_inter_cut_init (cut, QI_INTER_CUT_0,
			   u0, v);

	mqi_log_printf ("\nCut parameter with form [u0, v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);
	
	
	/* test of [u0 + u1*sqrt(d0), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_1,
			   u0, u1, d0, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	/* test of [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_2,
			   u0, u1, d0, u2, d1, u3, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	mqi_log_printf ("\n");

	mqi_log_printf ("--- u1 is one, d0 is one ---\n");
	mqi_coef_srandomize_int(u0, INT_MAX/2);
	mqi_coef_set_int (u1, 1);
	mqi_coef_set_int (d0, 1);

	qi_inter_cut_init (cut, QI_INTER_CUT_0,
			   u0, v);

	mqi_log_printf ("\nCut parameter with form [u0, v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);
	
	
	/* test of [u0 + u1*sqrt(d0), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_1,
			   u0, u1, d0, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	/* test of [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_2,
			   u0, u1, d0, u2, d1, u3, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	mqi_log_printf ("\n");

	mqi_log_printf ("--- u1 is one, d1 is zero ---\n");
	mqi_coef_urandomize_int(d0, INT_MAX/2);
	mqi_coef_set_int (u1, 1);
	mqi_coef_set_zero (d1);

	qi_inter_cut_init (cut, QI_INTER_CUT_0,
			   u0, v);

	mqi_log_printf ("\nCut parameter with form [u0, v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);
	
	
	/* test of [u0 + u1*sqrt(d0), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_1,
			   u0, u1, d0, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	/* test of [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v] */
	qi_inter_cut_init (cut, QI_INTER_CUT_2,
			   u0, u1, d0, u2, d1, u3, v);

	mqi_log_printf ("\nCut parameter with form [u0 + u1*sqrt(d0) + u2*sqrt(d1) + u3*sqrt(d0*d1), v]:\n");
	qi_inter_cut_print(cut);
	mqi_log_printf ("\nLaTeX:\n");
	qi_inter_cut_print_LaTeX(cut);
	qi_inter_cut_clear(cut);

	mqi_log_printf ("\n");


	/* Clean memory */
	mqi_coef_list_clear (u0, u1, u2, u3, d0, d1, v, NULL);

}

