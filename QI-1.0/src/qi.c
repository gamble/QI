#include "qi.h"

short QI_CPU_TIME_MS = 0;

/** *************************** */
/** Main intersection procedure */
/** *************************** */
void qi_intersect	(qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
                         int optimize)
{	
	clock_t start, end;
	mqi_poly_t det_p;
	ssize_t degree;

	start = clock();

#ifdef TRACEFUNC
	tracefunc("");
#endif

	/* Turn on or off the optimization flag */
	qi_settings.optimize = optimize;

	qi_pencil_det (det_p, q1, q2);


#ifdef DEBUG
	mqi_log_printf ("Det pencil: "); mqi_poly_print(det_p); mqi_log_printf ("\n");
#endif


	if ( mqi_poly_is_zero (det_p) )
	{
#ifdef DEBUG
		mqi_log_printf ("Determinant vanishes\n");
#endif
		qi_inter_vanish_det (rop, q1, q2, det_p);
	}
	else
	{
		mqi_poly_t det_p_orig;

		mqi_poly_init_cpy (det_p_orig, det_p);
		qi_hpoly_optimize (det_p);

#ifdef DEBUG
		mqi_log_printf ("Det pencil after optimization: "); mqi_poly_print_xw(det_p, 'l', 'm'); mqi_log_printf ("\n");
#endif

		mqi_poly_t derx, derw, gcd_p;
		mqi_hpoly_init_dynamic (derx, mqi_poly_typeof(det_p));
		mqi_hpoly_init_dynamic (derw, mqi_poly_typeof(det_p));
		mqi_hpoly_init_dynamic (gcd_p, mqi_poly_typeof(det_p));

		mqi_poly_derivate_xw (derw, det_p, 'w');
		mqi_poly_derivate_xw (derx, det_p, 'x');

#ifdef DEBUG
		mqi_log_printf ("Derivative %%x: "); mqi_poly_print_xw(derx, 'l', 'm'); mqi_log_printf ("\n");
		mqi_log_printf ("Derivative %%w: "); mqi_poly_print_xw(derw, 'l', 'm'); mqi_log_printf ("\n");
#endif

		mqi_poly_gcd (gcd_p, derx, derw);
		qi_hpoly_optimize (gcd_p);

#ifdef DEBUG
		mqi_log_printf ("GCD of derivatives of determinental equation: "); mqi_poly_print_xw(gcd_p, 'l', 'm'); mqi_log_printf ("\n");
#endif

		degree = mqi_poly_degree (gcd_p);

		if ( degree == 0 ) {

			/** **************** */
			/** No multiple root */
			/** **************** */
#ifdef DEBUG
			mqi_log_printf ("No multiple root\n");
#endif
			qi_inter_no_mult (rop, q1, q2, det_p, det_p_orig);
			mqi_poly_clear(det_p_orig);

		}
		else
		{
			mqi_coef_t pseudo_discriminant;

			mqi_coef_init_dynamic (pseudo_discriminant, mqi_poly_typeof(gcd_p));
			qi_hpoly_pseudo_discriminant (pseudo_discriminant, gcd_p);

			if ( (degree == 1) || (degree == 3) || ( (degree == 2) && (mqi_coef_sign(pseudo_discriminant) == 0) ) )
			{
				/** ***************** */
				/** One multiple root */
				/** ***************** */
#ifdef DEBUG
				mqi_log_printf ("One multiple root\n");
#endif
				qi_inter_one_mult (rop, q1, q2, det_p, det_p_orig, gcd_p);
			}
			else
			{
				/** **************** */
				/** Two double roots */
				/** **************** */
#ifdef DEBUG
				mqi_log_printf ("Two double roots\n");
#endif
				qi_inter_two_mult (rop, q1, q2, det_p, det_p_orig, gcd_p);
			}

			/** Free local memory */
			mqi_coef_clear(pseudo_discriminant);
		}
		
		/** Free local memory */
		mqi_poly_list_clear(det_p_orig, derx, derw, gcd_p, NULL);
	}

	/** ***************************** */
	/** Computes the calculation time */
	/** ***************************** */

	end = clock ();

	QI_CPU_TIME_MS = (short)( 1000.0 * (((double)(end - start))/CLOCKS_PER_SEC) );

	/** Free local memory */
	mqi_poly_clear(det_p);	

	return;
}

