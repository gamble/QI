#include "qi_param.h"

/** Outputs the parameterization of the quadric of inertia (2,2) of implicit
    equation "xy - zw = 0"  :  s = [u.t, v.s, u.s, v.t]  */
void qi_param_surface_22                (mqi_surface_param_t s) {

	mqi_hpoly_t		u_pol, v_pol;
	mqi_curve_param_t	cu, cv;
	mqi_surface_param_t	tmp;

	mqi_hpoly_init_dynamic (u_pol, mqi_surface_param_typeof(s));
	mqi_hpoly_init_dynamic (v_pol, mqi_surface_param_typeof(s));
	
	mqi_poly_set_at_int  (u_pol, 1, 1); /* x */
	mqi_poly_set_at_int  (v_pol, 0, 1); /* w */
	mqi_poly_set_homogeneous(u_pol, 1);
	mqi_poly_set_homogeneous(v_pol, 1);

	mqi_curve_param_init_dynamic (cu, 4, mqi_surface_param_typeof(s));
	mqi_curve_param_init_dynamic (cv, 4, mqi_surface_param_typeof(s));

	/* A curve param is made of "normal" polynomials.
	 * It is necessary to call the following setter to
	 * declare each polynomial as homogeneous with the
	 * specified degree.
	 */
/*	mqi_curve_param_set_homogeneous (cu, (ssize_t)-1);
	mqi_curve_param_set_homogeneous (cv, (ssize_t)-1);
*/	
	mqi_poly_set_at_int 	(CP(cu,0), 0, 1); /* w */
	mqi_poly_set_homogeneous(CP(cu,0), (ssize_t)1);
	

	mqi_poly_set_zero	(CP(cu,1));
	mqi_poly_set_homogeneous(CP(cu,1), (ssize_t)-1);

	mqi_poly_set_at_int	(CP(cu,2), 1, 1); /* x */
	mqi_poly_set_homogeneous(CP(cu,2), (ssize_t)1);
	
	mqi_poly_set_zero	(CP(cu,3));
	mqi_poly_set_homogeneous(CP(cu,3), (ssize_t)-1);

	mqi_poly_set_zero	(CP(cv,0));
	mqi_poly_set_homogeneous(CP(cv,0), (ssize_t)-1);

	mqi_poly_set_at_int	(CP(cv,1), 1, 1); /* x */
	mqi_poly_set_homogeneous(CP(cv,1), (ssize_t)1);

	mqi_poly_set_zero	(CP(cv,2));
	mqi_poly_set_homogeneous(CP(cv,2), (ssize_t)-1);

	mqi_poly_set_at_int	(CP(cv,3), 0, 1); /* w */
	mqi_poly_set_homogeneous(CP(cv,3), (ssize_t)1);

	mqi_surface_param_init_cpy	(tmp, s);

	mqi_surface_param_inject_poly_in_cp (s, cu, u_pol);
	mqi_surface_param_inject_poly_in_cp (tmp, cv, v_pol);

	mqi_log_printf ("s_1: "); mqi_surface_param_print_xwst(s, 'x', 'w', 's', 't'); mqi_log_printf("\n");
	mqi_log_printf ("s_2: "); mqi_surface_param_print_xwst(tmp, 'x', 'w', 's', 't'); mqi_log_printf("\n");

	int k;
	mqi_log_printf("Debug print:\n");
	for ( k = 0; k < 4; k++ )
	{
	 mqi_log_printf ("from s1->"); mqi_hhpoly_print_debug(SP(s,k));
	 mqi_log_printf ("from s2->"); mqi_hhpoly_print_debug(SP(tmp,k));
	}

	mqi_surface_param_add (s, s, tmp);

	mqi_poly_list_clear(u_pol, v_pol, NULL);
	mqi_curve_param_list_clear(cu, cv, NULL);
	mqi_surface_param_clear(tmp);

}

/** $$ EXTRA WORK ;) NOT USED BY THE QI KERNEL */
/*void qi_param_surface_22_1              (mqi_coef_ptr d, mqi_surface_param_ptr s1, mqi_surface_param_ptr s2) {

	mqi_hpoly_t		u_pol, v_pol;
	mqi_curve_param_t	cu, cv, cu1, cv1;
	mqi_surface_param_t	tmp;
	
	mqi_hpoly_init_dynamic (u_pol, QI_POLY_MAXDEG, mqi_coef_typeof(d));
	mqi_hpoly_init_dynamic (v_pol, QI_POLY_MAXDEG, mqi_coef_typeof(d));

	mqi_hpoly_set_x (u_pol);
	mqi_hpoly_set_w (v_pol);

	mqi_curve_param_init_dynamic (cu, 4, QI_POLY_MAXDEG, 0, mqi_coef_typeof(d));
	mqi_curve_param_init_dynamic (cv, 4, QI_POLY_MAXDEG, 0, mqi_coef_typeof(d));
	mqi_curve_param_init_dynamic (cu1, 4, QI_POLY_MAXDEG, 0, mqi_coef_typeof(d));
	mqi_curve_param_init_dynamic (cv1, 4, QI_POLY_MAXDEG, 0, mqi_coef_typeof(d));
	
	mqi_hpoly_set_w		(cu->equations[0]);
	mqi_hpoly_set_zero	(cu->equations[1]);
	mqi_hpoly_set_x		(cu->equations[2]);
	mqi_hpoly_set_zero	(cv->equations[0]);
	mqi_hpoly_set_x		(cv->equations[1]);
	mqi_hpoly_set_w		(cv->equations[2]);

	mqi_hpoly_neg		(cv->equations[2], cv->equations[2]);

	mqi_surface_param_init_cpy (tmp, s1);

	if ( mqi_coef_cmp_schar(d, 1) == 0 ) {

		mqi_hpoly_set_x (cu->equations[3]);
		mqi_hpoly_set_w (cv->equations[3]);

		mqi_surface_param_inject_poly_in_cp (s1, cu, u_pol);
		mqi_surface_param_inject_poly_in_cp (tmp, cv, v_pol);
		mqi_surface_param_add (s1, s1, tmp);

	}else {
		
		mqi_hpoly_set_x (cu1->equations[3]);
		mqi_hpoly_set_w (cv1->equations[3]);

		mqi_surface_param_inject_poly_in_cp (s1, cu1, u_pol);
		mqi_surface_param_inject_poly_in_cp (tmp, cv1, v_pol);
		mqi_surface_param_add (s1, s1, tmp);

		mqi_surface_param_inject_poly_in_cp (s2, cu, u_pol);
		mqi_surface_param_inject_poly_in_cp (tmp, cv, v_pol);
		mqi_surface_param_add (s2, s2, tmp);
		
	}

	mqi_hpoly_clear (u_pol);
	mqi_hpoly_clear (v_pol);
	mqi_curve_param_clear (cu);
	mqi_curve_param_clear (cv);
	mqi_curve_param_clear (cu1);
	mqi_curve_param_clear (cv1);
	mqi_surface_param_clear (tmp);
	
}
*/

