#include "qi_number.h"

/** ************************* **/
/** Curve params optimization **/
/** ************************* **/

/** Optimize a curve_param */
void qi_curve_param_optimize 		(mqi_curve_param_t curve_param)
{

	mqi_coef_t	content;

	mqi_coef_init_dynamic (content, mqi_curve_param_typeof(curve_param));
	mqi_curve_param_content (content, curve_param);

	mqi_curve_param_divexact_coef (curve_param, curve_param, content);

	mqi_coef_clear(content);

}

/** Optimize a pair of curve_params by taking the gcd of their content */
void qi_curve_param_optimize_gcd 	(mqi_curve_param_t curve_param1, mqi_curve_param_t curve_param2)
{

	mqi_coef_t	content1;
	mqi_coef_t	content2;

	mqi_coef_list_init_dynamic (mqi_curve_param_typeof(curve_param1), content1, content2, NULL);
	
	mqi_curve_param_content (content1, curve_param1);
	mqi_curve_param_content (content2, curve_param2);

	mqi_coef_gcd (content1, content1, content2);

	mqi_curve_param_divexact_coef (curve_param1, curve_param1, content1);
	mqi_curve_param_divexact_coef (curve_param2, curve_param2, content1);

	mqi_coef_list_clear(content1, content2, NULL);

}

/** Optimize the parameterization of a conic 
 *  c1 + sqrt(xi). c2 + eps. sqrt(D). (c3 + sqrt(xi). c4)
 *  where D and xi are already optimized. 
 *  Simplification by the common factor of the parameterization
 */
void qi_curve_param_optimize_conic (mqi_curve_param_t curve_param1, mqi_curve_param_t curve_param2,
				    mqi_curve_param_t curve_param3, mqi_curve_param_t curve_param4) {


	mqi_coef_t	content1;
	mqi_coef_t	content2;
	mqi_coef_t	content3;
	mqi_coef_t	content4;

	mqi_coef_list_init_dynamic(mqi_curve_param_typeof(curve_param1),
			content1, content2, content3, content4, NULL);

	mqi_curve_param_content (content1, curve_param1);
	mqi_curve_param_content (content2, curve_param2);
	mqi_curve_param_content (content3, curve_param3);
	mqi_curve_param_content (content4, curve_param4);

	mqi_coef_gcd (content1, content1, content2);
	mqi_coef_gcd (content1, content1, content3);
	mqi_coef_gcd (content1, content1, content4);

	
	mqi_curve_param_divexact_coef (curve_param1, curve_param1, content1);
	mqi_curve_param_divexact_coef (curve_param2, curve_param2, content1);
	mqi_curve_param_divexact_coef (curve_param3, curve_param3, content1);
	mqi_curve_param_divexact_coef (curve_param4, curve_param4, content1);

/*  if (factor != 1)
 *
    {
     #ifdef DEBUG
      s << ">> simplification factor of param: " << factor << endl;
      #endif*/

  /* } */ 

	mqi_coef_list_clear(content1, content2, content3, content4, NULL);

}

/** Optimize the parameterization of a smooth quartic 
 *  c1 + sqrt(xi). c2 + eps. sqrt(Delta). (c3 + sqrt(xi). c4)
 *  Delta = Delta1 + sqrt(xi). Delta2
 *  If xi = 1 then c2, c4 and Delta2 are 0
 *  Simplification by the common factor of the parameterization
 * */
void qi_curve_param_optimize_smooth_quartic (mqi_curve_param_t curve_param1, mqi_curve_param_t curve_param2,
					     mqi_curve_param_t curve_param3, mqi_curve_param_t curve_param4,
					     mqi_hpoly_t delta1, mqi_hpoly_t delta2)
{

	mqi_coef_t factor_delta;
	mqi_coef_t factor_delta_squared;
	mqi_coef_t factor_c12, factor_c34;
	mqi_coef_t cofactor;
	mqi_coef_t content_gcd;
	mqi_coef_t tmp;

	mqi_coef_list_init_dynamic(mqi_poly_typeof(delta1),
			content_gcd, tmp, factor_c12, factor_c34, NULL);

	mqi_poly_content		(tmp, delta1);
	mqi_poly_content		(content_gcd, delta2);

	mqi_coef_gcd			(content_gcd, content_gcd, tmp);
	
	qi_coef_extract_square_factors 	(factor_delta, cofactor, content_gcd);
	
	mqi_coef_init_cpy		(factor_delta_squared, factor_delta);
	mqi_coef_mul			(factor_delta_squared, factor_delta_squared, factor_delta_squared);

	mqi_poly_divexact_coef		(delta1, delta1, factor_delta_squared);
	mqi_poly_divexact_coef		(delta2, delta2, factor_delta_squared);

	mqi_curve_param_content		(tmp, curve_param1);
	mqi_curve_param_content		(content_gcd, curve_param2);
		
	mqi_coef_gcd			(factor_c12, tmp, content_gcd);
	
	mqi_curve_param_mul_coef (curve_param3, curve_param3, factor_delta);
	mqi_curve_param_mul_coef (curve_param4, curve_param4, factor_delta);

	if ( mqi_coef_cmp_schar (factor_c12, 1) != 0 )
	{		
		mqi_curve_param_content		(tmp, curve_param3);
		mqi_curve_param_content		(content_gcd, curve_param4);

		mqi_coef_gcd			(factor_c34, tmp, content_gcd);
		mqi_coef_mul			(factor_delta, factor_c34, factor_delta);
		mqi_coef_gcd			(factor_delta, factor_c12, factor_delta);	

		if ( mqi_coef_cmp_schar (factor_delta, 1) != 0 )
		{
			mqi_curve_param_divexact_coef	(curve_param1, curve_param1, factor_delta);
			mqi_curve_param_divexact_coef	(curve_param2, curve_param2, factor_delta);
			mqi_curve_param_divexact_coef	(curve_param3, curve_param3, factor_delta);
			mqi_curve_param_divexact_coef	(curve_param4, curve_param4, factor_delta);
		}
	}

	/** Free local memory */
	mqi_coef_list_clear(factor_delta, factor_delta_squared, factor_c12, factor_c34, cofactor,
				content_gcd, tmp, NULL);
}

