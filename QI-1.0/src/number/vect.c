#include "qi_number.h"

/** Optimize a vector by dividing by its content */
void qi_vect_optimize (mqi_vect_t vector) {

	mqi_coef_t content;

	mqi_coef_init_dynamic 	(content, mqi_vect_typeof(vector)); 
	mqi_vect_content      	(content, vector);
	mqi_vect_divexact_coef	(vector, vector, content);

	mqi_coef_clear(content);

}

/** Optimize a pair of vectors by dividing by the gcd of the contents */
void qi_vect_optimize_gcd (mqi_vect_t vector1, mqi_vect_t vector2) {

	mqi_coef_t content1;
	mqi_coef_t content2;

	mqi_coef_list_init_dynamic 	(mqi_vect_typeof(vector1), content1, content2, NULL);

	mqi_vect_content      	(content1, vector1);
	mqi_vect_content      	(content2, vector2);

	mqi_coef_gcd (content1, content1, content2);
	
	mqi_vect_divexact_coef	(vector1, vector1, content1);
	mqi_vect_divexact_coef	(vector2, vector2, content1);
	
	mqi_coef_list_clear(content1, content2, NULL);

}

/** Optimize a triplet of vectors by dividing by the gcd of the contents */
void qi_vect_optimize_gcd_triplet (mqi_vect_t vector1, mqi_vect_t vector2, mqi_vect_t vector3) {

	mqi_coef_t content1;
	mqi_coef_t content2;
	mqi_coef_t content3;

	mqi_coef_list_init_dynamic (mqi_vect_typeof(vector1), content1, content2, content3, NULL);
	
	mqi_vect_content      	(content1, vector1);
	mqi_vect_content      	(content2, vector2);
	mqi_vect_content      	(content3, vector3);

	mqi_coef_gcd (content1, content1, content2);
	mqi_coef_gcd (content1, content1, content3);
	
	mqi_vect_divexact_coef	(vector1, vector1, content1);
	mqi_vect_divexact_coef	(vector2, vector2, content1);
	mqi_vect_divexact_coef	(vector3, vector3, content1);

	mqi_coef_list_clear(content1, content2, content3, NULL);

}

/** Optimize a quadruplet of vectors by dividing by the gcd of the contents */
void qi_vect_optimize_gcd_quadruplet (mqi_vect_t vector1, mqi_vect_t vector2, 
				      mqi_vect_t vector3, mqi_vect_t vector4) {

	mqi_coef_t content1;
	mqi_coef_t content2;
	mqi_coef_t content3;
	mqi_coef_t content4;

	mqi_coef_list_init_dynamic(mqi_vect_typeof(vector1),
			content1, content2, content3, content4, NULL);

	mqi_vect_content      	(content1, vector1);
	mqi_vect_content      	(content2, vector2);
	mqi_vect_content      	(content3, vector3);
	mqi_vect_content      	(content4, vector4);
	
	mqi_coef_gcd (content1, content1, content2);
	mqi_coef_gcd (content1, content1, content3);
	mqi_coef_gcd (content1, content1, content4);
	
	mqi_vect_divexact_coef	(vector1, vector1, content1);
	mqi_vect_divexact_coef	(vector2, vector2, content1);
	mqi_vect_divexact_coef	(vector3, vector3, content1);
	mqi_vect_divexact_coef  (vector4, vector4, content1);
	
	mqi_coef_list_clear(content1, content2, content3, content4, NULL);

}

/** Optimize first half, then last half */
void qi_vect_optimize_by_half (mqi_vect_t vector) {

	mqi_vect_t bottom, up;
	size_t	   half_size;

	half_size = mqi_vect_size(vector) / 2;

	mqi_vect_list_init_dynamic (mqi_vect_size(vector), mqi_vect_typeof(vector),
					bottom, up, NULL);

	mqi_vect_get_subvect (bottom,	vector, 0, 		half_size);
	mqi_vect_get_subvect (up,	vector, half_size, 	half_size);

	qi_vect_optimize (bottom);
	qi_vect_optimize (up);

	mqi_vect_clear  (vector);
	mqi_vect_concat (__INIT vector, bottom, up);
	
	mqi_vect_list_clear(bottom, up, NULL);

}


/** Load balancing of two vectors */
void qi_vect_load_balancing (mqi_vect_t op1, mqi_vect_t op2) {


	mqi_coef_t content1;
	mqi_coef_t content2;
	mqi_coef_t max1;
	mqi_coef_t max2;
	mqi_coef_t coef, coef2;
	mqi_coef_t tmp_exponent, tmp_base;
	mqi_coef_t factor;
	mqi_prime_factorization_t  pf;

	unsigned short * factors;
	unsigned char  * exponents;
	int finished = 0;	
	size_t k, n_factors;
	int configuration;

	mqi_coef_list_init_dynamic (mqi_vect_typeof(op1), content1, content2, NULL);
	
	mqi_vect_content (content1, op1);
	mqi_vect_content (content2, op2);

	mqi_coef_init_cpy (max1, VECT(op1,0));
	mqi_coef_init_cpy (max2, VECT(op2,0));

	mqi_coef_abs (max1, max1);
	mqi_coef_abs (max2, max2);

	mqi_coef_init_cpy	(coef, max1);
	
	/** Maximum search */
	for ( k = 1; k < mqi_vect_size(op1); k++ ) { 
	
		mqi_vect_get_at (coef, op1, k);
		mqi_coef_abs (coef, coef);
		if ( mqi_coef_cmp (coef, max1) > 0 )
			mqi_coef_cpy (max1, coef);

		mqi_vect_get_at (coef, op2, k);
		mqi_coef_abs (coef, coef);
		if ( mqi_coef_cmp (coef, max2) > 0 )
			mqi_coef_cpy (max2, coef);
	
	}

	configuration = 0;
	
	if ( mqi_coef_cmp (max2, max1) > 0 )
		mqi_coef_cpy (coef, content2);
	else {
		mqi_coef_cpy (coef, max1);
		mqi_coef_cpy (max1, max2);
		mqi_coef_cpy (max2, coef);
		mqi_coef_cpy (coef, content1);
		configuration = 1;
	}
		
	mqi_prime_factorization_init (pf);
	mqi_primefactor (pf, coef, qi_settings.maxfactor);
	
	factors 	= mqi_prime_factorization_get_factors(pf);
	exponents	= mqi_prime_factorization_get_exponents(pf);
	n_factors	= mqi_prime_factorization_size(pf);

	mqi_coef_list_init_slong(coef, coef2, tmp_exponent, tmp_base, factor, NULL);
	mqi_coef_set_schar  (factor, 1);
	
	/** Calculate the balancing factor */	
	for ( k = 0; k < n_factors; k++) { 
	
		mqi_coef_set_slong (tmp_exponent, (slong)exponents[k]);
		mqi_coef_set_slong (tmp_base, (slong)factors[k]);
	
		while ( mqi_coef_cmp_schar (tmp_exponent, 0) > 0 ) {

			/* tmp_base^2 - max2 */
			mqi_coef_linexpr3 (coef,1,	1,tmp_base,tmp_base,max1,	-1,NULL,NULL,max2);
			
			/* tmp_base * | max1 - max2 | */
			mqi_coef_cpy (coef2, max1);
			mqi_coef_sub (coef2, coef2, max2);
			mqi_coef_abs (coef2, coef2);
			mqi_coef_mul (coef2, coef2, tmp_base);

			if ( mqi_coef_cmp (coef, coef2) < 0 ) {
				mqi_coef_mul 		(factor, factor, tmp_base);
				mqi_coef_mul 		(max1, max1, tmp_base);
				mqi_coef_divexact 	(max2, max2, tmp_base);
				mqi_coef_sub_schar (tmp_exponent, tmp_exponent, 1);
				
			}else {
				finished = 1;
				break;
			}
			
		}

		if ( finished ) break;
		
	}
	
	/** Apply the balancing factor "factor" to each vector */
	switch ( configuration )
	{

		case 0:
			mqi_vect_mul_int 	(op1, op1, mqi_coef_get_int(factor));
			if ( mqi_coef_sign(factor) )
				mqi_vect_divexact_int	(op2, op2, mqi_coef_get_int(factor));
			break;

		case 1:
			mqi_vect_mul_int	(op2, op2, mqi_coef_get_int(factor));
			if ( mqi_coef_sign(factor) )
				mqi_vect_divexact_int	(op1, op1, mqi_coef_get_int(factor));
			break;
			
		default:
			/** Cannot happen */
			break;
	}

	mqi_coef_list_clear(content1, content2, max1, max2, coef, coef2, tmp_exponent, tmp_base, factor, NULL);
	mqi_prime_factorization_clear(pf);

}

