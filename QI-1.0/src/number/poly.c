#include "qi_number.h"

/** ************************************* */
/** Homogeneous polynomials optimizations */
/** ************************************* */

/** Optimize coefficients of polynomial using the pp function */
void qi_hpoly_optimize 			(mqi_hpoly_t op) {

	mqi_coef_t coef_gcd;

	mqi_coef_init_dynamic	(coef_gcd, mqi_poly_typeof(op));

	mqi_poly_content 	(coef_gcd, op);
	mqi_poly_divexact_coef (op, op, coef_gcd);

	mqi_coef_clear(coef_gcd);

}

/** Optimize coefficients of a pair of hom_polynomials by taking the gcd of their coeff */
void qi_hpoly_optimize_gcd		(mqi_hpoly_t op1, mqi_hpoly_t op2) {

	mqi_coef_t content, tmp;

	mqi_coef_list_init_dynamic (mqi_poly_typeof(op1), content, tmp, NULL);
	
	mqi_poly_content (content, op1);
	mqi_poly_content (tmp, op2);
	mqi_coef_gcd	  (content, tmp, content);

	mqi_poly_divexact_coef (op1, op1, content);
	mqi_poly_divexact_coef (op2, op2, content);

	mqi_coef_list_clear(content, tmp, NULL);

}

/** Computes the pseudo-discriminant of a quadratic homogeneous polynomial, wich is:
 *
 * 	p[1]^2 - 4*p[0]*p[2]
 *
 */
void qi_hpoly_pseudo_discriminant 	(mqi_coef_t rop, mqi_hpoly_t poly) {

	mqi_coef_linexpr2(rop, 2,  1,POLY(poly,1),POLY(poly,1),  -4,POLY(poly,0),POLY(poly,2));

}

void qi_hhpoly_optimize_gcd		(mqi_hhpoly_t op1, mqi_hhpoly_t op2) {

	mqi_coef_t content, tmp;

	mqi_coef_init_dynamic (content, mqi_hhpoly_typeof(op1));
	mqi_coef_init_dynamic (tmp,	mqi_hhpoly_typeof(op2));
	
	mqi_hhpoly_content (content, op1);
	mqi_hhpoly_content (tmp, op2);
	mqi_coef_gcd	   (content, tmp, content);

	mqi_hhpoly_divexact_coef (op1, op1, content);
	mqi_hhpoly_divexact_coef (op2, op2, content);

	mqi_coef_clear(content);
	mqi_coef_clear(tmp);

}

/** Compute the primitive part of a hhpoly "op", i.e. "op" divided by its content */
void qi_hhpoly_pp 			(mqi_hhpoly_t __INIT rop, mqi_hhpoly_t op, mqi_coef_t content)
{

	mqi_coef_t	coef;

	if ( content == NULL ) {
		mqi_coef_init_dynamic (coef, mqi_hhpoly_typeof(op));
		mqi_hhpoly_content (coef, op);
	}else
		mqi_coef_init_cpy (coef, content);

	mqi_hhpoly_init_cpy		(rop, op);	
	mqi_hhpoly_divexact_coef	(rop, rop, coef);

	mqi_coef_clear(coef);

}

/** Compute the primitive part of a hom_hom_polynomial, but using the polynomial content */
void qi_hhpoly_pp_hpoly			(mqi_hhpoly_t __INIT rop, mqi_hhpoly_t op, mqi_hpoly_t content) {

	mqi_hpoly_t	coef;
	mqi_hpoly_t	Q, R;
	mqi_coef_t	alpha;
	size_t k;

	if ( content == NULL ) {
		mqi_poly_init_dynamic (coef, mqi_hhpoly_typeof(op));
		mqi_hhpoly_content_poly (coef, op);
	}else
		mqi_poly_init_cpy (coef, content);
		
	mqi_hhpoly_init_cpy (rop, op);

	for ( k = 0; k <= mqi_hhpoly_highest_x_degree(rop); k++ ) {
		mqi_poly_div_qr (__INIT  Q, __INIT R, __INIT  alpha, HHPOLY(op,k), content);
		mqi_poly_cpy    (HHPOLY(rop,k), Q);
	}

	mqi_coef_clear(alpha);
	mqi_poly_list_clear(Q, R, coef, NULL);

}

