#include "qi_inter.h"

void qi_inter_no_mult (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
		       mqi_hpoly_t __IN det_p, mqi_hpoly_t __IN det_p_orig)
{

	unsigned long	deg;
	interval	*roots;
	unsigned int	nbroot, nbroot_inf, case_flag = 0;
	mqi_mat_t	q;
	int		infinity_flag;
	int		offset;
	size_t		k;

	
	/* mqi_poly_degree returns an ssize_t.
	 * Here, we're sure that det_p's degree is strictly more
	 * than zero otherwise this function wouldn't have been
	 * triggered from the QI's main routine (see vanish det).
	 * The reason for have a signed data type for the degree
	 * is to treat the special case of the zero polynomial,
	 * which has an homogeneous degree of -1 by default.
	 * Here, it cannot happen so the cast is valid.
	 */
	deg = (unsigned long)mqi_poly_degree(det_p);

	/** If infinity is a solution in lambda */
	infinity_flag = 0;
	if ( mqi_coef_sign(POLY(det_p,4)) == 0 ) {
		infinity_flag = 1;
		deg = 3;
	}

	qi_uspensky (&roots, det_p, deg, &nbroot);

	nbroot_inf = nbroot;

	/** Number of roots plus infinity if applicable */
	if ( infinity_flag )
		nbroot_inf ++;

#ifdef DEBUG
	mqi_log_printf ("Number of real roots: %u\n", nbroot_inf);

	if ( nbroot_inf != 0 )
	{
		qi_uspensky_print_roots (roots, nbroot_inf);
		if ( infinity_flag )
			mqi_log_printf (", infinity");

		mqi_log_printf("\n");
	}
#endif

	if ( nbroot_inf == 0 ) {
#ifdef TRACEFUNC
	tracefunc("nbroot_inf = 0");
#endif
		case_flag = 2;
		mqi_mat_init_cpy (q, q1);
	}
	else {

		/** Two or four real roots */
		if ( nbroot_inf == 2 ) case_flag = 1;

#ifdef TRACEFUNC
	tracefunc("nbroot_inf > 0");
#endif

		/** Compute a quadric of positive determinant associated to a value either to
		 * the left of the first root of the determinential equation or in between
		 * the two first roots */

		mqi_hpoly_t deriv;
		mqi_hpoly_init_dynamic (deriv, mqi_poly_typeof(det_p));
		mqi_poly_derivate_xw   (deriv, det_p, 'x');

		offset = 0;

		/** Pick first point to left of first root */
		mqi_vect_t p;
		qi_uspensky_pick_point_outside_roots (p, roots, &infinity_flag, det_p, deriv, nbroot_inf, 0);
#ifdef TRACEFUNC
	tracefunc("pick point outside roots passed");
#endif
		qi_vect_optimize (p);
		
#ifdef DEBUG
		mqi_log_printf ("p: "); mqi_vect_print(p); mqi_log_printf("\n");
		mqi_log_printf ("det_p: "); mqi_poly_print(det_p); mqi_log_printf("\n");
#endif

		/** The determinental equation is negative at that point: pick the next one */
		mqi_coef_t tmp;
		mqi_coef_init_dynamic (tmp, mqi_poly_typeof(det_p));

		mqi_poly_eval_xw (tmp, det_p, VECT(p,0), VECT(p,1));

		if ( mqi_coef_sign(tmp) == -1 ) {

			offset = 1;
			qi_uspensky_pick_point_outside_roots (p, roots, &infinity_flag, det_p, deriv, nbroot_inf, 1);
			qi_vect_optimize(p);
#ifdef DEBUG
			mqi_log_printf ("p (retry): "); mqi_vect_print(p); mqi_log_printf("\n");
#endif
		}

#ifdef DEBUG
		mqi_log_printf ("Picked test lambda 1 at "); mqi_vect_print(p); mqi_log_printf("\n");
		if ( mqi_coef_sign(tmp) > 0 )
			mqi_log_printf ("> 0");
		else
			mqi_log_printf ("< 0");

		if ( nbroot_inf == 4 )
			mqi_log_printf (" -- ");
		else
			mqi_log_printf (" -- inertia (2,2) found");

		mqi_log_printf ("\n");
#endif

		/** The matrix associated to p */
		mqi_mat_t q1_tmp, q2_tmp;

		mqi_mat_initpool_cpy (q1_tmp, q1);
		mqi_mat_initpool_cpy (q2_tmp, q2);

		mqi_mat_mul_coef (q1_tmp, q1_tmp, VECT(p,0));
		mqi_mat_mul_coef (q2_tmp, q2_tmp, VECT(p,1));

		mqi_mat_init_dynamic(q, mqi_mat_nrows(q1_tmp), mqi_mat_ncols(q1_tmp),
					mqi_mat_typeof(q1_tmp));
		mqi_mat_add (q, q1_tmp, q2_tmp);

		/** 4 real roots */
		if ( nbroot_inf == 4 )
		{

#ifdef TRACEFUNC
	tracefunc("nbroot_inf = 4");
#endif
			/** Compute the inertia of q */
			mqi_vect_t in_q;
			qi_mat_inertia (in_q, q);

			/**  [4 0] found: empty */
			if ( mqi_coef_cmp_schar (VECT(in_q,0), 4) == 0 ) {

				qi_inter_init(rop);
				qi_inter_set_type (rop, 1,1);
				
				/** Clear roots  */
				for ( k = 0; k < deg; k++ ) {
					mpz_clear ( roots[k].c );
				}
				free ( roots );
				roots = NULL;

			}
			else
			{

				/** Not enough information to decide: pick a second point */
				/** Second test lambda */
				mqi_vect_t p2;
				qi_uspensky_pick_point_outside_roots (p2, roots, &infinity_flag, det_p, deriv, nbroot_inf, 2 + offset);
				qi_vect_optimize (p2);


				/** The matrix associated to p2 */
				/* discard old tmp matrices */
				mqi_mat_list_clear(q1_tmp, q2_tmp, NULL);
				mqi_mat_init_cpy (q1_tmp, q1);
				mqi_mat_init_cpy (q2_tmp, q2);

				mqi_mat_mul_coef (q1_tmp, q1, VECT(p2,0));
				mqi_mat_mul_coef (q2_tmp, q2, VECT(p2,1));

				mqi_mat_t qp2;
				mqi_mat_init_cpy (qp2, q1_tmp);
				mqi_mat_add	 (qp2, qp2, q2_tmp);

				/** Inertia */
				/* discard old in_q */
				mqi_vect_clear(in_q);
				qi_mat_inertia 	      (in_q, qp2);

				/** [4 0] found: empty */
				if ( mqi_coef_cmp_schar (VECT(in_q,0), 4) == 0 ) {

					qi_inter_init(rop);
					qi_inter_set_type (rop, 1,1);
				}
				else
				{

					/** Not empty */
					case_flag = 0;

					/** Choose the test point (and associated quadric) of smallest height */
					mqi_coef_t tmp, tmp2;
					mqi_coef_list_init_dynamic (mqi_vect_typeof(p), tmp, tmp2, NULL);

					mqi_vect_sum_of_squares (tmp, p);
					mqi_vect_sum_of_squares (tmp2, p2);

					if ( mqi_coef_cmp (tmp, tmp2) > 0 )
					{
						mqi_mat_clear(q);
						mqi_mat_init_cpy(q, qp2);
					}

					/** Free local memory */
					mqi_coef_list_clear(tmp, tmp2, NULL);

				}
			
				/** Free local memory */
				mqi_vect_clear(p2);
				mqi_mat_clear(qp2);

			}

			/** Free local memory */
			mqi_vect_clear(in_q);

		}

		/** Free local memory */
		mqi_poly_clear(deriv);
		mqi_vect_clear(p);
		mqi_mat_list_clear(q1_tmp, q2_tmp, NULL);
		mqi_coef_clear(tmp);

	}	

	/** Clear roots  */
	for ( k = 0; k < deg; k++ ) {
		mpz_clear ( roots[k].c );
	}
	free ( roots );
	roots = NULL;

	/** *************************************************************************** */
	/** The determinential equation passed is the one with unoptimized coefficients */
	/** *************************************************************************** */
#ifdef  TRACEFUNC
	tracefunc("before calling inter_with_22");
#endif

#ifdef DEBUG
	mqi_log_printf ("q:\n");
	mqi_mat_print(q);
	mqi_log_printf ("\n");
	mqi_log_printf ("det_p_orig:\n");
	mqi_poly_print (det_p_orig);
	mqi_log_printf ("\n");
	mqi_log_printf ("case_flag: %d\n", case_flag);
#endif
	qi_inter_with_22 (rop, q1, q2, det_p_orig, q, case_flag);

#ifdef TRACEFUNC
	tracefunc("inter with 22 passed");
#endif

	/** Free local memory */
	mqi_mat_clear(q);

}

