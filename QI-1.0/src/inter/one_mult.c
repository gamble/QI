#include "qi_inter.h"

void qi_inter_one_mult (qi_inter_t __INIT rop,
			mqi_mat_t __IN q1, mqi_mat_t __IN q2,
			mqi_hpoly_t __IN det_p, mqi_hpoly_t __IN det_p_orig, mqi_hpoly_t __IN gcd_p)
{

	qi_inter_component_t	comp;

	/** ******************************************************************************
	 *  Preliminaries: compute the multiple root, its multiplicity, the inertia of
	 *  the associated matrix and a few additional information
	 * ******************************************************************************/

	/* The degree of the gcd
	 * d = 1: double, d = 2: triple, d = 3: quadruple */
	ssize_t d;
	d = mqi_poly_degree(gcd_p);

	/** The (lambda,mu) of the multiple root */
	mqi_vect_t root;
	mqi_vect_init_dynamic (root, 2, mqi_mat_typeof(q1));

	/** A quadric different from the one used to parametrize: q_other */
	
	/** The usual trick to avoid the case where the root is "at infinity" */
	mqi_mat_t q_other;
	if ( mqi_coef_sign(POLY(gcd_p,0)) ) {

		mqi_coef_mul_slong (VECT(root,0), POLY(gcd_p,0), (slong)d);
		mqi_coef_neg (VECT(root,0), VECT(root,0));
		mqi_coef_cpy (VECT(root,1), POLY(gcd_p,1));
		mqi_mat_init_cpy (q_other,q2);

	}else { /* Root is [0 1] */

		mqi_vect_set_at_int (root,1, 1);
		mqi_mat_init_cpy (q_other,q1);

	}

	/** Simplify the coordinates of the root */
	qi_vect_optimize (root);

	/** The matrix associated to the multiple root */
	mqi_mat_t q;

	mqi_mat_init_dynamic(q, mqi_mat_nrows(q1), mqi_mat_ncols(q1), mqi_mat_typeof(q1));
	mqi_mat_linexpr(q,2,	VECT(root,0),q1,	VECT(root,1),q2);

	/** The singular locus of q (not needed in all cases, but eases readability of the code) */
	mqi_mat_t q_sing;
	qi_mat_singular (q_sing, q);

	/** Its inertia */
	/** $$ The "inertia" routines manipulate a vector of sshort, so we can
	 *  directly initialize it as a vector of sshort. */
	mqi_vect_t in_q;
	qi_mat_inertia (in_q, q);

	/** Its rank */
	mqi_coef_t rank_q;
	mqi_coef_init_dynamic (rank_q, mqi_vect_typeof(in_q));
	mqi_coef_add (rank_q, VECT(in_q,0), VECT(in_q,1));

	/* Compute the polynomial det_e such that det_p = det_e * (root[1]*x - root[0]*y)^(1+d)
	 * Compute the sign s_e of the value of det_e at the multiple root */
	
	/** det_e not really needed when quadruple root (s_e is easy to compute) */
	mqi_hpoly_t det_e;
	int s_e;

	if ( (d == 1) || (d == 2) ) { /** Double or triple root */

		mqi_hpoly_t lin, ptmp;

		/** First build the polynomial (root[1]*x - root[0]*y)^(d+1) */
		mqi_hpoly_init_dynamic (ptmp, mqi_vect_typeof(root));
		
		mqi_poly_set_homogeneous(ptmp, 1);
		mqi_coef_neg (POLY(ptmp,0), VECT(root,0));
		mqi_coef_cpy (POLY(ptmp,1), VECT(root,1));

		/** Polynomial det_e */
		mqi_hpoly_init_dynamic (lin, mqi_poly_typeof(ptmp));
		mqi_poly_pow	       (lin, ptmp, (size_t)d+1);
	
		mqi_coef_t alpha;
		mqi_poly_t R;
		mqi_poly_div_qr (det_e, R, alpha, det_p, lin);

		/** Sign s_e */
		mqi_poly_eval_xw (alpha, det_e, VECT(root,0), VECT(root,1));
		s_e = mqi_coef_sign(alpha);

		/** Free local memory */
		mqi_poly_list_clear(lin, ptmp, R, NULL);
		mqi_coef_clear(alpha);

	}else {  /** Quadruple real root - d = 3 (no need to compute det_e here, s_e is enough) */

		if ( ! mqi_coef_sign (VECT(root,1)) ) {
			s_e = mqi_coef_sign (POLY(det_p,0));
		}else {
			s_e = mqi_coef_sign (POLY(det_p,d+1));
		}
	
	}

#ifdef DEBUG
	mqi_log_printf ("Root:\033[20G"); mqi_vect_print(root); mqi_log_printf("\n");
	mqi_log_printf ("Inertia:\033[20G"); mqi_vect_print(in_q); mqi_log_printf("\n");
#endif

	/*******************************************************************************
	** The main switch 
	*******************************************************************************
	** d = 1: double real root case - The rank of the singular quadric can be
	** either 3 or 2
	*******************************************************************************/
	if ( d == 1 ) {

#ifdef DEBUG
		mqi_log_printf ("Double real root\n");
#endif

		/** Discriminant delta_e of det_e */
		mqi_coef_t delta_e;

		mqi_coef_init_dynamic (delta_e, mqi_poly_typeof(det_e));
		qi_hpoly_pseudo_discriminant (delta_e, det_e); /** Corresponds to "discriminant2" */

		/** ***************************** */
		/** Rank of singular quadric is 3 */
		/** ***************************** */
		if ( mqi_coef_cmp_schar (rank_q,3) == 0 ){
			/** $$ The promotion to "int" is valid, as we know that the inertia vector is a vector of signed shorts. */
			__qi_inter_one_mult_nodal_quartic (rop,q1,q2,q,q_other,q_sing,delta_e,s_e,mqi_coef_get_int(VECT(in_q,0)));
		}
		/** ***************************** */
		/** Rand of singular quadric is 2 */
		/** ***************************** */
		else {
			/** (C) Two secant conics */
			if ( mqi_coef_sign(delta_e) < 0 ) {
				/** Other roots of determinantal equation are complex */
				if ( s_e == 1 )
					__qi_inter_one_mult_secant_conics (rop,q1,q2,det_p_orig,q,q_other,q_sing,delta_e,
									   mqi_coef_get_int(VECT(in_q,0)));
				else
					__qi_inter_one_mult_secant_conics_no_sec (rop,q1,q2,det_p_orig,q,q_other,q_other,q_sing,delta_e);
			}else{
				/** Other roots of determinantal equation are real */
				if ( mqi_coef_cmp_schar (VECT(in_q,0),1) == 0 ) {
					/** Pair of planes corresponding to double root is real */
					if ( s_e == 1 )
						__qi_inter_one_mult_secant_conics (rop,q1,q2,det_p_orig,q,q_other,q_sing,delta_e,
										  mqi_coef_get_int(VECT(in_q,0)));
					else {
						/** s_e = -1 */
						/** Decide in which case we are by testing for the presence of a [4 0] in the pencil */

						/** A test point */
						mqi_vect_t root_e;
						mqi_vect_init_dynamic (root_e, 2, mqi_mat_typeof(q1));
				
						if ( mqi_coef_sign(POLY(det_e,2)) < 0 ) {
							/** The double root is outside the simple roots */

							/** Pick a point between the roots */
							mqi_coef_neg (VECT(root_e,0), POLY(det_e,1));
							mqi_coef_mul_schar (VECT(root_e,1), POLY(det_e,2), 2);
						}else if (mqi_coef_sign(POLY(det_e,2)) > 0) {
							/** The double root is between the simple roots */

							/** Take infinity as test point */
							mqi_vect_set_at_int (root_e,0, 1);
						}
						else { /** det_e[2] = 0 */
							if ( mqi_coef_sign(POLY(det_e,1)) > 0 ) {
								/** The double root is outside */
								mqi_coef_abs (VECT(root_e,0), POLY(det_e,0));
								mqi_coef_add_schar (VECT(root_e,0), VECT(root_e,0), 1);
								mqi_coef_cpy (VECT(root_e,1), POLY(det_e,1));
							}else{
								mqi_coef_abs (VECT(root_e,0), POLY(det_e,0));
								mqi_coef_add_schar (VECT(root_e,0), VECT(root_e,0), 1);
								mqi_coef_neg (VECT(root_e,0), VECT(root_e,0));
								mqi_coef_neg (VECT(root_e,1), POLY(det_e,1));
							}
						}

						/** The associated quadric */
						mqi_mat_t q_det_positive;

						mqi_mat_init_dynamic(q_det_positive, mqi_mat_nrows(q1), mqi_mat_ncols(q1),
									mqi_mat_typeof(q1));
						mqi_mat_linexpr(q_det_positive,2,	VECT(root_e,0),q1,	VECT(root_e,1),q2);

						mqi_vect_t vect;
						qi_mat_inertia_known_rank (vect, q_det_positive, 4);


						mqi_coef_t rank0;
						mqi_coef_init_cpy (rank0, VECT(vect,0));

						if ( mqi_coef_cmp_schar (rank0, 2) == 0 )
							__qi_inter_one_mult_secant_conics_no_sec (rop,q1,q2,det_p_orig,q,q_other,q_det_positive,q_sing,
												  delta_e);
						else {
							/** rank0 = 4 */
							qi_inter_init(rop);
							qi_inter_set_type (rop, 3,1);
						}

						/** Free local memory */
						mqi_mat_clear (q_det_positive);
						mqi_vect_list_clear (vect, root_e, NULL);
						mqi_coef_clear (rank0);
			
					}
				}
				else {
					/** Singular pair of planes is imaginary */
					if ( s_e == 1 ) {

						qi_inter_init(rop);
						qi_inter_set_type (rop, 3,1);

					}
					else /** Two points */
						__qi_inter_one_mult_secant_conics (rop,q1,q2,det_p_orig,q,q_other,q_sing,delta_e,
										   mqi_coef_get_int(VECT(in_q,0)));

				}


			}
		}

		/** Free local memory */
		mqi_coef_clear(delta_e);

	}
	else if ( d == 2 ) {
		/*******************************************************************************
		 ** d = 2: triple real root case - Rank of singular quadric can be either 3,
		 ** 2 or 1
		 ******************************************************************************* */
		
#ifdef DEBUG
		mqi_log_printf ("Triple real root\n");
#endif

		/** ***************************** */
		/** Rank of singular quadric is 3 */
		/** ***************************** */
		if ( mqi_coef_cmp_schar (rank_q,3) == 0 ) /** (C) Cuspidal quartic */
			__qi_inter_one_mult_cuspidal_quartic (rop,q1,q2,q,q_other,q_sing);
		else if ( mqi_coef_cmp_schar (rank_q,2) == 0 ) {

			/** ***************************** */
			/** Rank of singular quadric is 2 */
			/** ***************************** */

			/** (C) Two tangent conics */

			/** Here, q_other should be the cone of the pencil */

			mqi_coef_t coef;

			mqi_coef_init_cpy(coef, POLY(det_e,0));
			mqi_coef_neg(coef,coef);

			mqi_mat_init_dynamic(q_other, mqi_mat_nrows(q1), mqi_mat_ncols(q1), mqi_mat_typeof(q1));
			mqi_mat_linexpr(q_other,2,	coef,q1,	POLY(det_e,1),q2);

			__qi_inter_one_mult_two_tangent_conics (rop,q1,q2,q,q_other,q_sing, mqi_coef_get_int(VECT(in_q,0)));

			/** Free local memory */
			mqi_coef_clear(coef);

		}else {
			/** ***************************** */
			/** Rank of singular quadric is 1 */
			/** ***************************** */

			/** (C) Double conic */
			__qi_inter_one_mult_double_conic (rop,q1,q2,q,q_sing,det_e);

		}


	}else {
		/*******************************************************************************
		** d = 3: quadruple real root case - Rank of singular quadric can be either 3,
		** 2, 1 or 0
		******************************************************************************** */
		
#ifdef DEBUG
		mqi_log_printf ("Quadruple real root\n");
#endif

		/** ***************************** */
		/** Rank of singular quadric is 3 */
		/** ***************************** */
		
		if ( mqi_coef_cmp_schar (rank_q,3) == 0 ) {
			/** (C) Cubic and tangent line */
			__qi_inter_one_mult_cubic_tangent_line (rop,q1,q2,q,q_other,q_sing);
		}else if ( mqi_coef_cmp_schar (rank_q,2) == 0 ) {
			/** ***************************** */
			/** Rank of singular quadric is 2 */
			/** ***************************** */
			if ( mqi_coef_cmp_schar (VECT(in_q,0),2) == 0 ) {
				/** Pair of planes is imaginary */
				
				/** Two skew lines and double line, only the double line is real */
				__qi_inter_one_mult_two_skew_lines_double_line (rop,q1,q2,q,q_other,q_sing,0);
			}else {
				/** Pair of planes is real */
				if ( s_e == 1 ) {
					/* If the singular line of the pair of planes is entirely contained
					* in the other quadrics of the pencil, then 2 lines & double line.
					* Otherwise, conic & 2 lines crossing on conic */
					mqi_mat_t tmp;
					mqi_mat_transformation (tmp, q_other, q_sing, MQI_MAT_NOTRANSCO);

					if ( (mqi_coef_sign(MAT(tmp,0,0)) == 0) &&
					     (mqi_coef_sign(MAT(tmp,0,1)) == 0) &&
					     (mqi_coef_sign(MAT(tmp,1,1)) == 0) )
						/** Two skew lines and double line, everything is real */
						__qi_inter_one_mult_two_skew_lines_double_line(rop,q1,q2,q,q_other,q_sing,1);
					else
						/** Conic and two lines (crossing), everything is real */
						__qi_inter_one_mult_conic_2lines_crossing (rop,q1,q2,q,q_other,q_sing,1);
				
				
					mqi_mat_clear(tmp);

				}
				else /* s_e = -1 */
				{
					/* Conic and two lines (crossing), only the conic is real */
					__qi_inter_one_mult_conic_2lines_crossing (rop,q1,q2,q,q_other,q_sing,0);

				}
			}
		}else if ( mqi_coef_cmp_schar (rank_q,1) == 0 ) {
			/** ***************************** */
			/** Rank of singular quadric is 1 */
			/** ***************************** */

			/** (C) Two double lines */
			__qi_inter_one_mult_two_double_lines (rop,q1,q2,q,q_other,q_sing,s_e);
		}else {
			/** ***************************** */
			/** Rank of singular quadric is 0 */
			/** ***************************** */
			
			/** Any quadric of the pencil */

			/** To avoid picking the zero matrix */
			if ( ! mqi_coef_sign (VECT(root,0)) )
				mqi_mat_init_cpy (q, q1);
			else
				mqi_mat_init_cpy (q, q2);

			/** Its inertia */
			qi_mat_inertia_known_rank (in_q, q,4);

			/** Inertia [4 0] */
			if ( mqi_coef_cmp_schar (VECT(in_q,0),4) == 0 ) {

				qi_inter_init(rop);
				qi_inter_set_type (rop, 11,1);

			}else{

				qi_inter_init(rop);

				if ( mqi_coef_cmp_schar (VECT(in_q,0),3) == 0 )
					qi_inter_set_type (rop, 11,2);	
				else
					qi_inter_set_type (rop, 11,3);

				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, q);
				qi_inter_component_set_type	 (comp, QI_INTER_COMPONENT_SMOOTH_QUADRIC);

				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

			}

		}

	}

	/** Free local memory */
	mqi_mat_list_clear(q, q_other, q_sing, NULL);
	mqi_vect_list_clear(root, in_q, NULL);
	mqi_poly_clear(det_e);
	mqi_coef_clear(rank_q);	

}

/** Two lines crossing on a conic */
void __qi_inter_one_mult_conic_2lines_crossing (qi_inter_t __IN rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					        mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
					        int type_flag)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;
	
	qi_inter_init(rop);

	if ( type_flag )
		qi_inter_set_type (rop, 8,2);
	else
		qi_inter_set_type (rop, 8,1);

	/** First, parametrize the pair of planes */
	mqi_coef_t D;
	mqi_mat_t m1, m2;

	mqi_coef_init_dynamic(D, mqi_mat_typeof(q));
	mqi_mat_init_dynamic (m1, 4,3, mqi_mat_typeof(q));
	mqi_mat_init_dynamic (m2, 4,1, mqi_mat_typeof(q));

	/** $$ Important note: Normally, coefficients can be initialized
	 *  automatically by subfunctions, as there's not sizing problems */
	qi_param_plane_pair  (q, q_sing, D, m1, m2);

	/** The two planes are rational so we can directly replace the first column of m1 by
	 *  m1(0) +/- m2(0) */
	mqi_mat_t m1m, m1p;
	mqi_mat_init_cpy (m1m, m1);
	mqi_mat_init_cpy (m1p, m1);

	mqi_vect_add ((mqi_vect_ptr)COL(m1p,0), (mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m2,0));
	mqi_vect_sub ((mqi_vect_ptr)COL(m1m,0), (mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m2,0));

	qi_mat_optimize (m1m);
	qi_mat_optimize (m1p);

	/** Now we compute the following two matrices: one has inertia [2 1] and represents a conic.
	 *  The other has inertia [2 0] and represents the imaginary pair of lines */
	mqi_mat_t ap, am;
	mqi_mat_transformation (am, q_other, m1m, MQI_MAT_NOTRANSCO);
	mqi_mat_transformation (ap, q_other, m1p, MQI_MAT_NOTRANSCO);

	/** Swap if ap is not the conic of inertia [2 1] */
	mqi_coef_t det;
	mqi_coef_init_dynamic (det, mqi_mat_typeof(ap));
	mqi_mat_det (det, ap);

	if ( mqi_coef_sign(det) == 0 )
	{
		mqi_mat_swap(ap,am);
		mqi_mat_swap(m1p,m1m);
	}

	/** The rational singular point of the pair of lines */
	mqi_vect_t line_sing, line_sing3d;
	
	mqi_mat_t sing;
	qi_mat_singular (sing, am);

	mqi_vect_init_cpy(line_sing, (mqi_vect_ptr)COL(sing,0));

	mqi_mat_t tmpmat;
	mqi_mat_mul_init(tmpmat, m1m, (mqi_mat_ptr)line_sing);

	mqi_vect_init_cpy(line_sing3d, (mqi_vect_ptr)COL(tmpmat,0));


	/** Parametrization of the conic */
	mqi_mat_t p_trans;
	mqi_curve_param_t conic_2d;
	mqi_vect_t l;

	mqi_mat_init_dynamic (p_trans, 3, 3, mqi_mat_typeof(q1));
	mqi_curve_param_init_dynamic (conic_2d, 3, mqi_mat_typeof(q));
	mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q1));

	qi_param_conic_through_ratpoint (ap, line_sing, p_trans, conic_2d, l);

	/** Stacks the tarnsformations */
	mqi_mat_clear(tmpmat);
	mqi_mat_init_cpy(tmpmat, p_trans);
	mqi_mat_clear(p_trans);
	mqi_mat_mul_init(p_trans, m1p, tmpmat);

	qi_mat_optimize_3 (p_trans);

	/** u=1 and v=0 must correspond to the rational point */
	mqi_poly_swap(CP(conic_2d,0), CP(conic_2d,1));

	mqi_curve_param_t conic;
	mqi_curve_param_init_dynamic (conic, 4, mqi_mat_typeof(q));
	mqi_curve_param_mul_mat	     (conic, p_trans, conic_2d);

	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	if ( type_flag )
	{

		/** The intersection also contains two lines */
		mqi_coef_t D;
		mqi_mat_t m1, m2;

		mqi_coef_init_dynamic(D, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m1, 3, 2, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m2, 3, 1, mqi_mat_typeof(q1));

		qi_param_line_pair (am, line_sing, D, m1, m2);

		if ( mqi_coef_sign(D) == 0 ) {
			
			/** The two lines are rational */
			mqi_mat_t l1p, l1m;

			mqi_mat_init_cpy (l1m, m1);
			mqi_mat_init_cpy (l1p, m1);

			mqi_vect_add((mqi_vect_ptr)COL(l1p,0), (mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m2,0));
			mqi_vect_sub((mqi_vect_ptr)COL(l1m,0), (mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m2,0));

			mqi_mat_t linep, linem;
			mqi_mat_mul_init (linem, m1m, l1m);
			mqi_mat_mul_init (linep, m1m, l1p);

			qi_mat_optimize (linem);
			qi_mat_optimize (linep);

			/** The two lines */
			mqi_curve_param_t line1, line2;
			mqi_curve_param_init_dynamic(line1, mqi_mat_nrows(linep), mqi_mat_typeof(linep));
			mqi_curve_param_init_dynamic(line2, mqi_mat_nrows(linem), mqi_mat_typeof(linem));

			mqi_curve_param_line_through_two_points (line1,
					(mqi_vect_ptr)COL(linep,0), (mqi_vect_ptr)COL(linep,1));
			mqi_curve_param_line_through_two_points (line2,
					(mqi_vect_ptr)COL(linem,0), (mqi_vect_ptr)COL(linem,1));

			/** $$ TODO: cut parameters */

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line1);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line2);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_mat_list_clear(l1p, l1m, linep, linem, NULL);
			mqi_curve_param_list_clear(line1, line2, NULL);

		}else{

			/** The lines are not rational */

			/** Stacks the transformations */
			mqi_mat_t tmpmat, tmpmat2;

			mqi_mat_init_cpy(tmpmat,m1);
			mqi_mat_clear(m1);
			mqi_mat_mul_init (m1, m1m, tmpmat);
			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat,m2);
			mqi_mat_clear(m2);
			mqi_mat_mul_init (tmpmat2, m1m, tmpmat);
			mqi_mat_resize 	(m2, tmpmat2, 4, 2);

			qi_mat_optimize_2 (m1, m2);
			
			/** The two components of the lines */
			mqi_curve_param_t line_rat, line_D;
			mqi_curve_param_init_dynamic(line_rat, mqi_mat_nrows(m1), mqi_mat_typeof(m1));
			mqi_curve_param_init_dynamic(line_D, mqi_mat_nrows(m2), mqi_mat_typeof(m2));

			mqi_curve_param_line_through_two_points (line_rat,
					(mqi_vect_ptr)COL(m1,0), (mqi_vect_ptr)COL(m1,1));
			mqi_curve_param_line_through_two_points (line_D,
					(mqi_vect_ptr)COL(m2,0), (mqi_vect_ptr)COL(m2,1));

			mqi_vect_t ignored;
			qi_param_line_improved2 (ignored, line_rat, line_D, D);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
						    line_rat, line_D, D);

			/** Free local memory */
			mqi_mat_list_clear(tmpmat, tmpmat2, NULL);
			mqi_curve_param_list_clear(line_rat, line_D, NULL);
			mqi_vect_clear(ignored);

		}

		/** $$ Temporary: note that the "if (type_flag)" test has been removed,
		 *  we're already under that condition here.
		 */
		mqi_coef_t a;
		mqi_coef_init_dynamic (a, mqi_mat_typeof(q1));

		qi_inter_cut_init (cut, QI_INTER_CUT_0, a,a);
		
                qi_inter_component_add_cut_parameter (rop->components[0], cut);
                qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_component_add_cut_parameter (rop->components[2], cut);

		qi_inter_cut_set_owner(cut, rop->components[0]);

		/** Free local memory */
		mqi_coef_list_clear(a, D, NULL);
		mqi_mat_list_clear(m1, m2, NULL);

	}

	qi_inter_set_optimal (rop);


	/** Free local memory */
	mqi_curve_param_list_clear(conic, conic_2d, NULL);
	mqi_vect_list_clear(line_sing, line_sing3d, l, NULL);
	mqi_mat_list_clear(m1, m2, m1p, m1m, ap, am, sing, tmpmat, p_trans, NULL);
	mqi_coef_list_clear(D, det, NULL);

}

/** Two skew lines and double line */
void __qi_inter_one_mult_two_skew_lines_double_line (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
						     mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
						     int type_flag)
{
	
	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	qi_inter_init(rop);

	if ( type_flag )
		qi_inter_set_type (rop, 9,2);
	else
		qi_inter_set_type (rop, 9,1);

	mqi_curve_param_t line_par;
	mqi_curve_param_init_dynamic (line_par, mqi_mat_nrows(q_sing), mqi_mat_typeof(q_sing));
	mqi_curve_param_line_through_two_points (line_par,
		(mqi_vect_ptr)COL(q_sing,0), (mqi_vect_ptr)COL(q_sing,1));

	/** $$ TODO: Cut parameters */
	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line_par);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
	qi_inter_component_set_multiplicity (comp, 2);
	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	if ( type_flag ) {

		/** There are also two skew lines */
		
		/** First, parametrize the pair of planes */
		mqi_coef_t D;
		mqi_mat_t m1, m2;
		mqi_mat_init_dynamic (m1, 4, 3, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m2, 4, 1, mqi_mat_typeof(q2));

		qi_param_plane_pair (q, q_sing, D, m1, m2);

		/** We want to compute (m1*par1)^T*q_other*(m1*par1), but we don't do
		  * it explicitly. But since the singular line of the pair of planes
		  * is a factor of this plug (corresponding to 's'), the matrix has a very
		  * special shape, i.e. the 2x2 lower right matrix is zero. */
		mqi_mat_t a, c, tmpmat;
		mqi_coef_t b0;
		mqi_mat_transformation (a, q_other, m1, MQI_MAT_NOTRANSCO);
		mqi_mat_transformation (tmpmat, q_other, m2, MQI_MAT_NOTRANSCO);
		mqi_coef_init_cpy (b0, MAT(tmpmat,0,0));
		qi_mat_transformation_int (c, 1, m1, q_other, m2);

		/** Extract those coefficients that are eneded */
		mqi_coef_t a0, a1, a2, c0, c1, c2;
		mqi_coef_init_cpy   (a0, MAT(a,0,0));
		mqi_coef_init_cpy   (a1, MAT(a,0,1));
		mqi_coef_init_cpy   (a2, MAT(a,0,2));
		mqi_coef_init_cpy   (c0, MAT(c,0,0));
		mqi_coef_init_cpy   (c1, MAT(c,1,0));
		mqi_coef_init_cpy   (c2, MAT(c,2,0));

		mqi_coef_mul_schar  (a1, a1, 2);
		mqi_coef_mul_schar  (a2, a2, 2);
		mqi_coef_mul_schar  (c0, c0, 2);
		mqi_coef_mul_schar  (c1, c1, 2);
		mqi_coef_mul_schar  (c2, c2, 2);

		if ( mqi_coef_sign(D) == 0 ) {

			/** Each plane of the pair is rational.
			  * The plugs of the plane params in q_other are
			  * s*(a0+b0+/-c0)+t*(a1+/-c1)+v*(a2+/-c2) = 0
			 */
		
			/** Thus we can extract v = p(s,t) and build a new 4x2 transformation matrix.
			  * If the coefficients of v is zero, a special trick must be used. */
			mqi_mat_t vp, vm;
			mqi_coef_t ab0;

			mqi_coef_init_cpy (ab0, a0);
			mqi_coef_add	  (ab0, ab0, b0);

			mqi_mat_list_init_dynamic (4,2, mqi_mat_typeof(q1), vp, vm, NULL);

			mqi_coef_t tmp;
			mqi_coef_init_cpy (tmp, a2);
			mqi_coef_add	  (tmp, tmp, c2);

			mqi_mat_t m1m, m1p;
			mqi_coef_t coef, coef2;

			mqi_mat_init_cpy(m1p, m1);
			mqi_mat_init_cpy(m1m, m1);

			mqi_mat_add(m1p, m1p, m2);
			mqi_mat_sub(m1m, m1m, m2);

			mqi_coef_list_init_dynamic(mqi_coef_typeof(ab0), coef, coef2, NULL);

			if ( mqi_coef_sign(tmp) == 0 ) {

				mqi_mat_cpy_col (vp, m1,2, 0);
				mqi_coef_add(coef, a1, c1);
				mqi_coef_add(coef2, ab0, c0);
				mqi_coef_neg(coef2,coef2);

				mqi_mat_linexpr(COL(vp,1),2,	coef,COL(m1p,0), coef2,COL(m1,1));

			}else{
			
				mqi_coef_add(coef, ab0, c0);
				mqi_coef_neg(coef,coef);
				mqi_mat_linexpr(COL(vp,0),2,	tmp,COL(m1p,0),	coef,COL(m1,2));

				mqi_coef_add(coef, a1, c1);
				mqi_coef_neg(coef,coef);
				mqi_mat_linexpr(COL(vp,1),2,	tmp,COL(m1,1),	coef,COL(m1,2));

			}	

			mqi_coef_sub (tmp, a2, c2);
		
			if ( mqi_coef_sign(tmp) == 0 ) {

				mqi_mat_cpy_col (vp, m1,2, 0);
				mqi_coef_sub(coef, a1, c1);
				mqi_coef_sub(coef2, c0, ab0);
				mqi_mat_linexpr(COL(vp,1),2,	coef,COL(m1p,0), coef2,COL(m1,1));

			}else{
			
				mqi_coef_sub(coef, c0, ab0);
				mqi_mat_linexpr(COL(vp,0),2,	tmp,COL(m1p,0),	coef,COL(m1,2));

				mqi_coef_sub(coef, c1, a1);
				mqi_mat_linexpr(COL(vp,1),2,	tmp,COL(m1,1),	coef,COL(m1,2));

			}

			qi_mat_optimize(vp);
			qi_mat_optimize(vm);

			/** The two skew lines */
			mqi_curve_param_t linep, linem;
			mqi_curve_param_init_dynamic (linep, mqi_mat_nrows(vp), mqi_mat_typeof(vp));
			mqi_curve_param_init_dynamic (linem, mqi_mat_nrows(vm), mqi_mat_typeof(vm));

			mqi_curve_param_line_through_two_points (linep,
					(mqi_vect_ptr)COL(vp,0), (mqi_vect_ptr)COL(vp,1));
			mqi_curve_param_line_through_two_points (linem,
					(mqi_vect_ptr)COL(vm,0), (mqi_vect_ptr)COL(vm,1));

			/** $$ TODO: Cut parameters */
			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, linem);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, linep);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_add_component (rop, comp);
			/* discard comp */

			/** Free local memory */
			mqi_mat_list_clear(m1p, m1m, vp, vm, NULL);
			mqi_coef_list_clear(coef, coef2, ab0, tmp, NULL);
			mqi_curve_param_list_clear(linep, linem, NULL);

		}
		else {
			
			/** Individual planes are not rational.
			  * The plugs of the plane params in q_other are
			  * s*(a0+D*b0)+t*a1+v*a2 +/- sqrt(D)*(c0*s+c1*t+c2*v) = 0 */
			
			/** Extract dt*v = (cs_rat*s+ct_rat*t) + sqrt(D)*(cs_D*s+ct_D*t) */
			mqi_coef_t ab0, dt, cs_rat, ct_rat, cs_D, ct_D;

			mqi_coef_list_init_dynamic(mqi_coef_typeof(D), ab0, dt, cs_rat, ct_rat, cs_D, ct_D, NULL);

			mqi_coef_linexpr2 (ab0,2,	1,NULL,a0,	1,D,b0);
			mqi_coef_linexpr3 (dt,2,	1,NULL,a2,a2,	-1,D,c2,c2);
			mqi_coef_linexpr3 (cs_rat,2,	1,D,c2,c0,	-1,NULL,a2,ab0);
			mqi_coef_linexpr3 (ct_rat,2,	1,D,c2,c1,	-1,NULL,a2,a1);
			mqi_coef_linexpr2 (cs_D,2,	1,ab0,c2,	-1,a2,c0);
			mqi_coef_linexpr2 (ct_D,2,	1,a1,c2,	-1,a2,c1);

			/** Build new projection matrices */
			mqi_mat_t m_rat, m_D;
			mqi_mat_list_init_dynamic(4, 2, mqi_mat_typeof(q1), m_rat, m_D, NULL);

			mqi_vect_linexpr ((mqi_vect_ptr)COL(m_rat,0), 2,
					  dt,(mqi_vect_ptr)COL(m1,0),	cs_rat,(mqi_vect_ptr)COL(m1,2));
			mqi_vect_linexpr ((mqi_vect_ptr)COL(m_rat,1), 2,
					  dt,(mqi_vect_ptr)COL(m1,1),	ct_rat,(mqi_vect_ptr)COL(m1,2));
			mqi_vect_linexpr ((mqi_vect_ptr)COL(m_D,0), 2,
					  dt,(mqi_vect_ptr)COL(m2,0),	cs_D,(mqi_vect_ptr)COL(m1,2));
			mqi_vect_linexpr ((mqi_vect_ptr)COL(m_D,1), 1,
					  ct_D, (mqi_vect_ptr)COL(m1,2));

			qi_mat_optimize_2 (m_rat, m_D);

			mqi_curve_param_t line_rat, line_D;
			mqi_curve_param_initpool_dynamic (line_rat, mqi_mat_nrows(m_rat), mqi_mat_typeof(m_rat));
			mqi_curve_param_initpool_dynamic (line_D, mqi_mat_nrows(m_D), mqi_mat_typeof(m_D));

			mqi_curve_param_line_through_two_points (line_rat,
					(mqi_vect_ptr)COL(m_rat,0), (mqi_vect_ptr)COL(m_rat,1));
			mqi_curve_param_line_through_two_points (line_D,
					(mqi_vect_ptr)COL(m_D,0), (mqi_vect_ptr)COL(m_D,1));

			/** $$ TODO: Cut parameters ? */
			mqi_vect_t ignored;
			qi_param_line_improved2 (ignored, line_rat, line_D, D);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
						    line_rat, line_D, D);

			/** Free local memory */
			mqi_curve_param_list_clear(line_rat, line_D, NULL);
			mqi_vect_clear(ignored);
			mqi_mat_list_clear(m_rat, m_D, NULL);
			mqi_coef_list_clear(ab0, dt, cs_rat, ct_rat, cs_D, ct_D, NULL);
		
		}	

		/** $$ Temporary */
		mqi_coef_t coef;
		mqi_coef_init_dynamic (coef, mqi_mat_typeof(q1));
		qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);
		
		/** $$ Is it correct the way I add a double cut parameter ? */
                qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
                qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_component_add_cut_parameter (rop->components[2], cut);

		qi_inter_cut_set_owner(cut, rop->components[0]);

		/** Free local memory */
		mqi_coef_list_clear(coef, D, b0, a0, a1, a2, c0, c1, c2, NULL);
		mqi_mat_list_clear(m1, m2, a, c, tmpmat, NULL);

	}

	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_curve_param_clear(line_par);

}

/** Two double lines */
void __qi_inter_one_mult_two_double_lines (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					   mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
					   int s_e)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;


	qi_inter_init(rop);

	if ( s_e == 1 )
		qi_inter_set_type	(rop, 10,2);
	else
		qi_inter_set_type	(rop, 10,1);
	
	/** First, parametrize the double plane q */
	mqi_mat_t m1;
	mqi_mat_init_dynamic (m1, 4,3, mqi_mat_typeof(q1));

	qi_param_plane_double (q, q_sing, m1);

	qi_mat_optimize (m1);

	/** Now, we have a 3x3 matrix representing a real or imaginary pair of
	 *  lines */
	mqi_mat_t am;
	mqi_mat_transformation (am, q_other, m1, MQI_MAT_NOTRANSCO);

	/** The singular point of this pair of lines */
	mqi_mat_t sing;
	qi_mat_singular (sing, am);

	mqi_vect_t sing_l_2d;
	mqi_vect_init_cpy(sing_l_2d, (mqi_vect_ptr)COL(sing,0));

	if ( s_e == -1 ) {

		/** The intersection is reduced to the singular point of the
		 *  pair of lines */
		mqi_vect_t sing_l;
		mqi_vect_init_dynamic (sing_l, mqi_mat_nrows(m1), mqi_vect_typeof(sing_l_2d));
		mqi_mat_mul ((mqi_mat_ptr)sing_l, m1, (mqi_mat_ptr)sing_l_2d);

		qi_vect_optimize (sing_l);

		mqi_curve_param_t sing_par;
		mqi_curve_param_init_dynamic (sing_par, mqi_vect_size(sing_l), mqi_vect_typeof(sing_l));
		mqi_curve_param_set_vect     (sing_par, sing_l);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, sing_par);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

		qi_inter_add_component(rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		/** Free local memory */
		mqi_vect_clear(sing_l);
		mqi_curve_param_clear(sing_par);

	}else{

		/** Two double lines */

		/** Parametrize the pair of lines */
		mqi_coef_t D;
		mqi_mat_t m1_l, m2_l;
		mqi_mat_init_dynamic (m1_l, 3,2, mqi_mat_typeof(q1));
		mqi_mat_init_dynamic (m2_l, 3,1, mqi_mat_typeof(q1));

		qi_param_line_pair (am, sing_l_2d, D, m1_l, m2_l);

		if ( mqi_coef_sign(D) == 0 ) {
	
			/** The two lines are rational */
			mqi_mat_t l1p, l1m;
			mqi_mat_init_cpy (l1p, m1_l);
			mqi_mat_init_cpy (l1m, m1_l);
		
			mqi_vect_add((mqi_vect_ptr)COL(l1p,0),
				(mqi_vect_ptr)COL(m1_l,0), (mqi_vect_ptr)COL(m2_l,0));
			mqi_vect_sub((mqi_vect_ptr)COL(l1m,0),
				(mqi_vect_ptr)COL(m1_l,0), (mqi_vect_ptr)COL(m2_l,0));

			/** Stacks the transformation matrices */
			mqi_mat_t linep, linem;
			mqi_mat_mul_init (linep, m1, l1p);
			mqi_mat_mul_init (linem, m1, l1m);

			qi_mat_optimize (linep);
			qi_mat_optimize (linem);

			/** The two lines */
			mqi_curve_param_t line1, line2;
			mqi_curve_param_init_dynamic (line1, mqi_mat_nrows(linep), mqi_mat_typeof(linep));
			mqi_curve_param_init_dynamic (line2, mqi_mat_nrows(linem), mqi_mat_typeof(linem));

			mqi_curve_param_line_through_two_points (line1,
					(mqi_vect_ptr)COL(linep,1), (mqi_vect_ptr)COL(linep,0));
			mqi_curve_param_line_through_two_points (line2,
					(mqi_vect_ptr)COL(linem,1), (mqi_vect_ptr)COL(linem,0));

			/** Cut parameters of the lines */
			mqi_vect_t cut1, cut2;
			qi_param_line_improved1 (cut1, line1);
			qi_param_line_improved1 (cut2, line2);

			qi_vect_optimize_by_half (cut1);
			qi_vect_optimize_by_half (cut2);

			
			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line1);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut1,0), VECT(cut1,1));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner (cut, comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line2);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
			qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cut2,0), VECT(cut2,1));
			qi_inter_component_add_cut_parameter (comp, cut);
			qi_inter_cut_set_owner (cut, comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);
	

			/** Free local memory */
			mqi_mat_list_clear(l1p, l1m, linep, linem, NULL);
			mqi_curve_param_list_clear(line1, line2, NULL);
			mqi_vect_list_clear(cut1, cut2, NULL);

		}else{

			/** The lines are not rational */

			/** Stacks the transformations */
			mqi_mat_t tmpmat, tmpmat2;

			mqi_mat_init_cpy(tmpmat, m1_l);
			mqi_mat_clear(m1_l);
			mqi_mat_mul_init (m1_l, m1, tmpmat);
			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat, m2_l);
			mqi_mat_clear(m2_l);
			mqi_mat_mul_init(tmpmat2, m1, tmpmat);
			mqi_mat_resize  (m2_l, tmpmat2, 4,2);

			qi_mat_optimize_2 (m1_l, m2_l);

			/** The components of the lines */
			mqi_curve_param_t line_rat, line_D;
			mqi_curve_param_init_dynamic (line_rat, mqi_mat_nrows(m1_l), mqi_mat_typeof(m1_l));
			mqi_curve_param_init_dynamic (line_D, mqi_mat_nrows(m2_l), mqi_mat_typeof(m2_l));

			mqi_curve_param_line_through_two_points (line_rat,
					(mqi_vect_ptr)COL(m1_l,1), (mqi_vect_ptr)COL(m1_l,0));
			mqi_curve_param_line_through_two_points (line_D,
					(mqi_vect_ptr)COL(m2_l,1), (mqi_vect_ptr)COL(m2_l,0));


			/** Reparametrize the lines */
			mqi_vect_t cutp;
			qi_param_line_improved2 (cutp, line_rat, line_D, D);

			qi_vect_optimize_by_half(cutp);

			qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(cutp,0), VECT(cutp,1), D, VECT(cutp,2));

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_LINE, QI_INTER_COMPONENT_3,
						    line_rat, line_D, D);
			qi_inter_component_add_cut_parameter (rop->components[0], cut);
			qi_inter_component_add_cut_parameter (rop->components[1], cut);
			qi_inter_cut_set_owner(cut, rop->components[0]);
	
			/** Free local memory */
			mqi_mat_list_clear(tmpmat, tmpmat2, NULL);
			mqi_curve_param_list_clear(line_rat, line_D, NULL);
			mqi_vect_clear(cutp);

		}

		qi_inter_component_set_multiplicity(rop->components[0], 2);
		qi_inter_component_set_multiplicity(rop->components[1], 2);

		/** Free local memory */
		mqi_mat_list_clear(m1_l, m2_l, NULL);
		mqi_coef_clear(D);

	}
	
	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_mat_list_clear(m1, am, sing, NULL);
	mqi_vect_clear(sing_l_2d);

}

/** Cuspidal quartic */
void __qi_inter_one_mult_cuspidal_quartic (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					   mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	qi_inter_init(rop);
	qi_inter_set_type (rop, 4,1);

	/** Singular point of cone */
	mqi_vect_t sing;
	mqi_vect_init_cpy (sing, (mqi_vect_ptr)COL(q_sing,0));

	qi_vect_optimize (sing);

	/** We need to compute a point on the double line that is the intersection of q with
	 *  the tangent plane to q_other at sing.
	 *  First compute the "normal" to the tangent plane of q_other at sing */
	mqi_vect_t dir;
	mqi_mat_mul_init ((mqi_mat_ptr)dir, q_other, (mqi_mat_ptr)sing);


	/** Now a rational point on the line - We can't use a classical solve because we
	 *  want an answer in bigints. (classical solve might introduce rationals) */
	mqi_vect_t rat_point;
	qi_solve_proj (rat_point, q, dir, sing);

	/** Try to find a point of small height on the line by looking at the parameter
	 *  values such that one of the ocmponents is zero */

	/** The line param */
	mqi_curve_param_t line_par;
	mqi_curve_param_init_dynamic (line_par, mqi_vect_size(sing),  mqi_vect_typeof(sing));
	mqi_curve_param_line_through_two_points (line_par, sing, rat_point);

	/** $$ TODO ? Cut parameters of the lines */
	/** See original code */
	
	mqi_vect_t ignored;
	qi_param_line_improved1 (ignored, line_par);

	mqi_vect_t vect;
	mqi_vect_init_dynamic (vect, mqi_curve_param_n_equations(line_par), mqi_curve_param_typeof(line_par));

	mqi_curve_param_eval_int(vect, line_par, 1,0);

	if ( mqi_vect_equals (sing, vect) )
	{
		/** $$ Maybe we can save CPU here */
		/*mqi_vect_clear(rat_point);
		mqi_vect_init_dynamic (rat_point, mqi_curve_param_n_equations(line_par), mqi_curve_param_typeof(line_par));*/
		mqi_curve_param_eval_int (rat_point, line_par, 0,1);
	}
	else
	{
		mqi_vect_clear(rat_point);
		mqi_vect_init_cpy(rat_point, vect);
	}

	/** Parametrize the cone */
	mqi_surface_param_t par;
	mqi_mat_t p_trans;
	mqi_vect_t l;

	mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q));
	qi_param_cone_through_ratpoint (q, sing, rat_point, p_trans, par, l);

	qi_mat_optimize_3_update (p_trans, l);

	/** The quadric in the canonical frame of the cone */
	mqi_mat_t q_tmp;
	mqi_mat_transformation (q_tmp, q_other, p_trans, MQI_MAT_NOTRANSCO);

	/** Plug in the other quadric */
	mqi_hhpoly_t res;
	qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

	/** The solution (-l[1],l[0]) corresponds to the singular point of the quartic.
	 *  Let's rescale the thing so that it now stands at (1,0) */
	mqi_vect_t m;
	mqi_vect_init_dynamic (m, 2, mqi_mat_typeof(q1));

	if ( mqi_coef_sign(VECT(l,0)) != 0 ) {
		mqi_coef_linexpr2 (VECT(m,0),1,	1,VECT(l,0),HHCOEF(res,1,2));
		mqi_coef_linexpr2 (VECT(m,1),2,	1,VECT(l,0),HHCOEF(res,1,1),	-1,VECT(l,1),HHCOEF(res,1,2));
	}else {
		mqi_vect_set_at (m,0, HHCOEF(res,1,1));
		mqi_vect_set_at (m,1, HHCOEF(res,1,0));
	}

	if ( mqi_coef_sign(VECT(l,1)) == 0 ) {
		mqi_vect_set_at_int (m,0, 0);
		mqi_vect_set_at_int (m,1, 1);
	}else {
		mqi_vect_set_at_int (m,0, 1);
		mqi_vect_set_at_int (m,1, 0);
	}

	/** Now rewrite the par accordingly, i.e. replace l[0]*s+l[1]*t by t and
	 *  m[0]*s+m[1]*t by s */
	mqi_coef_linexpr2 (SPCOEF(par,0,1,2),1,		1,VECT(l,1),VECT(l,1));
	mqi_coef_linexpr2 (SPCOEF(par,0,1,1),1,		-2,VECT(m,1),VECT(l,1));
	mqi_coef_linexpr2 (SPCOEF(par,0,1,0),1,		1,VECT(m,1),VECT(m,1));
	mqi_coef_linexpr2 (SPCOEF(par,1,1,2),1,		1,VECT(l,0),VECT(l,0));
	mqi_coef_linexpr2 (SPCOEF(par,1,1,1),1,		-2,VECT(m,0),VECT(l,0));
	mqi_coef_linexpr2 (SPCOEF(par,1,1,0),1,		1,VECT(m,0),VECT(m,0));
	mqi_coef_linexpr2 (SPCOEF(par,2,1,2),1,		-1,VECT(l,0),VECT(l,1));
	mqi_coef_linexpr2 (SPCOEF(par,2,1,1),2,		1,VECT(l,0),VECT(m,1), 1,VECT(l,1),VECT(m,0));
	mqi_coef_linexpr2 (SPCOEF(par,2,1,0),1,		-1,VECT(m,0),VECT(m,1));

	/** And do the plug again */
	/* discard old res */
	mqi_hhpoly_clear(res);
	qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

	mqi_vect_set_at_int (l,0,  0);
	mqi_vect_set_at_int (l,1, -1);

	mqi_hpoly_t p1, p2;
	mqi_poly_init_cpy (p1, HHPOLY(res,2));
	mqi_poly_init_cpy (p2, HHPOLY(res,1));
	mqi_poly_neg	   (p2, p2);

	mqi_surface_param_t tmpsp;

	mqi_surface_param_init_cpy(tmpsp, par);
	mqi_surface_param_mul_mat (par, p_trans, tmpsp);

	mqi_curve_param_t curve;
	mqi_curve_param_init_dynamic (curve, 4, mqi_poly_typeof(p1));
	mqi_surface_param_eval_poly (curve, par, p2, p1);

	qi_curve_param_optimize (curve);

	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, curve);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CUSPIDAL_QUARTIC);
	mqi_coef_neg (VECT(l,1), VECT(l,1));
	qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(l,1), VECT(l,0));
	qi_inter_component_add_cut_parameter (comp, cut);
	qi_inter_cut_set_owner(cut, comp);

	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_vect_list_clear(sing, dir, ignored, rat_point, vect, l, m, NULL);
	mqi_curve_param_list_clear(line_par, curve, NULL);
	mqi_surface_param_list_clear(par, tmpsp, NULL);
	mqi_mat_list_clear(p_trans, q_tmp, NULL);
	mqi_hhpoly_clear(res);
	mqi_poly_list_clear(p1, p2, NULL);

}

/** Two tangent conics */
void __qi_inter_one_mult_two_tangent_conics (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					     mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
                                             int img_plane)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	qi_inter_init(rop);

	if ( img_plane == 1 )
		qi_inter_set_type (rop, 5,2);
	else
		qi_inter_set_type (rop, 5,1);
	
	mqi_curve_param_t sing_line, par_tmp;
	mqi_hpoly_t tmp;

	mqi_curve_param_init_dynamic (sing_line, mqi_mat_nrows(q_sing), mqi_mat_typeof(q_sing));
	mqi_curve_param_line_through_two_points (sing_line,
			(mqi_vect_ptr)COL(q_sing,0), (mqi_vect_ptr)COL(q_sing,1));

	mqi_curve_param_init_dynamic (par_tmp, 4, mqi_mat_typeof(q_other));
	mqi_curve_param_mul_mat	     (par_tmp, q_other, sing_line);

	mqi_hpoly_init_dynamic (tmp, mqi_curve_param_typeof(par_tmp));
	mqi_curve_param_scalar_product (tmp, sing_line, par_tmp);

	mqi_vect_t db_point;
	mqi_vect_init_dynamic (db_point, mqi_curve_param_n_equations(sing_line), mqi_curve_param_typeof(sing_line));

	if ( mqi_coef_sign (POLY(tmp,2)) != 0 ) {
		mqi_coef_neg (POLY(tmp,1), POLY(tmp,1));
		mqi_coef_mul_schar (POLY(tmp,2), POLY(tmp,2), 2);
		mqi_curve_param_eval (db_point, sing_line, POLY(tmp,1), POLY(tmp,2));
	}else{
		mqi_coef_neg (POLY(tmp,1), POLY(tmp,1));
		mqi_coef_mul_schar (POLY(tmp,0), POLY(tmp,0), 2);
		mqi_curve_param_eval (db_point, sing_line, POLY(tmp,0), POLY(tmp,1));
	}
	
	qi_vect_optimize (db_point);

	/** The intersection is reduced to the double point */
	if ( img_plane == 2 ) {
		mqi_curve_param_t sing_par;
		mqi_curve_param_init_dynamic (sing_par, mqi_vect_size(db_point), mqi_vect_typeof(db_point));
		mqi_curve_param_set_vect     (sing_par, db_point);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, sing_par);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

		qi_inter_add_component (rop, comp);

		/** Free local memory */
		mqi_curve_param_clear(sing_par);

	}else{
	
		/** First, parametrize the cone */
		mqi_mat_t sing;
		qi_mat_singular (sing, q_other);

		mqi_vect_t cone_sing;
		mqi_vect_init_cpy(cone_sing, (mqi_vect_ptr)COL(sing,0));

		mqi_vect_t rat_point;
		mqi_vect_init_cpy (rat_point, db_point);

		/** Try to find a point of small height on the line joining the singular
		 *  point of the cone to db_point and take that s rational point to parametrize
		 *  the cone. */

		/** The line param */
		mqi_curve_param_t line_par;
		mqi_curve_param_init_dynamic (line_par, mqi_vect_size(cone_sing), mqi_vect_typeof(cone_sing));
		mqi_curve_param_line_through_two_points (line_par, cone_sing, db_point);

		/** $$ TODO: cut parameters */
		mqi_vect_t ignored;
		qi_param_line_improved1(ignored, line_par);

		mqi_vect_t vect;

		mqi_vect_init_dynamic(vect, mqi_curve_param_n_equations(line_par),
					    mqi_curve_param_typeof(line_par));
		
		mqi_curve_param_eval_int (vect, line_par, 1,0);

		if ( mqi_vect_equals(cone_sing, vect) )
			mqi_curve_param_eval_int(rat_point, line_par, 0,1);
		else
		{
			mqi_vect_clear(rat_point);
			mqi_vect_init_cpy(rat_point, vect);
		}


		mqi_surface_param_t par;
		mqi_mat_t p_trans;
		mqi_vect_t l;

		mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q_other));
		qi_param_cone_through_ratpoint (q_other, cone_sing, rat_point, p_trans, par, l);

		/** The solution (-l[1],l[0]) corresponds to the line joining the singular point
		 *  of the cone to rat_point. Let's rescale the param so that this line (and thus the
		 *  point of tangency of the two conics) corresponds to the parameter (1,0). */
		mqi_vect_t m;
		mqi_vect_init_dynamic (m, 2, mqi_vect_typeof(l));

		if ( mqi_coef_sign(VECT(l,1)) == 0 ){
			mqi_vect_set_at_int(m,0, 0);
			mqi_vect_set_at_int(m,1, 1);
		}else{
			mqi_vect_set_at_int(m,0, 1);
			mqi_vect_set_at_int(m,1, 0);
		}

		/** Now rewrite the par accordingly, i.e. replace l[0]*s+l[1]*t by t and
		 *  m[0]*s+m[1]*t by s */
		mqi_mat_t par_trans;
		mqi_mat_init_dynamic (par_trans, 4, 4, mqi_vect_typeof(l));
		
		mqi_coef_mul(MAT(par_trans,0,0), VECT(l,1), VECT(l,1));
		mqi_coef_mul(MAT(par_trans,0,1), VECT(m,1), VECT(m,1));
		mqi_coef_linexpr2(MAT(par_trans,0,2),1,	-2,VECT(m,1),VECT(l,1));
		mqi_coef_mul(MAT(par_trans,1,0), VECT(l,0), VECT(l,0));
		mqi_coef_mul(MAT(par_trans,1,1), VECT(m,0), VECT(m,0));
		mqi_coef_linexpr2(MAT(par_trans,1,2),1,	-2,VECT(m,0),VECT(l,0));
		mqi_coef_linexpr2(MAT(par_trans,2,0),1,	-1,VECT(l,0),VECT(l,1));
		mqi_coef_linexpr2(MAT(par_trans,2,1),1,	-1,VECT(m,0),VECT(m,1));
		mqi_coef_linexpr2(MAT(par_trans,2,2),2,	1,VECT(l,0),VECT(m,1),	1,VECT(l,1),VECT(m,0));
		mqi_mat_set_at_int (par_trans,3,3, 1);

		/** Stack the transformations */
		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, p_trans);
		mqi_mat_clear(p_trans);
		mqi_mat_mul_init (p_trans, tmpmat, par_trans);

		qi_mat_optimize_3_update (p_trans, l);

		mqi_vect_set_at_int(l,0, 0);
		mqi_vect_set_at_int(l,1, -1);

		/** Parametrize the pair of planes */
		mqi_coef_t D;
		mqi_mat_t m1, m2;
		mqi_mat_init_dynamic (m1,4,3, mqi_vect_typeof(l));
		mqi_mat_init_dynamic (m2,4,1, mqi_mat_typeof(m1));

		qi_param_plane_pair (q, q_sing, D, m1, m2);

		mqi_curve_param_t conic2d;
		mqi_curve_param_init_dynamic (conic2d, 3, mqi_mat_typeof(m1));

		mqi_poly_set_x2 (CP(conic2d,0));
		mqi_poly_set_wi (CP(conic2d,1), 2);
		mqi_poly_set_xw (CP(conic2d,2));

		if ( mqi_coef_sign(D) == 0 ) {
			
			/** The two planes are rational so we can directly replace the first column
			 *  of m1 by m1(0) +/- m2(0) */
			mqi_mat_t m1p, m1m;
			mqi_mat_init_cpy (m1p, m1);
			mqi_mat_init_cpy (m1m, m1);

			mqi_mat_add(COL(m1p,0), COL(m1,0), COL(m2,0));
			mqi_mat_sub(COL(m1m,0), COL(m1,0), COL(m2,0));

			qi_mat_optimize (m1p);
			qi_mat_optimize (m1m);

			mqi_mat_t v1p, n1p, trans, ker;

			mqi_mat_transpose_init(trans, m1p);
			mqi_mat_kernel(ker, trans);
			mqi_mat_clear(trans);
			mqi_mat_transpose_init(trans, ker);
			mqi_mat_mul_init(v1p, trans, p_trans);

			mqi_mat_init_dynamic (n1p, 4, 3, mqi_mat_typeof(v1p));

			mqi_coef_neg (MAT(n1p,0,0), MAT(v1p,0,3));
			mqi_coef_neg (MAT(n1p,1,1), MAT(v1p,0,3));
			mqi_coef_neg (MAT(n1p,2,2), MAT(v1p,0,3));
			mqi_mat_set_at (n1p,3,0, MAT(v1p,0,0));
			mqi_mat_set_at (n1p,3,1, MAT(v1p,0,1));
			mqi_mat_set_at (n1p,3,2, MAT(v1p,0,2));


			mqi_mat_t v1m, n1m;
			mqi_mat_list_clear(trans, ker, NULL);
			mqi_mat_transpose_init(trans, m1m);
			mqi_mat_kernel(ker, trans);
			mqi_mat_clear(trans);
			mqi_mat_transpose_init(trans, ker);
			mqi_mat_mul_init(v1m, trans, p_trans);

			mqi_mat_init_dynamic (n1m, 4, 3, mqi_mat_typeof(v1m));

			mqi_coef_neg (MAT(n1m,0,0), MAT(v1m,0,3));
			mqi_coef_neg (MAT(n1m,1,1), MAT(v1m,0,3));
			mqi_coef_neg (MAT(n1m,2,2), MAT(v1m,0,3));
			mqi_mat_set_at (n1m,3,0, MAT(v1m,0,0));
			mqi_mat_set_at (n1m,3,1, MAT(v1m,0,1));
			mqi_mat_set_at (n1m,3,2, MAT(v1m,0,2));


			mqi_mat_t p_trans1, p_trans2;
			mqi_mat_mul_init (p_trans1, p_trans, n1p);
			mqi_mat_mul_init (p_trans2, p_trans, n1m);

			qi_mat_optimize_3 (p_trans1);
			qi_mat_optimize_3 (p_trans2);

			mqi_curve_param_t conic1, conic2;
			mqi_curve_param_initpool_dynamic (conic1, 4, mqi_mat_typeof(p_trans));
			mqi_curve_param_initpool_dynamic (conic2, 4, mqi_mat_typeof(p_trans));

			mqi_curve_param_mul_mat (conic1, p_trans1, conic2d);
			mqi_curve_param_mul_mat (conic2, p_trans2, conic2d);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic1);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic2);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);


			/** Free local memory */
			mqi_mat_list_clear(m1p, v1p, n1p, m1m, v1m, n1m, p_trans1, p_trans2, trans, ker, NULL);
			mqi_curve_param_list_clear(conic1, conic2, NULL);

		}else{

			/** The two planes are not rational */
			mqi_mat_t tmpmat;
			
			mqi_mat_init_cpy(tmpmat, m2);
			mqi_mat_clear(m2);
			mqi_mat_resize (m2, tmpmat, 4, 3);

			mqi_mat_t big_mat;
			mqi_mat_t k1, k2, k3, k4;

			mqi_mat_transpose_init(k1, m1);
			mqi_mat_transpose_init(k2, m2);
			mqi_mat_init_cpy(k3, k2);
			mqi_mat_mul_coef(k2, k2, D);
		
			mqi_mat_set_submat (big_mat, k1, 0, 0);
			mqi_mat_set_submat (big_mat, k2, 0, mqi_mat_ncols(k1));
			mqi_mat_set_submat (big_mat, k3, mqi_mat_nrows(k1), 0);
			mqi_mat_set_submat (big_mat, k1, mqi_mat_nrows(k1), mqi_mat_ncols(k1));	
			
			mqi_mat_t sing;
			qi_mat_singular (sing, big_mat);

			/** The kernel of trans(m1 +/- sqrt(D)*m2) is generated by k1 +/- sqrt(D)*k2.
			 *  k3 and k4 are tmp variables */
			mqi_mat_list_clear(k1, k2, k3, NULL);

			mqi_mat_init_cpy (k1, COL(sing,0));
			mqi_mat_init_cpy (k3, COL(sing,1));
			mqi_mat_init_cpy (k2, COL(sing,2));
			mqi_mat_init_cpy (k4, COL(sing,3));

			mqi_mat_t trans;
			mqi_mat_t v_rat, v_sq;

			mqi_mat_transpose_init(trans, k1);
			mqi_mat_mul_init(v_rat, trans, p_trans);

			mqi_mat_clear(trans);
			mqi_mat_transpose_init(trans, k2);
			mqi_mat_mul_init(v_sq, trans, p_trans);

			mqi_mat_t n_rat, n_sq;
			mqi_mat_list_init_dynamic (4,3, mqi_mat_typeof(v_sq), n_rat, n_sq, NULL);

			mqi_coef_neg (MAT(n_rat,0,0), MAT(v_rat,0,3));
			mqi_coef_neg (MAT(n_rat,1,1), MAT(v_rat,0,3));
			mqi_coef_neg (MAT(n_rat,2,2), MAT(v_rat,0,3));
			mqi_coef_cpy (MAT(n_rat,3,0), MAT(v_rat,0,0));
			mqi_coef_cpy (MAT(n_rat,3,1), MAT(v_rat,0,1));
			mqi_coef_cpy (MAT(n_rat,3,2), MAT(v_rat,0,2));

			mqi_coef_neg (MAT(n_sq,0,0), MAT(v_sq,0,3));
			mqi_coef_neg (MAT(n_sq,1,1), MAT(v_sq,0,3));
			mqi_coef_neg (MAT(n_sq,2,2), MAT(v_sq,0,3));
			mqi_coef_cpy (MAT(n_sq,3,0), MAT(v_sq,0,0));
			mqi_coef_cpy (MAT(n_sq,3,1), MAT(v_sq,0,1));
			mqi_coef_cpy (MAT(n_sq,3,2), MAT(v_sq,0,2));

			mqi_mat_t p_trans_rat, p_trans_sq;

			mqi_mat_mul_init (p_trans_rat, p_trans, n_rat);
			mqi_mat_mul_init (p_trans_sq,  p_trans, n_sq);

			qi_mat_optimize_4 (p_trans_rat, p_trans_sq);

			mqi_curve_param_t conic_rat, conic_sq;
			mqi_curve_param_init_dynamic (conic_rat, 4, mqi_mat_typeof(p_trans));
			mqi_curve_param_init_dynamic (conic_sq, 4, mqi_mat_typeof(p_trans));

			mqi_curve_param_mul_mat (conic_rat, p_trans_rat, conic2d);
			mqi_curve_param_mul_mat (conic_sq,  p_trans_sq,  conic2d);

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_CONIC, QI_INTER_COMPONENT_3, conic_rat, conic_sq, D);

			
			/** Free local memory */
			mqi_mat_list_clear(big_mat, tmpmat, k1, k2, k3, k4, sing, trans,
					   v_rat, v_sq, n_rat, n_sq, p_trans_rat, p_trans_sq, NULL);
			mqi_curve_param_list_clear(conic_rat, conic_sq, NULL);

		}

		mqi_coef_neg (VECT(l,1), VECT(l,1));
		qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(l,1), VECT(l,0));

		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);

		qi_inter_cut_set_owner(cut, rop->components[0]);

		/** Free local memory */
		mqi_mat_list_clear(sing, p_trans, par_trans, tmpmat, m1, m2, NULL);
		mqi_vect_list_clear(cone_sing, rat_point, ignored, vect, l, m, NULL);
		mqi_curve_param_list_clear(line_par, conic2d, NULL);
		mqi_surface_param_clear(par);
		mqi_coef_clear(D);

	}

	qi_inter_set_optimal (rop);


	/** Free local memory */
	mqi_curve_param_list_clear (sing_line, par_tmp, NULL);
	mqi_poly_clear (tmp);
	mqi_vect_clear (db_point);

}

/** Nodal quartic */
void __qi_inter_one_mult_nodal_quartic (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
				        mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing,
                                        mqi_coef_t __IN delta_e, int __IN s_e, int __IN in_q)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;
	int isolated_sing;

	qi_inter_init(rop);

	/* no need to discriminate the two cases anymore, because the components
	 * are now added sequentially
	 */
	/*if ( mqi_coef_sign(delta_e) > 0 && (s_e == -1) && (in_q == 2) )
	  {
	  mqi_inter_init(rop);
	  }else{
	  mqi_inter_init(rop);
	  }*/

	/** The node of the quartic = singular point of cone */
	mqi_vect_t sing;
	mqi_vect_init_cpy (sing, (mqi_vect_ptr)COL(q_sing,0));

	qi_vect_optimize (sing);

	/** Other roots of determinental equation are complex */
	if ( mqi_coef_sign(delta_e) < 0 )
		qi_inter_set_type (rop, 2,4);
	else {
		/** Other roots of determinental equation are real */
		if ( s_e == 1 )
			qi_inter_set_type (rop, 2,3);
		else {
			if ( in_q == 2 ) /** The cone corresponding to the double real root is real */
				qi_inter_set_type (rop, 2,2);
			else		 /** The cone is complex */
				qi_inter_set_type (rop, 2,1);
		}
	}

	if ( mqi_coef_sign(delta_e) > 0 && (s_e == -1) && (in_q == 3) )
	{
		mqi_curve_param_t cone_p;
		mqi_curve_param_init_dynamic (cone_p, mqi_vect_size(sing), mqi_vect_typeof(sing));
		mqi_curve_param_set_vect     (cone_p, sing);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cone_p);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

		qi_inter_set_optimal (rop);

		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		/** Free local memory */
		mqi_curve_param_clear(cone_p);

	}
	else
	{
		/** isolated_sing is true if the singularity of the nodal quartic is isolated. */
		if ( (in_q == 2) && (s_e == -1) && (mqi_coef_sign(delta_e) > 0) )
			isolated_sing = 1;
		else
			isolated_sing = 0;

		/** Parametrizes the projective cone q */
		mqi_mat_t p_trans, p_trans2;
		mqi_coef_t D;
		mqi_surface_param_t par;
		mqi_vect_t vect;

		mqi_vect_init_cpy(vect, (mqi_vect_ptr)COL(q_sing,0));
		qi_param_cone (q, sing, D, p_trans, p_trans2, par);

		if ( mqi_coef_sign(D) == 0 ) {

			/** We have found a rational parametrization of the cone */

			qi_mat_optimize_3 (p_trans);

			/** The quadric in the canonical frame of the cone */
			mqi_mat_t q_tmp;
			mqi_mat_transformation (q_tmp, q_other, p_trans, MQI_MAT_NOTRANSCO);

			/** Plug in the other quadric */
			mqi_hhpoly_t res;
			qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

			/** The parameter values corresponding to the node, if applicable */
			mqi_vect_t l, m;
			mqi_vect_list_init_dynamic (2, mqi_mat_typeof(q_tmp), l, m, NULL);

			mqi_coef_t sq;

			mqi_coef_init_dynamic(sq, mqi_hhpoly_typeof(res));

			mqi_coef_linexpr2(sq,2,	1,HHCOEF(res,1,1),HHCOEF(res,1,1),  -4,HHCOEF(res,1,0),HHCOEF(res,1,2));

			if ( mqi_coef_is_square(sq) )
				mqi_coef_sqrt(sq, sq);
			else
				mqi_coef_set_zero(sq);

			if ( !isolated_sing && (mqi_coef_sign(sq) > 0) )
			{

				/** res has the form:
				 *  u*(l[0]*s+l[1]*t)*(m[0]*s+m[1]*t) + v*p1(s,t)
				 *
				 *  where p1(s,t) has degree 4.
				 *
				 *  The two solutions (-l[1],l[0]) and (-m[1],m[0]) correspond to the two
				 *  parameters of the node of the quartic */

				mqi_hpoly_t p2;
				mqi_poly_init_cpy (p2, HHPOLY(res,1));

				if ( mqi_coef_sign(POLY(p2,2)) == 0 ) {
					mqi_vect_set_at_int(l,1,  1);
					mqi_vect_set_at(m,0,  POLY(p2,1));
					mqi_vect_set_at(m,1,  POLY(p2,0));
				}
				else{
					mqi_coef_linexpr1 (VECT(l,0), 1,		2, POLY(p2,2));
					mqi_coef_linexpr1 (VECT(m,0), 1,		2, POLY(p2,2));
					mqi_coef_linexpr1 (VECT(l,1), 2,		1, POLY(p2,1), -1,sq);
					mqi_coef_linexpr1 (VECT(m,1), 2,		1, POLY(p2,1), 1,sq);
				}

				qi_vect_optimize (l);
				qi_vect_optimize (m);

				/** Rescale the parametrization so that the node corresponds to the parameters (1,0) and (0,1) */

				/** Rewrite the parametrization accordingly, i.e. replace l[0]*s+l[1]*t by t and
				 *  m[0]*s+m[1]*t by s */

				/** The "SPCOEF" macro has been introduced as new tool to easily access a particular
				 *  coefficient within a surface param. */
				mqi_coef_linexpr2 (SPCOEF(par,0,1,2), 1,		1, VECT(l,1), VECT(l,1));
				mqi_coef_linexpr2 (SPCOEF(par,0,1,1), 1,		-2, VECT(m,1), VECT(l,1));
				mqi_coef_linexpr2 (SPCOEF(par,0,1,0), 1,		1, VECT(m,1), VECT(m,1));
				mqi_coef_linexpr2 (SPCOEF(par,1,1,2), 1,		1, VECT(l,0), VECT(l,0));
				mqi_coef_linexpr2 (SPCOEF(par,1,1,1), 1,		-2, VECT(m,0), VECT(l,0));
				mqi_coef_linexpr2 (SPCOEF(par,1,1,0), 1,		1, VECT(m,0), VECT(m,0));
				mqi_coef_linexpr2 (SPCOEF(par,2,1,2), 1,		-1, VECT(l,0), VECT(l,1));
				mqi_coef_linexpr2 (SPCOEF(par,2,1,1), 2,		1, VECT(l,0), VECT(m,1), 1, VECT(l,1), VECT(m,0));
				mqi_coef_linexpr2 (SPCOEF(par,2,1,0), 1,		-1, VECT(m,0), VECT(m,1));

				/** And do the plug again */
				mqi_hhpoly_clear(res);
				qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

				mqi_vect_set_at_int (l,0,  0);
				mqi_vect_set_at_int (l,1,  -1);
				mqi_vect_set_at_int (m,0,  1);
				mqi_vect_set_at_int (m,1,  0);

				/** Free local memory */
				mqi_poly_clear(p2);

			}

			/** We first extract p1 and p2 */
			/** HHPOLY is a new tool for accessing a particular homogeneous polynomial */
			mqi_poly_t p1, p2;
			mqi_poly_init_cpy (p1, HHPOLY(res,2));
			mqi_poly_init_cpy (p2, HHPOLY(res,1));
			mqi_poly_neg (p2, p2);

			/** Expand param */
			mqi_surface_param_t tmpsp;

			mqi_surface_param_init_cpy(tmpsp, par);
			mqi_surface_param_mul_mat (par, p_trans, tmpsp);

			/** Compute the result */
			mqi_curve_param_t curve;
			mqi_curve_param_init_dynamic (curve, 4, mqi_mat_typeof(p_trans));
			mqi_surface_param_eval_poly (curve, par, p2, p1);

			qi_curve_param_optimize (curve);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, curve);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_NODAL_QUARTIC);
			qi_inter_component_set_optimal (comp);

			/** $$ BAD CUT PARAMETERS */
			if ( !isolated_sing && (mqi_coef_sign(sq) > 0) ) {
				mqi_coef_neg (VECT(l,1), VECT(l,1));
				mqi_coef_neg (VECT(m,1), VECT(m,1));
				qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(l,1), VECT(l,0));
				qi_inter_component_add_cut_parameter (comp, cut);
				qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(m,1), VECT(m,0));
				qi_inter_component_add_cut_parameter (comp, cut);
				qi_inter_cut_set_owner(cut, comp);
			}

			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_mat_clear(q_tmp);
			mqi_hhpoly_clear(res);
			mqi_vect_list_clear(l, m, NULL);
			mqi_coef_clear(sq);
			mqi_poly_list_clear(p1, p2, NULL);
			mqi_surface_param_clear(tmpsp);
			mqi_curve_param_clear(curve);

		}
		else
		{

			/** Expand the params */
			mqi_surface_param_t par2;
			mqi_surface_param_init_dynamic (par2, 4, mqi_mat_typeof(p_trans2));

			mqi_surface_param_mul_mat (par2, p_trans2, par2);

			mqi_surface_param_t tmpsp;

			mqi_surface_param_init_cpy(tmpsp, par);
			mqi_surface_param_mul_mat (par, p_trans, tmpsp);

			/** Plug in the other quadric
			 *  res is  par*q_other*par + D*par2*q_other*par2
			 *  res2 is 2*par2*q_other*par */
			mqi_hhpoly_t res, res2, tmp;
			qi_param_plug_sp_in_quadric (res, par, q_other, par);
			qi_param_plug_sp_in_quadric (tmp, par2, q_other, par2);

			mqi_hhpoly_mul_coef (tmp, tmp, D);
			mqi_hhpoly_add	    (res, res, tmp);

			qi_param_plug_sp_in_quadric (res2, par2, q_other, par);
			mqi_hhpoly_mul_int (res2, res2, 2);

			/** Now res and res2 are of the form u^2*p1(s,t)+u*v*p2(s,t), where p1
			 *  has degree 4 and p2 has degree 2. Also, par and par2 are vector of
			 *  components of the form p3(s,t)*u+p4(s,t)*v, where p3 has degree 2 and
			 *  p4 has degree 0. */

			/** We first extract p1 and p2 */
			mqi_poly_t p1, p12, p2, p22;
			mqi_poly_init_cpy (p1, HHPOLY(res,2));
			mqi_poly_init_cpy (p2, HHPOLY(res,1));
			mqi_poly_neg	   (p2, p2);
			mqi_poly_init_cpy (p12, HHPOLY(res2,2));
			mqi_poly_init_cpy (p22, HHPOLY(res2,1));
			mqi_poly_neg	   (p22, p22);

			/** Then, we extract the components p3 and p4 of par and put them in
			 *  curve_param's.
			 *  The result is:  curve + sqrt(D)*curve2 */
			mqi_curve_param_t curve, curve2, cu, tmpcp;
			mqi_curve_param_init_dynamic (curve, 4, mqi_poly_typeof(p22));
			mqi_curve_param_init_dynamic (curve2, 4, mqi_poly_typeof(p22));
			mqi_curve_param_init_dynamic (cu, 4, mqi_poly_typeof(p22));
			mqi_curve_param_init_dynamic (tmpcp, 4, mqi_poly_typeof(p22));

			mqi_surface_param_eval_poly (cu, par2, p22, p12);
			mqi_curve_param_mul_coef (cu, cu, D);
			mqi_surface_param_eval_poly (curve, par, p2, p1);
			mqi_curve_param_add (curve, curve, cu);

			mqi_surface_param_eval_poly (tmpcp, par, p22, p12);
			mqi_surface_param_eval_poly (curve2, par2, p2, p1);
			mqi_curve_param_add (curve2, curve2, tmpcp);

			qi_curve_param_optimize_gcd (curve, curve2);


			qi_inter_component_init (comp, QI_INTER_COMPONENT_3, curve, curve2, D);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_NODAL_QUARTIC);

			if ( qi_settings.optimize )
				qi_inter_component_set_optimal (comp);

			qi_inter_add_component (rop, comp);

			/** The polynomial p2+sqrt(D)*p22 factors in two linear factors corresponding to the parameters
			 *  at which the node of the quartic is reached. */
			if ( ! isolated_sing )
			{

				mqi_coef_t d1, d2, sq, dp, dm, dsq;

				mqi_coef_list_init_dynamic(mqi_coef_typeof(D), d1, d2, dp, dm, dsq, NULL);

				mqi_coef_linexpr3 (d1, 4,	1,POLY(p2,1),POLY(p2,1),NULL,  1,D,POLY(p22,1),POLY(p22,1),  -4,POLY(p2,2),POLY(p2,0),NULL,
						-4,D,POLY(p22,2),POLY(p22,0) );
				mqi_coef_linexpr2 (d2, 3,	2,POLY(p2,1),POLY(p22,1),  -4,POLY(p2,2),POLY(p22,0),  -4,POLY(p2,0),POLY(p22,2) );
				mqi_coef_linexpr3 (sq, 2, 	1,d1,d1,NULL,  -1,D,d2,d2);

				/** $$ What if it's *not* a square ?? LiDIA's documentation says nothing about it ! */
				if ( mqi_coef_is_square(sq) ) {
					mqi_coef_sqrt (sq, sq);
				}

				mqi_coef_linexpr1 (dp,2,	2,d1,	2,sq);
				mqi_coef_linexpr1 (dm,2,	2,d1,	-2,sq);

				/** Things might simplify a bit */
				int dp_square = 0, dm_square = 0;
				if ( mqi_coef_is_square(dp) )
				{
					mqi_coef_sqrt (dsq, dp);
					dp_square = 1;
				}
				else if ( mqi_coef_is_square(dm) )
				{
					mqi_coef_sqrt (dsq, dm);
					dm_square = 1;
				}

				if ( dp_square || dm_square )
				{

					mqi_vect_t p1_rat, p2_rat, p1_D, p2_D;

					mqi_vect_list_init_dynamic(2, mqi_coef_typeof(D),
							p1_rat, p2_rat, p1_D, p2_D, NULL);

					if ( !mqi_coef_sign(POLY(p2,2)) || !mqi_coef_sign(POLY(p22,2)) )
					{
						mqi_coef_linexpr3(VECT(p1_rat,0),2,
								1,D,POLY(p22,1),POLY(p22,0),
								-1,NULL,POLY(p2,1),POLY(p2,0));
						mqi_coef_linexpr3(VECT(p1_rat,1),2,
								1,NULL,POLY(p2,1),POLY(p2,1),
								-1,D,POLY(p22,1),POLY(p22,1));
						mqi_coef_linexpr2(VECT(p1_D,0),2,
								1,POLY(p22,1),POLY(p2,0),
								-1,POLY(p22,0),POLY(p2,1));

						qi_vect_optimize_gcd(p1_rat, p1_D);

						qi_inter_cut_init(cut, QI_INTER_CUT_1, VECT(p1_rat,0),VECT(p1_D,0),D,VECT(p1_rat,1));
						qi_inter_component_add_cut_parameter(rop->components[0], cut);
						qi_inter_cut_set_owner(cut, rop->components[0]);

						mqi_coef_t coef, coef2;
						mqi_coef_list_init_dynamic(mqi_coef_typeof(D), coef, coef2, NULL);

						mqi_coef_set_zero(coef);
						mqi_coef_set_schar(coef2, 1);

						qi_inter_cut_init(cut, QI_INTER_CUT_1, coef2, coef, D, coef);
						qi_inter_component_add_cut_parameter(rop->components[0], cut);
						qi_inter_cut_set_owner(cut, rop->components[0]);

						/** Free local memory */
						mqi_coef_list_clear(coef, coef2, NULL);
					}
					else
					{
						mqi_coef_t tmp1, tmp2, tmp3, tmp4, tmp5;

						mqi_coef_list_init_dynamic(mqi_coef_typeof(D), tmp1, tmp2, tmp3, tmp4, tmp5, NULL);

						mqi_coef_linexpr2(tmp1,2, -2,dsq,POLY(p2,1),	1,dsq,dsq);
						mqi_coef_linexpr2(tmp2,2, -2,dsq,POLY(p22,1),	2,d2,NULL);
						mqi_coef_linexpr2(tmp3,2, -2,dsq,POLY(p2,1),	-1,dsq,dsq);
						mqi_coef_linexpr2(tmp4,2, -2,dsq,POLY(p22,1),	-2,d2,NULL);
						mqi_coef_linexpr2(tmp5,1, 1,D,POLY(p22,2));

						mqi_coef_linexpr2(VECT(p1_rat,0),2,  1,POLY(p2,2),tmp1,	 -1,tmp5,tmp2);
						mqi_coef_linexpr3(VECT(p1_rat,1),2,  4,dsq,POLY(p2,2),POLY(p2,2),
								-4,dsq,tmp5,POLY(p22,2));
						mqi_coef_linexpr2(VECT(p2_rat,0),2,  1,POLY(p2,2),tmp3,	-1,tmp5,tmp4);
						mqi_vect_set_at(p2_rat,1,  VECT(p1_rat,1));

						mqi_coef_linexpr2(VECT(p1_D,0),2,  -1,POLY(p22,2),tmp1,	1,POLY(p2,2),tmp2);
						mqi_coef_linexpr2(VECT(p2_D,0),2,  -1,POLY(p22,2),tmp3, 1,POLY(p2,2),tmp4);

						qi_vect_optimize_gcd (p1_rat, p1_D);
						qi_vect_optimize_gcd (p2_rat, p2_D);

						qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(p1_rat,0),VECT(p1_D,0),D,VECT(p1_rat,1));
						qi_inter_component_add_cut_parameter (rop->components[0], cut);
						qi_inter_cut_set_owner (cut, rop->components[0]);

						qi_inter_cut_init (cut, QI_INTER_CUT_1, VECT(p2_rat,0),VECT(p2_D,0),D,VECT(p2_rat,1));
						qi_inter_component_add_cut_parameter (rop->components[0], cut);
						qi_inter_cut_set_owner (cut, rop->components[0]);


						/** Free local memory */
						mqi_coef_list_clear (tmp1, tmp2, tmp3, tmp4, tmp5, NULL);
					}

					/** Free local memory */
					mqi_vect_list_clear(p1_rat, p2_rat, p1_D, p2_D, NULL);

				}
				else
				{

					/** No simplification in factors: general situation */
					mqi_coef_t ep, em, dp_old, dm_old;

					mqi_coef_list_init_dynamic(mqi_coef_typeof(dp), ep, em, dp_old, dm_old, NULL);		
					mqi_coef_cpy     (dm_old, dm);
					mqi_coef_cpy     (dp_old, dp);

					mqi_coef_set_schar (ep, 1);
					mqi_coef_set_schar (em, 1);

					/** $$ Triple check the order in square extraction parameters: square, cofactor **/
					mqi_coef_t coef;

					mqi_coef_init_cpy(coef, dp);
					qi_coef_extract_square_factors (ep, dp, coef);

					mqi_coef_cpy(coef, dm);
					qi_coef_extract_square_factors (em, dm, coef);

					mqi_coef_t tmp1, tmp2, tmp3, tmp4, tmp5;

					mqi_coef_linexpr4(tmp1,2, 4,d2,POLY(p22,2),POLY(p22,1),D,  -4,d2,POLY(p2,2),POLY(p2,1),NULL);
					mqi_coef_linexpr4(tmp2,2, 8,d2,POLY(p2,2),POLY(p2,2),NULL, -8,d2,D,POLY(p22,2),POLY(p22,2));
					mqi_coef_linexpr3(tmp3,2, 2,ep,POLY(p2,2),d2,  -1,ep,POLY(p22,2),dm_old);
					mqi_coef_linexpr3(tmp4,2, 2,em,POLY(p2,2),d2,  -1,em,POLY(p22,2),dp_old);
					mqi_coef_linexpr2(tmp5,2, 2,POLY(p22,2),POLY(p2,1), -2,POLY(p22,1),POLY(p2,2));

					if ( mqi_coef_cmp (D,dp) > 0 && mqi_coef_cmp (D,dm) > 0 ) {

						mqi_vect_t p_rat, p_dp, p_dm, p_dpdm;

						/** dp and dm are the smallest */
						mqi_vect_list_init_dynamic(2, mqi_coef_typeof(tmp1),
								p_rat, p_dp, p_dm, p_dpdm, NULL);

						mqi_coef_cpy (VECT(p_rat,0), tmp1);
						mqi_coef_cpy (VECT(p_rat,1), tmp2);

						mqi_coef_cpy (VECT(p_dp,0), tmp3); /** coef of sqrt(dp) */
						mqi_coef_cpy (VECT(p_dm,0), tmp4); /** coef of sqrt(dm) */
						mqi_coef_linexpr3 (VECT(p_dpdm,0),1, 1,ep,em,tmp5); /** coef of sqrt(dp*dm) */

						qi_vect_optimize_gcd_quadruplet (p_rat, p_dp, p_dm, p_dpdm);

						if ( mqi_coef_cmp(dp,dm) > 0 )
						{
							mqi_coef_swap(dp, dm);
							mqi_vect_swap(p_dp, p_dm);
						}

						mqi_coef_neg (tmp1, VECT(p_dp,0));
						mqi_coef_neg (tmp2, VECT(p_dm,0));

						qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_dp,0), dp,
								VECT(p_dm,0), dm, VECT(p_dpdm,0), VECT(p_rat,1));
						qi_inter_component_add_cut_parameter (rop->components[0], cut);
						qi_inter_cut_set_owner (cut, rop->components[0]);

						qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), tmp1, dp,
								tmp2, dm, VECT(p_dpdm,0), VECT(p_rat,1));
						qi_inter_component_add_cut_parameter (rop->components[0], cut);
						qi_inter_cut_set_owner (cut, rop->components[0]);

						/** Free local memory */
						mqi_vect_list_clear(p_rat, p_dp, p_dm, p_dpdm, NULL);

					}else if ( mqi_coef_cmp(dp,D) > 0 && mqi_coef_cmp(dp,dm) > 0 ) {

						/** dm and D are the smallest */
						mqi_vect_t p_rat, p_dm, p_D, p_dmD;

						mqi_vect_list_init_dynamic (2, mqi_coef_typeof(dp),
								p_rat, p_dm, p_D, p_dmD, NULL);

						mqi_coef_linexpr2 (VECT(p_rat,0), 1, 1,dm_old,tmp1);
						mqi_coef_linexpr2 (VECT(p_rat,1), 1, 1,dm_old,tmp2);

						mqi_coef_linexpr2 (VECT(p_dm,0), 1, 1,dm_old,tmp4);
						mqi_coef_linexpr3(VECT(p_D,0), 1, 2,d2,dm_old,tmp5);
						mqi_coef_linexpr2 (VECT(p_dmD,0), 1, 2,d2,tmp3);

						qi_vect_optimize_gcd_quadruplet (p_rat, p_dm, p_D, p_dmD);

						if ( mqi_coef_cmp(dm,D) > 0 ) {
							mqi_coef_neg (tmp1, VECT(p_dm,0));
							mqi_coef_neg (tmp2, VECT(p_dmD,0));

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_D,0), D, VECT(p_dm,0), dm, VECT(p_dmD,0), VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_D,0), D, tmp1, dm, tmp2, VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

						}else{
							mqi_coef_neg (tmp1, VECT(p_dm,0));
							mqi_coef_neg (tmp2, VECT(p_dmD,0));

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_dm,0), dm, VECT(p_D,0), D, VECT(p_dmD,0), VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), tmp1, dm, VECT(p_D,0), D, tmp2, VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);
						}

						/** Free local memory */
						mqi_vect_list_clear(p_rat, p_dm, p_D, p_dmD, NULL);

					}
					else
					{
						/** dp and D are the smallest */
						mqi_vect_t p_rat, p_dp, p_D, p_dpD;

						mqi_vect_list_init_dynamic(2, mqi_coef_typeof(dp),
								p_rat, p_dp, p_D, p_dpD, NULL);

						mqi_coef_linexpr2 (VECT(p_rat,0), 1, 1,dp_old,tmp1);
						mqi_coef_linexpr2 (VECT(p_rat,1), 1, 1,dp_old,tmp2);

						mqi_coef_linexpr2 (VECT(p_dp,0), 1, 1,dp_old,tmp3);
						mqi_coef_linexpr3(VECT(p_D,0), 1, 2,d2,dp_old,tmp5);
						mqi_coef_linexpr2 (VECT(p_dpD,0), 1, 2,d2,tmp4);

						qi_vect_optimize_gcd_quadruplet (p_rat, p_dp, p_D, p_dpD);

						if ( mqi_coef_cmp(dp,D) > 0 ) {
							mqi_coef_neg (tmp1, VECT(p_dp,0));
							mqi_coef_neg (tmp2, VECT(p_dpD,0));

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_D,0), D, VECT(p_dp,0), dp, VECT(p_dpD,0), VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_D,0), D, tmp1, dp, tmp2, VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

						}else{
							mqi_coef_neg (tmp1, VECT(p_dp,0));
							mqi_coef_neg (tmp2, VECT(p_dpD,0));

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), VECT(p_dp,0), dp, VECT(p_D,0), D, VECT(p_dpD,0), VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

							qi_inter_cut_init (cut, QI_INTER_CUT_2, VECT(p_rat,0), tmp1, dp, VECT(p_D,0), D, tmp2, VECT(p_rat,1));
							qi_inter_component_add_cut_parameter (rop->components[0], cut);
							qi_inter_cut_set_owner (cut, rop->components[0]);

						}

						/** Free local memory */
						mqi_vect_list_clear(p_rat, p_dp, p_D, p_dpD, NULL);

					}

					/** Free local memory */
					mqi_coef_list_clear(ep, em, dp_old, dm_old, coef, tmp1, tmp2, tmp3, tmp4, tmp5, NULL);

				}

				/** Free local memory */
				mqi_coef_list_clear(d1, d2, sq, dsq, dp, dm, NULL);

			}

			/** Free local memory */
			mqi_surface_param_list_clear(par2, tmpsp, NULL);
			mqi_hhpoly_list_clear(res, res2, tmp, NULL);
			mqi_poly_list_clear(p1, p2, p12, p22, NULL);
			mqi_curve_param_list_clear(curve, curve2, cu, NULL);

		}

		if ( (in_q == 2) && (s_e == -1) && (mqi_coef_sign(delta_e)) > 0 ) {
	
			/** Isolated singularity */
			mqi_curve_param_t cone_p;
			mqi_curve_param_init_dynamic (cone_p, mqi_vect_size(sing), mqi_vect_typeof(sing));
			mqi_curve_param_set_vect (cone_p, sing);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cone_p);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);

			qi_inter_component_set_optimal (comp);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_curve_param_clear(cone_p);

		}

		/** Free local memory */
		mqi_mat_list_clear(p_trans, p_trans2, NULL);
		mqi_coef_clear(D);
		mqi_surface_param_clear(par);
		mqi_vect_clear(vect);

	}

	/** Free local memory */
	mqi_vect_clear(sing);

}

/** Double conic */
void __qi_inter_one_mult_double_conic (qi_inter_t rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
				       mqi_mat_t __IN q, mqi_mat_t __IN q_sing, mqi_hpoly_t __IN det_e)
{

	qi_inter_component_t 	comp;

	/** First, compute the matrix associated to the other (simple) root of the determinential equation */
	/** The (lambda,mu) of the other (single) root of the pencil */
	mqi_vect_t root_cone;
	mqi_vect_init_dynamic (root_cone, 2, mqi_mat_typeof(q1));

	mqi_coef_neg (VECT(root_cone,0), POLY(det_e,0));
	mqi_coef_cpy (VECT(root_cone,1), POLY(det_e,1));

	/** Simplify the coordinates of root_cone */
	qi_vect_optimize (root_cone);

	/** The cone */
	mqi_mat_t q1_tmp, q2_tmp;
	mqi_mat_init_dynamic (q1_tmp, mqi_mat_nrows(q1), mqi_mat_ncols(q1), mqi_mat_typeof(q1));
	mqi_mat_init_dynamic (q2_tmp, mqi_mat_nrows(q2), mqi_mat_ncols(q2), mqi_mat_typeof(q2));

	mqi_mat_mul_coef (q1_tmp, q1, VECT(root_cone,0));
	mqi_mat_mul_coef (q2_tmp, q2, VECT(root_cone,1));

	mqi_mat_t cone;
	mqi_mat_init_cpy (cone, q1_tmp);
	mqi_mat_add	 (cone, cone, q2_tmp);

	mqi_vect_t inertia;
	qi_mat_inertia_known_rank (inertia, cone, 3);

	if ( mqi_coef_cmp_schar (VECT(inertia,0), 3) == 0 ) {
		/** Cone is imaginary */
		qi_inter_init(rop);
		qi_inter_set_type (rop, 6,1);
	}else{

		/** Cone is real: intersection is a double conic */

		/** One component */
		qi_inter_init(rop);
		qi_inter_set_type (rop, 6,2);

		/** First parametrize the double plane q */
		mqi_mat_t m1;
		mqi_mat_init_dynamic (m1, 4,3, mqi_mat_typeof(cone));

		qi_param_plane_double (q, q_sing, m1);

		qi_mat_optimize (m1);

		/** Now we have a 3x3 matrix representing a real non-singular conic */
		mqi_mat_t conic_2d;
		mqi_mat_transformation (conic_2d, cone, m1, MQI_MAT_NOTRANSCO);

		/** Parametrize the conic */
		mqi_mat_t p_trans, p_trans2;
		mqi_coef_t D;
		mqi_curve_param_t par;
		mqi_mat_initpool_dynamic (p_trans, 3, 3, mqi_mat_typeof(cone));
		mqi_mat_initpool_dynamic (p_trans2, 3, 3, mqi_mat_typeof(cone));
		mqi_curve_param_initpool_dynamic (par, 3, mqi_mat_typeof(cone));

		qi_param_conic (conic_2d, D, p_trans, p_trans2, par);

		if ( mqi_coef_sign(D) == 0 ) {

			/** We found a rational conic */

			/** Stacks the transformations */
			mqi_mat_t tmpmat;

			mqi_mat_init_cpy(tmpmat, p_trans);
			mqi_mat_clear(p_trans);
			mqi_mat_mul_init (p_trans, m1, tmpmat);

			qi_mat_optimize_3 (p_trans);

			mqi_curve_param_t conic;
			mqi_curve_param_init_dynamic (conic, 4, mqi_mat_typeof(p_trans));

			mqi_curve_param_mul_mat (conic, p_trans, par);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
			qi_inter_component_set_optimal (comp);
			qi_inter_add_component (rop, comp);

			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_mat_clear(tmpmat);
			mqi_curve_param_clear(conic);

		}else{

			/** Have to content oneself with one square root */

			/** Stacks the transformation */
			mqi_mat_t tmpmat;

			mqi_mat_init_cpy(tmpmat, p_trans);
			mqi_mat_clear(p_trans);
			mqi_mat_mul_init (p_trans, m1, tmpmat);

			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat, p_trans2);
			mqi_mat_clear(p_trans2);
			mqi_mat_mul_init (p_trans2, m1, tmpmat);

			qi_mat_optimize_4 (p_trans, p_trans2);

			mqi_curve_param_t conic, conic2;
			mqi_curve_param_init_dynamic (conic, 4, mqi_mat_typeof(p_trans));
			mqi_curve_param_init_dynamic (conic2, 4, mqi_mat_typeof(p_trans));

			mqi_curve_param_mul_mat (conic, p_trans, par);
			mqi_curve_param_mul_mat (conic2, p_trans2, par);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_3, conic, conic2, D);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);


			if ( qi_settings.optimize )
				qi_inter_component_set_optimal (comp);
			
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_mat_clear(tmpmat);
			mqi_curve_param_list_clear(conic, conic2, NULL);

		}

		qi_inter_component_set_multiplicity(rop->components[0], 2);
		
		/** Free local memory */
		mqi_mat_list_clear(m1, conic_2d, p_trans, p_trans2, NULL);
		mqi_curve_param_clear(par);
		mqi_coef_clear(D);

	}

	/** Free local memory */
	mqi_vect_list_clear(root_cone, inertia, NULL);
	mqi_mat_list_clear(q1_tmp, q2_tmp, cone, NULL);

}

/** Cubic and tangent line */
void __qi_inter_one_mult_cubic_tangent_line (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2,
					     mqi_mat_ptr __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	/** Always 2 components */
	qi_inter_init(rop);
	qi_inter_set_type (rop, 7,1);

	/** Singular point of cone */
	mqi_vect_t sing;
	mqi_vect_init_cpy (sing, (mqi_vect_ptr)COL(q_sing,0));

	qi_vect_optimize (sing);

	/** The line of the intersection is the intersection of the tangent plane
	 *  of q_other at sing with the cone */
	mqi_vect_t dir;
	mqi_mat_mul_init ((mqi_mat_ptr)dir, q_other, (mqi_mat_ptr)sing);

	/** Now a rational point on the line - We can't use a classical solve because we
	 *  want an answer in bigints, and classical solve might introduce rationals. */
	mqi_vect_t rat_point;
	qi_solve_proj (rat_point, q, dir, sing);

	/** The line param */
	mqi_curve_param_t line_par;
	mqi_curve_param_init_dynamic (line_par, mqi_vect_size(sing), mqi_vect_typeof(sing));

	mqi_curve_param_line_through_two_points (line_par, sing, rat_point);

	/** Cut parameter of the line */
	mqi_vect_t cutv;
	mqi_vect_init_dynamic (cutv, 2, mqi_vect_typeof(sing));

	/** Try to find a point of small height on the line by looking at the
	 *  parameter values such that one of the components is zero */
	qi_param_line_improved1 (cutv, line_par);
	qi_vect_optimize_by_half (cutv);

	mqi_curve_param_eval_int (rat_point, line_par, 1,0);

	if ( mqi_vect_equals(rat_point, sing) )
		mqi_curve_param_eval_int (rat_point, line_par, 0,1);


	/** Parametrize the cone */
	mqi_surface_param_t par;
	mqi_mat_t p_trans;
	mqi_vect_t l;

	mqi_vect_init_dynamic(l, 2, mqi_mat_typeof(q));
	qi_param_cone_through_ratpoint (q, sing, rat_point, p_trans, par, l);

	qi_mat_optimize_3_update (p_trans, l);

	/** The quadric in the canonical frame of the cone */
	mqi_mat_t q_tmp;
	mqi_mat_transformation (q_tmp, p_trans, q_other, MQI_MAT_NOTRANSCO);

	/** Plug in the other quadric */
	mqi_hhpoly_t res;
	qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

	/** The common factor of res (see below) */
	mqi_hpoly_t tmp;
	
	/** res has the form:
  	  *       ---> u^2*(l[0]*s+l[1]*t)*p1(s,t)+u*v*(l[0]*s+l[1]*t)^2
  	  * The solution (-l[1],l[0]) corresponds to the point of tangency of the
  	  * line with the cubic */
	mqi_vect_t m;
	mqi_vect_init_dynamic (m, 2, mqi_vect_typeof(sing));

	if ( mqi_coef_sign(VECT(l,0)) ) {
		mqi_coef_mul 		(VECT(m,0), VECT(l,0), HHCOEF(res,1,2));
		mqi_coef_linexpr2 	(VECT(m,1),2,   1,VECT(l,0),HHCOEF(res,1,1),   -1,VECT(l,1),HHCOEF(res,1,2) );
	}else {
		mqi_coef_cpy (VECT(m,0), HHCOEF(res,1,1));
		mqi_coef_cpy (VECT(m,1), HHCOEF(res,1,0));
	}

	if ( !mqi_coef_sign(VECT(l,1)) ) {
		mqi_vect_set_at_int (m,0, 0);
		mqi_vect_set_at_int (m,1, 1);
	}else {
		mqi_vect_set_at_int (m,0, 1);
		mqi_vect_set_at_int (m,1, 0);
	}	
	
	/** Now rewrite the par accordingly, i.e. replace l[0]*s+l[1]*t by t and
	  * m[0]*s+m[1]*t by s */

	mqi_coef_mul (SPCOEF(par,0,1,2), VECT(l,1),VECT(l,1));
	mqi_coef_linexpr2 (SPCOEF(par,0,1,1),1,  -2,VECT(m,1),VECT(l,1));
	mqi_coef_mul (SPCOEF(par,0,1,0), VECT(m,1),VECT(m,1));
	mqi_coef_mul (SPCOEF(par,1,1,2), VECT(l,0),VECT(l,0));
	mqi_coef_linexpr2 (SPCOEF(par,1,1,1), 1,  -2,VECT(m,0),VECT(l,0));
	mqi_coef_mul (SPCOEF(par,1,1,0), VECT(m,0),VECT(m,0));
	mqi_coef_linexpr2 (SPCOEF(par,2,1,2), 1,  -1,VECT(l,0),VECT(l,1));
	mqi_coef_linexpr2 (SPCOEF(par,2,1,1), 2,  1,VECT(l,0),VECT(m,1),  1,VECT(l,1),VECT(m,0));
	mqi_coef_linexpr2 (SPCOEF(par,2,1,0), 1, -1,VECT(m,0),VECT(m,1));

	/** And do the plug again */
	mqi_hhpoly_clear(res);
	qi_param_plug_sp_in_quadric (res, par, q_tmp, par);

	mqi_hpoly_init_dynamic (tmp, mqi_vect_typeof(m));
	mqi_poly_set_wi (tmp, 1);

	mqi_vect_set_at_int (l,0, 0);
	mqi_vect_set_at_int (l,1, -1);

	mqi_hpoly_t R;
	mqi_coef_t alpha;
	mqi_hpoly_t p1, p2;
	mqi_poly_div_qr (p1, R, alpha, HHPOLY(res,2), tmp);

	mqi_poly_clear(R);
	mqi_coef_clear(alpha);
	mqi_poly_div_qr (p2, R, alpha, HHPOLY(res,1), tmp);

	mqi_poly_neg (p2, p2);

	/** Expand param */
	mqi_surface_param_t tmpsp;

	mqi_surface_param_init_cpy (tmpsp, par);
	mqi_surface_param_mul_mat (par, p_trans, tmpsp);

	mqi_curve_param_t cubic;
	mqi_curve_param_init_dynamic (cubic, 4, mqi_mat_typeof(p_trans));

	mqi_surface_param_eval_poly (cubic, par, p2, p1);

	qi_curve_param_optimize (cubic);
	
	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, cubic);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CUBIC);
	
	mqi_coef_neg (VECT(l,1), VECT(l,1));
	qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(l,1), VECT(l,0));
	qi_inter_component_add_cut_parameter (comp, cut);
	qi_inter_cut_set_owner(cut, comp);
	
	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear (comp);

	qi_inter_component_init (comp, QI_INTER_COMPONENT_2, line_par);
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_LINE);
	qi_inter_cut_init (cut, QI_INTER_CUT_0, VECT(cutv,0), VECT(cutv,1));
	qi_inter_component_add_cut_parameter (comp, cut);
	qi_inter_cut_set_owner (cut, comp);

	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear (comp);

	qi_inter_set_optimal (rop);


	/** Free local memory */
	mqi_surface_param_list_clear (par, tmpsp, NULL);
	mqi_curve_param_list_clear (line_par, cubic, NULL);
	mqi_hhpoly_clear (res);
	mqi_poly_list_clear (tmp, R, p1, p2, NULL);
	mqi_mat_list_clear (p_trans, q_tmp, NULL);
	mqi_vect_list_clear (sing, dir, rat_point, cutv, l, m, NULL);
	mqi_coef_clear (alpha);

}

/** Pair of non-rational planes */
/**  m1*[u v s] +/- sqrt(D)*m2*[u] (D != 0, 1)
  *  q_22_pos is a matrix of inertia 22 */
void __qi_inter_one_mult_secant_conics_non_rational_plane (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_hpoly_t __IN det_p,
							   mqi_mat_t __IN q_22_pos, mqi_coef_t __IN D, mqi_mat_t __IN m1, mqi_mat_t __IN m2,
							   int rtype)
{

	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;

	/** Two components */
	qi_inter_init(rop);
	qi_inter_set_type (rop, 3,rtype);

	/** Computes the new quadric q of intertia 2,2
	 *  point_on_q is a point on it. det_q its determinant in the form [a,b] with det = a^2*b
	 *  (duplicate here the beginning of intersection_with_quadric22() )
	*/

	/** $$ Is it necessary to initialize those two variables, as "qi_inter_find_quadric_22_and_point" may
	 *     do the job already ? */
	mqi_vect_t point_on_q, det_q;
	mqi_mat_t q_22;
	mqi_coef_t det_R;
	qi_inter_find_quadric_22_and_point (q_22, point_on_q, det_R, det_q, q1, q2, det_p, q_22_pos);

	mqi_surface_param_t s1, s2;
	mqi_surface_param_init_dynamic (s1, 4, mqi_mat_typeof(q1));
	mqi_surface_param_init_dynamic (s2, 4, mqi_mat_typeof(q1));
	qi_inter_find_parametrization_quadric_22 (s1, s2, q_22, point_on_q, det_q, det_R);

	/** Parameterization of the quadric is  s1 + sqrt(det_q[1]) s2
	 * If det_q[1]=1 then  s2=[0,0,0,0]

	 * We first compute the implicit equation of the 2 non-rational planes 
	 * The equation of the plane is the determinat of the 4x4 matrix whose 
	 * first three collomns are 3 points on the planes and the last one is  [x y z w].
	 * Three points are :
	 *       m1[1]+/- sqrt(D)*m2 (for u=1, v=s=0)
	 *       m2[1] for (v=1, u=s=0) and m3[1] (for s=1, u=v=0). */

	/** We compute the six 2x2 sub-determinants of the 4x2 matrix consisting of the
	 * collomn 2 and 3 of m1 */
	mqi_coef_t det01, det02, det03, det12, det13, det23;
	mqi_coef_t A0, A1, A2, A3, A0p, A1p, A2p, A3p;

	mqi_coef_list_init_dynamic (mqi_mat_typeof(q1),
				    det01, det02, det03, det12, det13, det23,
				    A0, A1, A2, A3, A0p, A1p, A2p, A3p, NULL);

	mqi_coef_linexpr2 (det01, 2,	1,MAT(m1,0,1),MAT(m1,1,2),	-1,MAT(m1,0,2),MAT(m1,1,1));
	mqi_coef_linexpr2 (det02, 2,	1,MAT(m1,0,1),MAT(m1,2,2),	-1,MAT(m1,0,2),MAT(m1,2,1));
	mqi_coef_linexpr2 (det03, 2,	1,MAT(m1,0,1),MAT(m1,3,2),	-1,MAT(m1,0,2),MAT(m1,3,1));
	mqi_coef_linexpr2 (det12, 2,	1,MAT(m1,1,1),MAT(m1,2,2),	-1,MAT(m1,1,2),MAT(m1,2,1));
	mqi_coef_linexpr2 (det13, 2,	1,MAT(m1,1,1),MAT(m1,3,2),	-1,MAT(m1,1,2),MAT(m1,3,1));
	mqi_coef_linexpr2 (det23, 2,	1,MAT(m1,2,1),MAT(m1,3,2),	-1,MAT(m1,2,2),MAT(m1,3,1));

	/** Note the  first column of m1 : [a0, a1, a2, a3] and m2 = [a0', a1', a2', a3']. 
	 * Then the implicit equation of the planes are (in var x0, x1, x2, x3) :
	 *  x0*[a1*det23 - a2*det13 + a3*det12] -x1*[a0*det23 - a2*det03 + a3*det02] 
	 * +x2*[a0*det13 - a1*det03 + a3*det01] -x3*[a0*det12 - a1*det02 + a2*det01] 
	 * +/-sqrt(D)*[the same thing with [a0, a1, a2, a3] rplaced by [a0', a1', a2', a3']. 
	 *   = (A0*x0 + A1*x1 + A2*x2 + A3*x3) +/- sqrt(D)*(A0p*x0 + A1p*x1 + A2p*x2 + A3p*x3)	*/
	mqi_coef_linexpr2 (A0, 3,	1,MAT(m1,1,0),det23,	-1,MAT(m1,2,0),det13,	1,MAT(m1,3,0),det12);
	mqi_coef_linexpr2 (A1, 3,	-1,MAT(m1,0,0),det23,	1,MAT(m1,2,0),det03,	-1,MAT(m1,3,0),det02);
	mqi_coef_linexpr2 (A2, 3,	1,MAT(m1,0,0),det13,	-1,MAT(m1,1,0),det03,	1,MAT(m1,3,0),det01);
	mqi_coef_linexpr2 (A3, 3, 	-1,MAT(m1,0,0),det12,	1,MAT(m1,1,0),det02,	-1,MAT(m1,2,0),det01);
	mqi_coef_linexpr2 (A0p, 3,	1,MAT(m2,1,0),det23,	-1,MAT(m2,2,0),det13,	1,MAT(m2,3,0),det12);
	mqi_coef_linexpr2 (A1p, 3,	-1,MAT(m2,0,0),det23,	1,MAT(m2,2,0),det03,	-1,MAT(m2,3,0),det02);
	mqi_coef_linexpr2 (A2p, 3,	1,MAT(m2,0,0),det13,	-1,MAT(m2,1,0),det03,	1,MAT(m2,3,0),det01);
	mqi_coef_linexpr2 (A3p, 3,	-1,MAT(m2,0,0),det12,	1,MAT(m2,1,0),det02,	-1,MAT(m2,2,0),det01);

	/* We plug the parameterization in each of the 2 non-rational planes. 
	 * Recall the parameterization of the quadric is  s1 + sqrt(det_q[1]) s2
	 * Compute the polynomial poly = P1 + sqrt(xi) P2 +/- sqrt(D)*(P3 + sqrt(xi) P4) */
	mqi_hhpoly_t P1, P2, P3, P4;
	mqi_hhpoly_list_init_dynamic (mqi_coef_typeof(A0), P1, P2, P3, P4, NULL);


	/** P1 = A0*s1[0] + A1*s1[1] + A2*s1[2] + A3*s1[3] */
	mqi_hhpoly_linexpr(P1,4, A0,SP(s1,0),	A1,SP(s1,1),	A2,SP(s1,2),	A3,SP(s1,3));

	/** P2 = A0*s2[0] + A1*s2[1] + A2*s2[2] + A3*s2[3] */
	mqi_hhpoly_linexpr(P2,4, A0,SP(s2,0),	A1,SP(s2,1),	A2,SP(s2,2),	A3,SP(s2,3));

	/** P3 = A0p*s1[0] + A1p*s1[1] + A2p*s1[2] + A3p*s1[3] */
	mqi_hhpoly_linexpr(P3,4, A0p,SP(s1,0),	A1p,SP(s1,1),	A2p,SP(s1,2),	A3p,SP(s1,3));

	/** P4 = A0p*s2[0] + A1p*s2[1] + A2p*s2[2] + A3p*s2[3] */
	mqi_hhpoly_linexpr(P3,4, A0p,SP(s2,0),	A1p,SP(s2,1),	A2p,SP(s2,2),	A3p,SP(s2,3));

	/** Linear equation : poly = P1 + sqrt(xi) P2 +/- sqrt(D)*(P3 + sqrt(xi) P4)
	 * = u. (P1[1] + sqrt(xi)*P2[1] +/- sqrt(D)*P3[1] +/- sqrt(D*xi)*P4[1])
	 *  +v. (P1[0] + sqrt(xi)*P2[0] +/- sqrt(D)*P3[0] +/- sqrt(D*xi)*P4[0])
	 * Recall the parameterization of the quadric 22 is  s1 + sqrt(xi) s2
	 * The solution is  
	 * s1(u = P1[0] + sqrt(xi)*P2[0] +/- sqrt(D)*P3[0] +/- sqrt(D*xi)*P4[0], 
	 *     v = -(P1[1] + sqrt(xi)*P2[1] +/- sqrt(D)*P3[1] +/- sqrt(D*xi)*P4[1]))
	 * +sqrt(xi)*s2 (....)
	 * 
	 * = s1(u = P1[0], v=-P1[1]) + sqrt(xi)*s1(u = P2[0], v=-P2[1]) 
	 *     +/- sqrt(D)*s1(u = P3[0], v=-P3[1]) +/- sqrt(D*xi)*s1(u = P4[0], v=-P4[1]) 
	 *   + sqrt(xi)*s2(u = P1[0], v=-P1[1]) + xi*s2(u = P2[0], v=-P2[1]) 
	 *     +/- sqrt(D*xi)*s2(u = P3[0], v=-P3[1]) +/- sqrt(D)*xi*s2(u = P4[0], v=-P4[1]) 
	 *
	 * = s1(u = P1[0], v=-P1[1]) + xi*s2(u = P2[0], v=-P2[1]) 
	 *    + sqrt(xi)* (s1(u = P2[0], v=-P2[1]) + s2(u = P1[0], v=-P1[1]))
	 *    +/- sqrt(D)*(s1(u = P3[0], v=-P3[1]) + xi*s2(u = P4[0], v=-P4[1]))
	 *    +/- sqrt(D*xi)*(s1(u = P4[0], v=-P4[1]) + s2(u = P3[0], v=-P3[1]))

	 * The output  parameterized curve is thus : 
	 *                     c = c1 + sqrt(xi). c2 + eps. sqrt(D). (c3 + sqrt(xi). c4)
	 * c1 = s1(u = P1[0], v=-P1[1]) + xi*s2(u = P2[0], v=-P2[1]) 
	 * c2 = s1(u = P2[0], v=-P2[1]) + s2(u = P1[0], v=-P1[1])
	 * c3 = s1(u = P3[0], v=-P3[1]) + xi*s2(u = P4[0], v=-P4[1])
	 * c4 = s1(u = P4[0], v=-P4[1]) + s2(u = P3[0], v=-P3[1])
	 *  (If xi=1 then c2=c4=0) */
	
	mqi_curve_param_t c1, c2, c3, c4, c_tmp;

	mqi_curve_param_list_init_dynamic (4, mqi_coef_typeof(A0),
					   c1, c2, c3, c4, c_tmp, NULL);

	mqi_hpoly_t htmp;
	mqi_hpoly_init_dynamic (htmp, mqi_coef_typeof(A0));

	mqi_coef_t xi;
	mqi_coef_init_cpy (xi, VECT(det_q,1));
	
	if ( ! mqi_hhpoly_is_zero (P1) )
	{
		mqi_poly_neg (htmp, HHPOLY(P1,1));
		mqi_surface_param_eval_poly (c1, s1, HHPOLY(P1,0), htmp);
		mqi_surface_param_eval_poly (c2, s2, HHPOLY(P1,0), htmp);
	}

	if ( ! mqi_hhpoly_is_zero (P2) )
	{
		mqi_poly_neg (htmp, HHPOLY(P2,1));
		mqi_surface_param_eval_poly (c_tmp, s2, HHPOLY(P2,0), htmp);
		mqi_curve_param_mul_coef (c_tmp, c_tmp, xi);
		mqi_curve_param_add (c1, c1, c_tmp);
		mqi_surface_param_eval_poly (c_tmp, s1, HHPOLY(P2,0), htmp);
		mqi_curve_param_add (c2, c2, c_tmp);
	}

	if ( ! mqi_hhpoly_is_zero (P3) )
        {
		mqi_poly_neg (htmp, HHPOLY(P3,1));
		mqi_surface_param_eval_poly (c3, s1, HHPOLY(P3,0), htmp);
		mqi_surface_param_eval_poly (c4, s2, HHPOLY(P3,0), htmp);
	}

	if ( ! mqi_hhpoly_is_zero (P4) )
	{
		mqi_poly_neg (htmp, HHPOLY(P4,1));
		mqi_surface_param_eval_poly (c_tmp, s2, HHPOLY(P4,0), htmp);
		mqi_curve_param_mul_coef (c_tmp, c_tmp, xi);
		mqi_curve_param_add (c3, c3, c_tmp);
		mqi_surface_param_eval_poly (c_tmp, s1, HHPOLY(P4,0), htmp);
		mqi_curve_param_add (c4, c4, c_tmp);
	}

	/** c = c1 + sqrt(xi). c2 + eps. sqrt(D). (c3 + sqrt(xi). c4)
	  *  Recall D != 0, 1, xi !=0 */

	/** c2 and c4 are 0 */
	mqi_coef_t sq, Dxi;

	mqi_coef_init_cpy (Dxi, D);
	mqi_coef_mul	  (Dxi, Dxi, xi);

	if ( mqi_coef_cmp_schar (xi, 1) == 0 ) {

		/** The 2 conics are c1+sqrt(D)*c3 and c1-sqrt(D)*c3 */
		qi_curve_param_optimize_gcd (c1, c3);

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_CONIC, QI_INTER_COMPONENT_3,
			       			c1, c3, D);
		qi_inter_set_optimal (rop);
	
	/** Recall neither D nor xi is 1 or 0 */
	}else if ( mqi_coef_is_square (Dxi) ) {

		mqi_coef_t sq;

		mqi_coef_init_dynamic(sq, mqi_coef_typeof(Dxi));
		mqi_coef_sqrt (sq, Dxi);
		
		/** the 2 conics are c = c1 + sqrt(xi). c2 + eps. f. sqrt(xi). (c3 + sqrt(xi). c4)
		  * = (projective) xi. c2 + eps. sq. c3 + sqrt(xi). (c1 + eps. sq. c4) */
		mqi_curve_param_t c1p, c1m, c2p, c2m, tmp;
		mqi_curve_param_list_init_dynamic (4, mqi_coef_typeof(Dxi),
						c1p, c1m, c2p, c2m, tmp, NULL);

		mqi_curve_param_mul_coef (c1p, c3, sq);
		mqi_curve_param_mul_coef (c2p, c4, sq);
		mqi_curve_param_neg (c1m, c1p);
		mqi_curve_param_neg (c2m, c2p);
		mqi_curve_param_mul_coef (tmp, c2, xi);
		mqi_curve_param_add (c1p, c1p, tmp);
		mqi_curve_param_add (c2p, c2p, c1);
		mqi_curve_param_add (c1m, c1m, tmp);
		mqi_curve_param_add (c2m, c2m, c1);

		qi_curve_param_optimize_gcd (c1p, c2p);
		qi_curve_param_optimize_gcd (c1m, c2m);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_3, c1p, c2p, xi);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_component_init (comp, QI_INTER_COMPONENT_3, c1m, c2m, xi);
		qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
		qi_inter_add_component (rop, comp);
		/* discard comp */
		qi_inter_component_clear(comp);

		qi_inter_set_optimal (rop);

		/** Free local memory */
		mqi_coef_clear(sq);
		mqi_curve_param_list_clear(c1p, c1m, c2p, c2m, tmp, NULL);


	}else {

		/** Output parametrized curve: c = c1+sqrt(xi)*c2+eps*sqrt(D)*(c3+sqrt(xi)*c4) */
		qi_curve_param_optimize_conic (c1, c2, c3, c4);

		mqi_coef_t delta;
		mqi_coef_init_dynamic(delta, mqi_curve_param_typeof(c1));
		mqi_coef_set_zero (delta);

		qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_CONIC, QI_INTER_COMPONENT_6,
						c1, c2, xi, c3, c4, D, delta);

		/** Free local memory */
		mqi_coef_clear(delta);

	}

	if ( (rtype == 4) || (rtype == 6) ) {

		/** Temporary */
		mqi_coef_t coef;
		mqi_coef_init_dynamic(coef, mqi_coef_typeof(Dxi));
		mqi_coef_set_zero (coef);

		qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);

		qi_inter_cut_set_owner(cut, rop->components[0]);

		/** Free local memory */
		mqi_coef_clear(coef);

	}


	/** Free local memory */
	mqi_hhpoly_list_clear (P1, P2, P3, P4, NULL);
	mqi_poly_clear (htmp);
	mqi_curve_param_list_clear (c1, c2, c3, c4, c_tmp, NULL);
	mqi_mat_clear (q_22);
	mqi_vect_list_clear (point_on_q, det_q, NULL);
	mqi_coef_list_clear (sq, Dxi, xi, det01, det02, det03, det12, det13, det23,
				A0, A1, A2, A3, A0p, A1p, A2p, A3p, det_R);

}


/** Two secant conics.
  * (when the pair of planes is real
  * and the conics cut in real space (s_e == 1) )
  * det_p = det_e * (a*x-b*y)^2, and delta_e is the discrimant of det_e.
  * q_other is a matrix that does not correspond to the double root of the pencil
  * (namely q1 or q2) q_other2 is matrix of inertia 22 (one exists) */
void __qi_inter_one_mult_secant_conics (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_hpoly_t __IN det_p,
				        mqi_mat_t __IN q, mqi_mat_t __IN q_other, mqi_mat_t __IN q_sing, mqi_coef_t __IN delta_e,
				        int __IN in_q)
{
	
	int ctype = 3;
	int rtype;
	qi_inter_component_t	comp;
	qi_inter_cut_t		cut;	
	
	if ( in_q == 2 ) 
		rtype = 2;
	else {
		if ( mqi_coef_sign(delta_e) < 0 )
			rtype = 6;
		else
			rtype = 4;
	}

	/** Compute the two points of intersection of the conics, in all cases */
	
	/** Intersect the singular line of q with q_other */
	mqi_mat_t plug;
	mqi_mat_transformation (plug, q_other, q_sing, MQI_MAT_NOTRANSCO);

	/** Parametrize plug */
	mqi_mat_t m1, m2;
	mqi_coef_t D;
	mqi_mat_list_init_dynamic (2,1, mqi_mat_typeof(q1), m1, m2, NULL);
	qi_param_2x2 (plug, D, m1, m2);

	mqi_mat_t p1, p2;

	if ( mqi_coef_sign(D) == 0 ) {
		
		/** The two points are rational */
		mqi_mat_t sum, diff;

		mqi_mat_list_init_dynamic(mqi_mat_nrows(m1), mqi_mat_ncols(m1), mqi_mat_typeof(m1), sum, diff, NULL);
		mqi_mat_add	 (sum, m1, m2);
		mqi_mat_sub	 (diff, m1, m2);

		mqi_mat_mul_init (p1, q_sing, sum);
		mqi_mat_mul_init (p2, q_sing, diff);	

		qi_mat_optimize (p2);

		/** Free local memory */
		mqi_mat_list_clear (sum, diff, NULL);

	}else{

		/** The points are not rational */

		/** Stack the transformations */
		mqi_mat_t tmpmat;

		mqi_mat_init_cpy(tmpmat, m1);
		mqi_mat_clear(m1);
		mqi_mat_mul_init (m1, q_sing, tmpmat);

		mqi_mat_clear(tmpmat);
		mqi_mat_init_cpy(tmpmat, m2);
		mqi_mat_clear(m2);
		mqi_mat_mul_init (m2, q_sing, tmpmat);

		qi_mat_optimize_2 (m1, m2);

		/** Free local memory */
		mqi_mat_clear (tmpmat);

	}

	if ( in_q == 2 ) {

		/** Two components */
		qi_inter_init(rop);
		qi_inter_set_type (rop, ctype, rtype);

		if ( mqi_coef_sign(D) == 0 ) {
			
			mqi_curve_param_t c1, c2;
			mqi_curve_param_init_dynamic(c1, mqi_mat_nrows(p1), mqi_mat_typeof(p1));
			mqi_curve_param_init_dynamic(c2, mqi_mat_nrows(p2), mqi_mat_typeof(p2));

			mqi_curve_param_set_vect (c1, (mqi_vect_ptr)COL(p1,0));
			mqi_curve_param_set_vect (c2, (mqi_vect_ptr)COL(p2,0));

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, c1);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear (comp);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, c2);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_POINT);
			qi_inter_add_component (rop, comp);
			/* discard comp */
			qi_inter_component_clear (comp);

			/** Free local memory */
			mqi_curve_param_list_clear (c1, c2, NULL);

		}else{
	
			mqi_curve_param_t p_rat, p_D;
			mqi_curve_param_init_dynamic(p_rat, mqi_mat_nrows(m1), mqi_mat_typeof(m1));
			mqi_curve_param_init_dynamic(p_D, mqi_mat_nrows(m2), mqi_mat_typeof(m2));

			mqi_curve_param_set_vect (p_rat, (mqi_vect_ptr)COL(m1,0));
			mqi_curve_param_set_vect (p_D, (mqi_vect_ptr)COL(m2,0));

			qi_inter_create_components (rop, 2, QI_INTER_COMPONENT_POINT, QI_INTER_COMPONENT_3,
							p_rat, p_D, D);


			/** Free local memory */
			mqi_curve_param_list_clear (p_rat, p_D, NULL);

		}

		qi_inter_set_optimal(rop);

	}
	else
	{ 
		/** in_q == 1 */

		/** $$ Skipping the DEBUG section */

		/** Parametrize the 2 planes */
		mqi_mat_t m1, m2, q_sing_cp;
		mqi_coef_t D2;

		mqi_mat_init_dynamic (m1, 4,3, mqi_mat_typeof(q_sing));
		mqi_mat_init_dynamic (m2, 4,1, mqi_mat_typeof(q_sing));
		mqi_mat_init_cpy (q_sing_cp, q_sing);

		/** We make this little change so that the intersection points are easy to retrieve later on */
		if ( mqi_coef_sign(D) == 0 )
		{
			mqi_mat_cpy_col (q_sing_cp, p1, 0, 0);
			mqi_mat_cpy_col (q_sing_cp, p2, 0, 1);
		}

		/** Output is m1*[s*u t*u v] +/- sqrt(D2)*m2*[s*u] if D2 is non zero
		  * Output is m1*[s*u t*u v] +/- m2*[s*u] otherwise */
		qi_param_plane_pair (q, q_sing_cp, D2, m1, m2);

		if ( mqi_coef_sign(D2) )
		{

			/** The individual planes are not rational
			  * We search for a quadric of inertia 22 in the pencil */
          		if ( mqi_coef_sign(delta_e) < 0) /** det_p > 0 except at the double root => q_other is 22 */
				__qi_inter_one_mult_secant_conics_non_rational_plane (rop, q1, q2, det_p, q_other, D2, m1, m2, rtype);
			else
				__qi_inter_one_mult_secant_conics_non_rational_plane (rop, q1, q2, det_p, q, D2, m1, m2, rtype);
		}else{ /** D2 = 0 */

			/** Two components */
			qi_inter_init(rop);
			qi_inter_set_type (rop, ctype, rtype);

			/** Each individual plane is rational */
			mqi_mat_t m1p, m1m, ap, am;
			mqi_mat_list_init_dynamic (4,3, mqi_mat_typeof(m1), m1p, m1m);
			
			mqi_mat_cpy (m1p, m1);
			mqi_mat_cpy (m1m, m1);

			mqi_mat_add (COL(m1p,0), COL(m1,0), COL(m2,0));
			mqi_mat_sub (COL(m1m,0), COL(m1,0), COL(m2,0));

			qi_mat_optimize (m1p);
			qi_mat_optimize (m1m);

			/** Now we compute the following two matrices: they represent our two conics */
			mqi_mat_transformation (ap, q_other, m1p, MQI_MAT_NOTRANSCO);
			mqi_mat_transformation (am, q_other, m1m, MQI_MAT_NOTRANSCO);

			if ( ! mqi_coef_sign(D) )
			{
				
				/** We have a rational point on each conic, so we know how to find a
				  * rational param
				  * The two points of intersection correspond to (0 0 1) and (0 1 0) in
				  * the space of the conics */

				mqi_curve_param_t parp, parm;
				mqi_mat_t p_transp, p_transm;
				mqi_vect_t lp, lm;

				mqi_mat_list_init_dynamic (3,3, mqi_mat_typeof(q1), p_transp, p_transm, NULL);
				mqi_curve_param_list_init_dynamic (3, mqi_mat_typeof(q1), parp, parm, NULL);
				mqi_vect_list_init_dynamic (2, mqi_mat_typeof(q1), lp, lm, NULL);

				qi_param_conic_through_origin (ap, p_transp, parp, lp);
				qi_param_conic_through_origin (am, p_transm, parm, lm);

				/** Stack the transformation */
				mqi_mat_t tmpmat;

				mqi_mat_init_cpy(tmpmat, p_transp);
				mqi_mat_clear(p_transp);
				mqi_mat_mul_init (p_transp, m1p, tmpmat);

				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, p_transm);
				mqi_mat_clear(p_transm);
				mqi_mat_mul_init (p_transm, m1m, tmpmat);

				/** Rescale so that l[0]*u+l[1]*v is replaced by v */
				mqi_mat_t scalep, scalem;
				mqi_mat_list_init_dynamic (3,3, mqi_mat_typeof(q1), scalep, scalem, NULL);

				mqi_coef_mul (MAT(scalep,0,0), VECT(lp,1), VECT(lp,1));
				mqi_coef_mul (MAT(scalep,1,0), VECT(lp,0), VECT(lp,0));
				mqi_mat_set_at_int (scalep,1,1, 1);
				mqi_coef_linexpr1(MAT(scalep,1,2),1, -2,VECT(lp,0));
				mqi_coef_linexpr2(MAT(scalep,2,0),1, -1,VECT(lp,0),VECT(lp,1));
				mqi_mat_set_at (scalep,2,2, VECT(lp,1));
	
				mqi_coef_mul (MAT(scalem,0,0), VECT(lm,1), VECT(lm,1));
				mqi_coef_mul (MAT(scalem,1,0), VECT(lm,0), VECT(lm,0));
				mqi_mat_set_at_int (scalem,1,1, 1);
				mqi_coef_linexpr1(MAT(scalem,1,2),1, -2,VECT(lm,0));
				mqi_coef_linexpr2(MAT(scalem,2,0),1, -1,VECT(lm,0),VECT(lm,1));
				mqi_mat_set_at (scalem,2,2, VECT(lm,1));
	
				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, p_transp);
				mqi_mat_clear(p_transp);
				mqi_mat_mul_init (p_transp, p_transp, tmpmat);

				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, p_transm);
				mqi_mat_clear(p_transm);
				mqi_mat_mul_init (p_transm, p_transm, tmpmat);

				qi_mat_optimize_3 (p_transp);
				qi_mat_optimize_3 (p_transm);
				
				mqi_curve_param_t conicp, conicm;
				mqi_curve_param_init_cpy (conicp, parp);
				mqi_curve_param_init_cpy (conicm, parm);

				mqi_curve_param_mul_mat (conicp, p_transp, conicp);
				mqi_curve_param_mul_mat (conicm, p_transm, conicm);
				
				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conicp);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conicm);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
				qi_inter_add_component (rop, comp);
				/* discard comp */
				qi_inter_component_clear(comp);

				qi_inter_set_optimal (rop);

				/** Free local memory */
				mqi_curve_param_list_clear (parp, parm, conicp, conicm, NULL);
				mqi_mat_list_clear (p_transp, p_transm, tmpmat, scalep, scalem, NULL);
				mqi_vect_list_clear (lp, lm, NULL);
		
			}
			else
			{ /** If D != 0 */

				/* The two points are not rational... but maybe we can still find a
				* rational param for the conics...

				* Parameterize the first conic */
				mqi_coef_t D3;
				mqi_mat_t p_transp, p_trans2p;
				mqi_curve_param_t par;

				mqi_mat_list_init_dynamic(3,3, mqi_mat_typeof(q1), p_transp, p_trans2p, NULL);
				mqi_curve_param_init_dynamic(par, 3, mqi_mat_typeof(q1));
				qi_param_conic (ap, D3, p_transp, p_trans2p, par);

				if ( ! mqi_coef_sign(D3) )
				{

					/** Found a rational conic */
					/** Stack the transformation */
					mqi_mat_t tmpmat;

					mqi_mat_init_cpy(tmpmat, p_transp);
					mqi_mat_clear(p_transp);
					mqi_mat_mul_init (p_transp, m1p, tmpmat);

					qi_mat_optimize_3 (p_transp);

					mqi_curve_param_t conic;
					mqi_curve_param_init_cpy (conic, par);
					mqi_curve_param_mul_mat  (conic, p_transp, conic);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

					qi_inter_component_set_optimal(comp);

					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear (comp);


					/** Free local memory */
					mqi_mat_clear(tmpmat);
					mqi_curve_param_clear(conic);

				}
				else
				{ /** D3 != 0 */

					/** One square root cannot be simplified */
					/** Stack the transformation */
					mqi_mat_t tmpmat;

					mqi_mat_init_cpy(tmpmat, p_transp);
					mqi_mat_clear(p_transp);
					mqi_mat_mul_init (p_transp, m1p, tmpmat);

					mqi_mat_clear(tmpmat);
					mqi_mat_init_cpy(tmpmat, p_trans2p);
					mqi_mat_clear(p_trans2p);
					mqi_mat_mul_init (p_trans2p, m1p, tmpmat);

					qi_mat_optimize_4 (p_transp, p_trans2p);
					
					mqi_curve_param_t conic, conic2;
					mqi_curve_param_init_cpy (conic, par);
					mqi_curve_param_init_cpy (conic2, par);

					mqi_curve_param_mul_mat (conic, p_transp, conic);
					mqi_curve_param_mul_mat (conic2, p_trans2p, conic2);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_3, conic, conic2, D3);
					if ( qi_settings.optimize )
						qi_inter_component_set_optimal (comp);
	
					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear (comp);

				
					/** Free local memory */
					mqi_mat_clear (tmpmat);
					mqi_curve_param_list_clear (conic, conic2, NULL);

				}

				/** Parametrize the second conic */
				mqi_mat_t p_transm, p_trans2m;

				mqi_mat_list_init_dynamic(3,3, mqi_mat_typeof(q1), p_transm, p_trans2m, NULL);

				qi_param_conic (am, D3, p_transm, p_trans2m, par);
				
				if ( !mqi_coef_sign(D3) )
				{

					/** Found a rational conic */
					/** Stack the transformation */
					mqi_mat_t tmpmat;

					mqi_mat_init_cpy(tmpmat, p_transm);
					mqi_mat_clear(p_transm);
					mqi_mat_mul_init (p_transm, m1m, tmpmat);

					qi_mat_optimize_3 (p_transm);

					mqi_curve_param_t conic;
					mqi_curve_param_init_cpy (conic, par);
					mqi_curve_param_mul_mat  (conic, p_transm, conic);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
					qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
					qi_inter_component_set_optimal (comp);

					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear (comp);


					/** Free local memory */
					mqi_mat_clear(tmpmat);
					mqi_curve_param_clear(conic);

				}
				else
				{ /** D3 != 0 */

					/** One square root cannot be simplified */
					/** Stack the transformation */
					mqi_mat_t tmpmat;

					mqi_mat_init_cpy(tmpmat, p_transm);
					mqi_mat_clear(p_transm);
					mqi_mat_mul_init (p_transm, m1m, tmpmat);

					mqi_mat_clear(tmpmat);
					mqi_mat_init_cpy(tmpmat, p_trans2m);
					mqi_mat_clear(p_trans2m);
					mqi_mat_mul_init (p_trans2m, m1m, tmpmat);

					qi_mat_optimize_4 (p_transm, p_trans2m);
				
					mqi_curve_param_t conic, conic2;	
					mqi_curve_param_init_cpy (conic, par);
					mqi_curve_param_init_cpy (conic2, par);

					mqi_curve_param_mul_mat (conic, p_transm, conic);
					mqi_curve_param_mul_mat (conic2, p_trans2m, conic2);

					qi_inter_component_init (comp, QI_INTER_COMPONENT_3, conic, conic2, D3);

					if ( qi_settings.optimize )
						qi_inter_component_set_optimal (comp);
					
					qi_inter_add_component (rop, comp);
					/* discard comp */
					qi_inter_component_clear(comp);

					/** Free local memory */
					mqi_mat_clear(tmpmat);
					mqi_curve_param_list_clear(conic, conic2, NULL);

				}

				mqi_coef_clear(D3);
				mqi_mat_list_clear(p_transp, p_trans2p, p_transm, p_trans2m, NULL);
				mqi_curve_param_clear(par);

			} /** End if D = 0 */

			/** Free local memory */
			mqi_mat_list_clear(m1p, m1m, ap, am, NULL);

		} /** End if D2 = 0 */

		/** $$ CUT PARAM: Temporary */
		mqi_coef_t coef;
		mqi_coef_init_dynamic (coef, mqi_mat_typeof(q1));
		mqi_coef_set_zero (coef);

		qi_inter_cut_init (cut, QI_INTER_CUT_0, coef, coef);

		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[0], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);
		qi_inter_component_add_cut_parameter (rop->components[1], cut);

		qi_inter_cut_set_owner(cut, rop->components[0]);


		mqi_mat_list_clear(m1, m2, q_sing_cp, NULL);
		mqi_coef_list_clear(D2, coef, NULL);

	} /** End if in_q == ... */

	/** Free local memory */
	mqi_mat_list_clear (m1, m2, p1, p2, plug, NULL);
	mqi_coef_clear(D);

}

/** Conic lying in a non-rational plane */
/**  The non-rational plane of the pencil are parameterized by one of the two planes
  *  m1*[u v s] +/- sqrt(D)*m2*[u] (D != 0)
  *  D is here already optimized under -o
  *  q is a matrix that does not correspond to the double root of the pencil (namely q1 or q2) */
void __qi_inter_one_mult_one_conic_non_rational_plane (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_hpoly_t __IN det_p,
						       mqi_mat_t __IN q, mqi_coef_t __IN D, mqi_mat_t __IN m1, mqi_mat_t __IN m2_tmp)
{
	
	qi_inter_component_t	comp;


	/* Here the determinential polynomial is always negative execpt at the rational
	* root which corresponds to the pair of non-rational planes (parameterized by
	* m1, m2 and D).  

	* Let T be the 4x3 parameterization matrix of the plane containing the conic,
	* namely, the parameterization is (m1[1] +/- sqrt(D)*m2)*u + m1[2]*v + m1[3]*s
	* and the matrix : its 1st collumn is m1[1] +/- sqrt(D)*m2, the 2nd and 3rd
	* collumns are  m1[i=2, 3] 
	* For convenience we rename m2 to be a 4x3 matrix whose 2nd and 3rd collum are 0
	* Then T = m1 +/- sqrt(D)*m2 */
	
	mqi_mat_t m2;
	mqi_mat_init_dynamic (m2, 4,3, mqi_mat_typeof(m2_tmp));

	mqi_mat_cpy_col	     (m2, m2_tmp, 0, 0);

	/* The 3x3 matrix associated to the  equation of the conic is 
	* C=T^t.Q.T where Q is any other (ie, not the pair of planes) quadric
	* of the pencil, namely here q.
	* One conic for say +sqrt(D) is only imaginary, and the other (for -+sqrt(D))
	* is the intersection curve we look for.  

	* Let C=C1 +/- sqrt(D)*C2 
	*   = (m1 + sqrt(D)*m2)*Q*(m1 + sqrt(D)*m2)
	*   = m1*Q*m1 + D*m2*Q*m2 + sqrt(D)*(m1*Q*m2 + m2*Q*m1) */
	mqi_mat_t C1, C2, Ctmp, tmp, tmp2;

	mqi_mat_mul_init (tmp, q, m1);
	mqi_mat_transpose_init (tmp2, m1);
	mqi_mat_mul_init (C1, tmp2, tmp);

	mqi_mat_list_clear(tmp, tmp2, NULL);

	mqi_mat_mul_init (tmp, q, m2);
	mqi_mat_transpose_init (tmp2, m2);
	mqi_mat_mul_init (Ctmp, tmp2, tmp);
	mqi_mat_mul_coef (Ctmp, Ctmp, D);
	mqi_mat_add (C1, C1, Ctmp);

	mqi_mat_list_clear(tmp, tmp2, NULL);

	mqi_mat_mul_init (tmp, q, m2);
	mqi_mat_transpose_init (tmp2, m1);
	mqi_mat_mul_init (C2, tmp2, tmp);

	mqi_mat_list_clear(tmp, tmp2, NULL);

	mqi_mat_mul_init (tmp, q, m1);
	mqi_mat_transpose_init (tmp2, m2);
	mqi_mat_mul_init (Ctmp, tmp2, tmp);
	mqi_mat_add (C2, C2, Ctmp);

	/** Inertia of C1+sqrt(D)*C2 is [3 0] */
	if ( ! qi_mat_inertia_non_rational_conic (C1, C2, D) )
	{
		mqi_mat_neg (C2, C2); /** Inertia of C1+sqrt(D)*C2 is now [2 1] */
		mqi_mat_neg (m2, m2);
	}

	mqi_vect_t D2;
	mqi_mat_t p_trans0, p_trans1, p_trans2, p_trans3;
	mqi_curve_param_t par;

	mqi_vect_init_dynamic(D2, 2, mqi_coef_typeof(D));
	mqi_mat_list_init_dynamic(3,3, mqi_mat_typeof(m2), p_trans0, p_trans1, p_trans2, p_trans2, NULL);
	mqi_curve_param_init_dynamic(par, 3, mqi_mat_typeof(m2));
	qi_param_conic_nr (C1, C2, D,  D2, p_trans0, p_trans1, p_trans2, p_trans3, par);

	/* The parameterization of the conic  in the plane is p*par where 
	* p = (p_trans0 + sqrt(D)*p_trans1 + sqrt(D2)*p_trans2 + sqrt(D)*sqrt(D2)*p_trans3) 
	* par = [u^2 v^2 uv],  D2 = D2[0] + sqrt(D)*D2[1]
	* Thus the parameterization of the conic  in space is 
	* T*p*par where T be the 4x3 parameterization matrix of the plane containing
	* the conic. T = m1 + sqrt(D)*m2
	* T*p = (m1 + sqrt(D)*m2) * (p_trans0 + sqrt(D)*p_trans1 +
	* sqrt(D2)*p_trans2+sqrt(D)*sqrt(D2)*p_trans3)  

	* T*p = (m1*p_trans0 + D*m2*p_trans1)  + sqrt(D)*(m1*p_trans1+m2*p_trans0)
	* +sqrt(D2)*(m1*p_trans2 + D*m2*p_trans3)+sqrt(D)*sqrt(D2)*(m1*p_trans3+m2*p_trans2)
	* Let T*p = p0 + sqrt(D)*p1 + sqrt(D2)*p2 + sqrt(D)*sqrt(D2)*p3	*/
	mqi_mat_t p0, p1, p2, p3, p_tmp;
	mqi_mat_list_init_dynamic(4,3, mqi_mat_typeof(C1), p0, p1, p2, p3, p_tmp, NULL);

	mqi_mat_mul (p0, m1, p_trans0);
	mqi_mat_mul (p_tmp, m2, p_trans1);
	mqi_mat_mul_coef (p_tmp, p_tmp, D);
	mqi_mat_add (p0, p0, p_tmp);

	mqi_mat_mul (p1, m1, p_trans1);
	mqi_mat_mul (p_tmp, m2, p_trans0);
	mqi_mat_add (p1, p1, p_tmp);

	mqi_mat_mul (p2, m1, p_trans2);
	mqi_mat_mul (p_tmp, m2, p_trans3);
	mqi_mat_mul_coef (p_tmp, p_tmp, D);
	mqi_mat_add (p2, p2, p_tmp);

	mqi_mat_mul (p3, m1, p_trans3);
	mqi_mat_mul (p_tmp, m2, p_trans2);
	mqi_mat_add (p3, p3, p_tmp);

	mqi_curve_param_t c0, c1, c2, c3;
	mqi_curve_param_init_cpy (c0, par);
	mqi_curve_param_init_cpy (c1, par);
	mqi_curve_param_init_cpy (c2, par);
	mqi_curve_param_init_cpy (c3, par);

	mqi_curve_param_mul_mat (c0, p0, c0);
	mqi_curve_param_mul_mat (c1, p1, c1);
	mqi_curve_param_mul_mat (c2, p2, c2);
	mqi_curve_param_mul_mat (c3, p3, c3);

	/** The parameterization is = c0 + sqrt(D)*c1 + sqrt(D2)*c2 + sqrt(D)*sqrt(D2)*c3 */
	mqi_coef_t gcd_tmp, coef;
	mqi_coef_init_dynamic (gcd_tmp, mqi_mat_typeof(p0));
	mqi_coef_init_dynamic (coef, mqi_coef_typeof(gcd_tmp));

	mqi_curve_param_content	(gcd_tmp, c0);
	mqi_curve_param_content (coef, c1);
	mqi_coef_gcd (gcd_tmp, gcd_tmp, coef);

	mqi_curve_param_content (coef, c2);
	mqi_coef_gcd (gcd_tmp, gcd_tmp, coef);

	mqi_curve_param_content (coef, c3);
	mqi_coef_gcd (gcd_tmp, gcd_tmp, coef);
	
	mqi_curve_param_divexact_coef (c0, c0, gcd_tmp);
	mqi_curve_param_divexact_coef (c1, c1, gcd_tmp);
	mqi_curve_param_divexact_coef (c2, c2, gcd_tmp);
	mqi_curve_param_divexact_coef (c3, c3, gcd_tmp);

	qi_inter_component_init (comp, QI_INTER_COMPONENT_6, 
				 c0, c1, D, c2, c3, VECT(D2,0), VECT(D2,1));
	qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
	qi_inter_add_component (rop, comp);
	/* discard comp */
	qi_inter_component_clear(comp);

	qi_inter_set_optimal (rop);

	/** Free local memory */
	mqi_mat_list_clear(m2, C1, C2, Ctmp, tmp, tmp2, p_trans0, p_trans1, p_trans2, p_trans3,
			   p0, p1, p2, p3, p_tmp, NULL);
	mqi_coef_list_clear(coef, gcd_tmp, NULL);
	mqi_vect_clear(D2);
	mqi_curve_param_list_clear(par, c0, c1, c2, c3, NULL);

}

/** Two secant conics */
/** When the pair of planes is real and the conics do not cut in real space (s_e == -1) */
/** det_p = det_e * (a*x-b*y)^2, and delta_e is the discrimant of det_e. */
/** q_other is a matrix that does not correspond to the double root of the pencil */
/** (namely q1 or q2) */
/** q_other2 is matrix of inertia 22 if one exists in the pencil (that is when delta_e>0) */
void __qi_inter_one_mult_secant_conics_no_sec (qi_inter_t __INIT rop, mqi_mat_t __IN q1, mqi_mat_t __IN q2, mqi_hpoly_t __IN det_p, mqi_mat_t __IN q,
					       mqi_mat_t __IN q_other, mqi_mat_t __IN q_other2, mqi_mat_t __IN q_sing, mqi_coef_t __IN delta_e)
{
	
	int ctype = 3, rtype;
	qi_inter_component_t	comp;


	if ( mqi_coef_sign(delta_e) < 0 ) /** Other roots of determinantal equation are complex */
		rtype = 5;
	else
		rtype = 3;

	/** The pair of planes is always real. */
	mqi_coef_t D;
	mqi_mat_t m1, m2;
	mqi_mat_init_dynamic (m1, 4, 3, mqi_mat_typeof(q1));
	mqi_mat_init_dynamic (m2, 4, 1, mqi_mat_typeof(q1));

	qi_param_plane_pair (q, q_sing, D, m1, m2);

	if ( ! mqi_coef_sign(D) )
	{
		/** The individual planes are not rational */
		if ( mqi_coef_sign(delta_e) < 0)
		{
			__qi_inter_one_mult_one_conic_non_rational_plane (rop, q1, q2, det_p, q_other, D, m1, m2);
			mqi_coef_clear(D);
			mqi_mat_list_clear(m1, m2, NULL);
			return;
		}
		else
		{
			__qi_inter_one_mult_secant_conics_non_rational_plane (rop, q1, q2, det_p, q_other2, D, m1, m2, rtype);
			mqi_coef_clear(D);
			mqi_mat_list_clear(m1, m2, NULL);
			return;
		}
	}
	else
	{
		
		/** D = 0 */
		/** One or two components */

		if ( mqi_coef_sign(delta_e) < 0 )
			qi_inter_init(rop);
		else
			qi_inter_init(rop);

		qi_inter_set_type (rop, ctype, rtype);

		/** Each individual plane is rational */
		mqi_mat_t m1p, m1m;
		mqi_mat_init_cpy (m1p, m1);
		mqi_mat_init_cpy (m1m, m1);
				
		mqi_mat_add (COL(m1p,0), COL(m1,0), COL(m2,0));
		mqi_mat_sub (COL(m1m,0), COL(m1,0), COL(m2,0));

		qi_mat_optimize (m1p);
		qi_mat_optimize (m1m);

		/** Now we compute the following two matrices: they represent our two conics */
		mqi_mat_t ap, am;
		mqi_mat_transformation (ap, q_other, m1p, MQI_MAT_NOTRANSCO);
		mqi_mat_transformation (am, q_other, m1m, MQI_MAT_NOTRANSCO);

		/** If delta_e < 0, then one of the conics is imaginary */
		mqi_vect_t inertia;
		qi_mat_inertia (inertia, ap);

		if ( (delta_e < 0) && (mqi_coef_cmp_schar (VECT(inertia,0), 3) == 0) ) {
			/** Swap ap and am, m1p and m1m */
			mqi_mat_swap(ap, am);
			mqi_mat_swap(m1p, m1m);
		}
		
		/** Parametrize the first conic */
		mqi_mat_t p_trans, p_trans2;
		mqi_curve_param_t par;

		mqi_mat_list_init_dynamic(3,3, mqi_coef_typeof(D), p_trans, p_trans2, NULL);
		mqi_curve_param_init_dynamic(par, 3, mqi_coef_typeof(D));
		qi_param_conic (ap, D, p_trans, p_trans2, par);


		if ( ! mqi_coef_sign(D) )
		{

			/** Found a rational conic */
			/** Stack the transformations */	
			mqi_mat_t tmpmat;

			mqi_mat_init_cpy(tmpmat, p_trans);
			mqi_mat_clear(p_trans);			
			mqi_mat_mul_init (p_trans, m1p, tmpmat);

			qi_mat_optimize_3 (p_trans);

			mqi_curve_param_t conic;
			mqi_curve_param_init_cpy (conic, par);
			mqi_curve_param_mul_mat  (conic, p_trans, conic);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
			qi_inter_component_set_optimal (comp);		
			qi_inter_add_component (rop, comp);

			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_mat_clear(tmpmat);
			mqi_curve_param_clear(conic);

		}
		else
		{

			/** One square root cannot be discarded */
			/** Stack the transformations */
			mqi_mat_t tmpmat;

			mqi_mat_init_cpy(tmpmat, p_trans);
			mqi_mat_clear(p_trans);
			mqi_mat_mul_init (p_trans, m1p, tmpmat);

			mqi_mat_clear(tmpmat);
			mqi_mat_init_cpy(tmpmat, p_trans2);
			mqi_mat_clear(p_trans2);
			mqi_mat_mul_init (p_trans2, m1p, tmpmat);
			
			qi_mat_optimize_4 (p_trans, p_trans2);

			mqi_curve_param_t conic, conic2;
			mqi_curve_param_init_cpy (conic, par);
			mqi_curve_param_init_cpy (conic2, par);

			mqi_curve_param_mul_mat (conic, p_trans, conic);
			mqi_curve_param_mul_mat (conic2, p_trans2, conic2);

			qi_inter_component_init (comp, QI_INTER_COMPONENT_3, conic, conic2, D);
			qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

			if ( qi_settings.optimize )
				qi_inter_component_set_optimal (comp);

			qi_inter_add_component (rop, comp);

			/* discard comp */
			qi_inter_component_clear(comp);

			/** Free local memory */
			mqi_mat_clear(tmpmat);
			mqi_curve_param_list_clear(conic, conic2, NULL);

		}

		/** Parametrize the second conic, if any */
		if ( mqi_coef_sign(delta_e) > 0 )
		{

			mqi_mat_t p_trans, p_trans2;
			mqi_curve_param_t par;

			mqi_mat_list_init_dynamic(3,3, mqi_coef_typeof(D), p_trans, p_trans2, NULL);
			mqi_curve_param_init_dynamic(par, 3, mqi_coef_typeof(D));
			qi_param_conic (am, D, p_trans, p_trans2, par);

			if ( ! mqi_coef_sign(D) )
			{

				/** Found a rational conic */
				/** Stack the transformations */	
				mqi_mat_t tmpmat;

				mqi_mat_init_cpy(tmpmat, p_trans);
				mqi_mat_clear(p_trans);			
				mqi_mat_mul_init (p_trans, m1m, tmpmat);

				qi_mat_optimize_3 (p_trans);

				mqi_curve_param_t conic;
				mqi_curve_param_init_cpy (conic, par);
				mqi_curve_param_mul_mat  (conic, p_trans, conic);

				qi_inter_component_init (comp, QI_INTER_COMPONENT_2, conic);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);
				qi_inter_component_set_optimal (comp);		
				qi_inter_add_component (rop, comp);

				/* discard comp */
				qi_inter_component_clear(comp);

				/** Free local memory */
				mqi_mat_clear(tmpmat);
				mqi_curve_param_clear(conic);

			}
			else
			{

				/** One square root cannot be discarded */
				/** Stack the transformations */
				mqi_mat_t tmpmat;

				mqi_mat_init_cpy(tmpmat, p_trans);
				mqi_mat_clear(p_trans);
				mqi_mat_mul_init (p_trans, m1m, tmpmat);

				mqi_mat_clear(tmpmat);
				mqi_mat_init_cpy(tmpmat, p_trans2);
				mqi_mat_clear(p_trans2);
				mqi_mat_mul_init (p_trans2, m1m, tmpmat);

				qi_mat_optimize_4 (p_trans, p_trans2);

				mqi_curve_param_t conic, conic2;
				mqi_curve_param_init_cpy (conic, par);
				mqi_curve_param_init_cpy (conic2, par);

				mqi_curve_param_mul_mat (conic, p_trans, conic);
				mqi_curve_param_mul_mat (conic2, p_trans2, conic2);

				qi_inter_component_init (comp, QI_INTER_COMPONENT_3, conic, conic2, D);
				qi_inter_component_set_type (comp, QI_INTER_COMPONENT_CONIC);

				if ( qi_settings.optimize )
					qi_inter_component_set_optimal (comp);

				qi_inter_add_component (rop, comp);

				/* discard comp */
				qi_inter_component_clear(comp);

				/** Free local memory */
				mqi_mat_clear(tmpmat);
				mqi_curve_param_list_clear(conic, conic2, NULL);

			}


			/** Free local memory */
			mqi_mat_list_clear (p_trans, p_trans2, NULL);
			mqi_curve_param_clear(par);

		}


		/** Free local memory */
		mqi_mat_list_clear (m1p, m1m, ap, am, p_trans, p_trans2, NULL);
		mqi_vect_clear(inertia);
		mqi_curve_param_clear(par);

	}

	/** Free local memory */
	mqi_mat_list_clear(m1, m2, NULL);
	mqi_coef_clear(D);

}

