/** An interactive shell to perform live intersection calculation
 *  with the libqi library. */ 

#include "parse_args.h"
#include "shell.h"

int main(int argc, char	**argv)
{

  parseArgs (argc, argv);
  shell_main ();

}
