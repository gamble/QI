// Main intersection loop
// Parsing of cases depending on number of multiple roots

// Bit of template instantiation
#include "QITemplateInst.cc"

#include "QIInter.h"

#include "QIElem.h"
#include "QINumber.h"
#include "QIVanishDet.h"
#include "QINoMult.h"
#include "QIOneMult.h"
#include "QITwoMult.h"

#include "QIBench.h"

short QI_CPU_TIME_MS = 0;

using namespace std;
using namespace LiDIA;

// Enter namespace QI
namespace QI {
	
// The main intersection procedure
quad_inter <bigint> intersection(const bigint_matrix &q1, const bigint_matrix &q2, 
				 const int opt_level, ostream &s)
{
  quad_inter <bigint> result;
  int start, end;

  start = qi_bench_cputime();

  hom_polynomial <bigint> det_p = det_pencil(q1,q2);

#ifdef HARD_DEBUG
  cout << "q1:" << endl;
  cout << q1 << endl << endl;
  cout << "q2:" << endl;
  cout << q2 << endl << endl;

  cout << "<< determinental equation without optimization: " << det_p << endl;
#endif

  if (det_p.is_zero()) {
#ifdef DEBUG
	  s << ">> determinantal equation vanishes" << endl;
#endif
	  result =  inter_vanish_det(q1,q2,det_p,opt_level,s);
  }
  else
    {
      // Keep a copy of the original determinantal equation for the (2,2) case
      hom_polynomial <bigint> det_p_orig = det_p;

      #ifdef DEBUG
      s << ">> optimization of coefficients of determinantal equation" << endl;
      #endif

      optimize(det_p);  

      #ifdef DEBUG
      s << ">> determinantal equation: ";
      det_p.print_verbose(s,'l','m');
      s << endl;
      #endif

#ifdef DEBUG
      s << "Partial derivatives: " << endl;
      derivative(det_p,'x').print_verbose(s,'l','m');
      s << endl << endl;
      derivative(det_p,'y').print_verbose(s,'l','m');
#endif
      hom_polynomial <bigint> gcd_p = gcd(derivative(det_p,'x'),derivative(det_p,'y'));

      #ifdef DEBUG
      s << ">> optimization of coefficients of gcd" << endl;
      #endif	  
	
      optimize(gcd_p);

      #ifdef DEBUG
      s << ">> gcd of derivatives of determinantal equation: ";
      gcd_p.print_verbose(s,'l','m');
      s << endl;
      #endif    

      if (gcd_p.degree() == 0) // No multiple root
      	{
#ifdef DEBUG
		cout << "No multiple root" << endl;
#endif
		result = inter_no_mult(q1,q2,det_p,det_p_orig,opt_level,s);
	}
      else if ((gcd_p.degree() == 1) || (gcd_p.degree() == 3) ||
         ((gcd_p.degree() == 2) && (discriminant2(gcd_p).is_zero()))) // One mult. root
 	{
#ifdef DEBUG
	      cout << "One multiple root" << endl;
#endif
	      result = inter_one_mult(q1,q2,det_p,det_p_orig,gcd_p,opt_level,s);
	}
      else // Two double roots
      	{
#ifdef DEBUG
		cout << "Two double roots" << endl;
#endif
		result = inter_two_mult(q1,q2,det_p,det_p_orig,gcd_p,opt_level,s);
     	}

    }
  
  end = qi_bench_cputime();

  QI_CPU_TIME_MS = end - start;

  return result;

}

} // end of namespace QI
