# Current version: Change **ONLY** these
# macro definitions.
m4_define([MAJOR], 	0)
m4_define([MINOR], 	9)
m4_define([PATCHLEVEL], 33)
m4_define([VERSION_STR], MAJOR.MINOR.PATCHLEVEL)
# ****************************

AC_INIT(QI library, [VERSION_STR], sylvain.petitjean@loria.fr/sylvain.lazard@loria.fr/julien.clement@loria.fr, libqi)
#AM_INIT_AUTOMAKE(libqi, [VERSION_STR])
AM_INIT_AUTOMAKE

AC_PROG_CC
AC_LANG_C

AC_PROG_CXX
AC_LANG_CPLUSPLUS

AC_HEADER_STDC

AC_PROG_LIBTOOL(libtool)
AC_PROG_RANLIB
AC_PROG_INSTALL

AC_CHECK_FUNC(memset,,AC_MSG_FAILURE([memset not found.])) 
AC_CHECK_FUNC(strlen,,AC_MSG_FAILURE([strlen not found.]))
AC_CHECK_FUNC(getrusage,AC_DEFINE([HAVE_GETRUSAGE],1,[If getrusage is available, we can benchmark with a higher accuracy]),getrusage not found: benchmarking accuracy will down to second)

AC_ARG_WITH([gmp-path],
            [AS_HELP_STRING([--with-gmp-path],
              		[Path to the GMP library and includes, example: /myserver/gmp-4.2]
		)]
	    ,
            [	
		echo "Custom GMP path specified: $withval" ; 
		GMP_PATH=${withval}
	    ]
	    , 
	    [])

AC_ARG_WITH([LiDIA-path],
            [AS_HELP_STRING([--with-LiDIA-path],
              		[Path to the LiDIA library and includes, example: /myserver/lidia-2.2.0]
		)]
	    ,
            [	
		echo "Custom LiDIA path specified: $withval" ; 
		LiDIA_PATH=${withval}
	    ]
	    , 
	    [])


debug_mode="no"
AC_ARG_ENABLE(debug,
		[AS_HELP_STRING([--enable-debug],
              		[Toggle the QI library in debug mode (more verbose)]
		)]
		,debug_mode="yes",)
 
color_option="yes"
AC_ARG_ENABLE(color,
		[AS_HELP_STRING([--disable-color],
              		[Turn off colored output]
		)]
		,color_option="no",)

test_mode="no"
AC_ARG_ENABLE(testing,
                [AS_HELP_STRING([--enable-testing],
                        [Enables testing mode, ignoring every previous options (color, debug)]
                )]
                ,test_mode="yes",)


# See "acinclude.m4" for details about these two custom macros
CHECK_LIB_GMP
if test $found_gmp = "no"; then AC_MSG_FAILURE([
	GMP library cannot be found. You can download it at: 
		http://www.swox.com/gmp/
	]) ; fi 

CHECK_LIB_LIDIA
if test $found_lidia = "no" ; then AC_MSG_FAILURE([
	LiDIA library cannot be found. You can download it at: 
		http://www.cdc.informatik.tu-darmstadt.de/TI/LiDIA
	]) ; fi

	
# QI compilation flags

# flag "-march=pentium4" shouldn't be used now.
# LIBQI_CFLAGS="$CFLAGS -Wall -ansi -pedantic -march=pentium4 -DPRINTOUT"
#LIBQI_CFLAGS="$CFLAGS -Wall -ansi -pedantic -DPRINTOUT"


CFLAGS=
CXXFLAGS=
LIBQI_CFLAGS="-Wall -ansi -pedantic"

echo -n "Checking whether test mode is set... "
if test $test_mode = "yes"
then
  echo "yes"
  debug_mode="no"
  color_option="no"
else
  echo "no"
fi
	

echo -n "Checking whether the DEBUG flag should be set... "
if test $debug_mode = "yes"
then 
  echo "yes"
  LIBQI_CFLAGS="$LIBQI_CFLAGS -O0 -g"
  AC_DEFINE([DEBUG],, [Debug mode])
else 
  echo "no"
  LIBQI_CFLAGS="$LIBQI_CFLAGS -O2"
fi

echo -n "Checking whether the COLOR flag should be set... "
if test $color_option = "yes"
then
  echo "yes"
  LIBQI_CFLAGS="$LIBQI_CFLAGS"
  AC_DEFINE([LIBQI_COLOR_SHELL],, [Activate colors in console])
else
  echo "no"
fi

CHECK_TESTING_COMPLIANT
test $testing_compliant = "yes" && AC_DEFINE([LIBQI_TESTING_COMPLIANT],, [Testing compliance])

LIBQI_INCLUDES="-I${GMP_PATH}/include -I${LiDIA_PATH}/include"
LIBQI_LDFLAGS="-Wl-rpath,${GMP_PATH}/lib,${LiDIA_PATH}/lib -L${GMP_PATH}/lib -L${LiDIA_PATH}/lib"


LIBQI_VERSION=MAJOR:MINOR:PATCHLEVEL

# Defines these values as macros, inside the config.h file.
AC_DEFINE([LIBQI_VERSION_MAJOR], MAJOR, [Major version])
AC_DEFINE([LIBQI_VERSION_MINOR], MINOR, [Minor version])
AC_DEFINE([LIBQI_VERSION_PATCHLEVEL], PATCHLEVEL, [Patchlevel version])

AC_SUBST(CFLAGS)
AC_SUBST(CXXFLAGS)
AC_SUBST(LIBQI_CFLAGS)
AC_SUBST(LIBQI_INCLUDES)
AC_SUBST(LIBQI_LDFLAGS)
AC_SUBST(LIBQI_VERSION)
AC_SUBST(LiDIA_PATH)
AC_SUBST(GMP_PATH)

AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES(Makefile src/Makefile src/libqi/Makefile src/qi/Makefile include/Makefile include/libqi/Makefile doc/Makefile doc/example/Makefile)

AC_OUTPUT

echo
echo "-----------------------------------------------"
echo "The configuration procedure was	"
echo "successful."
echo
echo
echo "* Optional flags used to compile libqi:"
echo " 		$LIBQI_CFLAGS"
echo

echo "Ready to build libqi version MAJOR.MINOR.PATCHLEVEL"
echo "-----------------------------------------------"
echo
echo " *** Now, you can type \"make\" to build the package ***"
echo
