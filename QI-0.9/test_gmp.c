/** Used to test whether the machine can compile a GMP-based
    program correctly.
    From the "acinclude.m4" of the LiDIA 2.2.0 package. */
#include	<gmp.h>


#if \
(__GNU_MP_VERSION < 4) || \
(__GNU_MP_VERSION == 4 && __GNU_MP_VERSION_MINOR < 2)
/*(__GNU_MP_VERSION == 4 && __GNU_MP_MINOR == 2 && __GNU_MP_PATCHLEVEL < 1)*/
#error GMP version 4.2 recquired.
#endif

int main ()
{
	mpz_t	x, y, z;

	mpz_init(x);
	mpz_init(y);
	mpz_init(z);

	mpz_gcd(z, x, y);

	mpz_clear(x);
	mpz_clear(y);
	mpz_clear(z);
	return 0;
}
