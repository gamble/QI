/** A basic program to check whether the machine can
    compile a LiDIA-based program.
    From the LiDIA user manual. */
#include <lidia/LiDIA.h>
#include <lidia/rational_factorization.h>

#if \
(LIDIA_MAJOR_VERSION < 2) || \
(LIDIA_MAJOR_VERSION == 2 && LIDIA_MINOR_VERSION < 2)
#error LiDIA version 2.2.0 recquired.
#endif

using namespace LiDIA;

int main () {

  rational_factorization f;
  bigint n = 123;

  f.assign(n);
  f.factor();

  f.is_prime_factorization();

  return 0;

}
