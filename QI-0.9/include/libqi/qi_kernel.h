#ifndef _qi_kernel_h_
#define _qi_kernel_h_

/** Include this header if you intend to use
    the QI's calculus kernel. */

#include <lidia/bigint_matrix.h>

#include "kernel/QIElem.h"
#include "kernel/QIParam.h"
#include "kernel/QIInter.h"

#endif
