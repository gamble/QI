// Main intersection loop

#ifndef _qi_inter_h_ 
#define _qi_inter_h_

/** LiDIA */
#include <lidia/bigint_matrix.h>

/** QI */
#include "QIQsicStruct.h"

using namespace std;
using namespace LiDIA;

// Enter namespace QI
namespace QI {

// The main intersection procedure
quad_inter <bigint> intersection(const bigint_matrix &q1, const bigint_matrix &q2, 
				 const int opt_level, ostream &s);

} // end of namespace QI

#endif
